#include <amxmodx>
#include <amxmisc>
#include <cstrike>
#include <engine>
#include <fakemeta>
#include <hamsandwich>
#include <cromchat>
#include <fvault>
#include <fun>
#include <xs>
#include <pubg_mod>
#tryinclude <reapi>

#if AMXX_VERSION_NUM < 183
#include <dhudmessage>
#endif

#define PLUGIN 				"BattleRoyale Mod"
#if defined _reapi_included
#define VERSION 			"1.6 (reAPI Fork)"
#else
#define VERSION 			"1.6 (Fork)"
#endif
#define AUTHOR 				"EFFx"

#define SOUND_EFFECTS_FILE		"pubg_sound_effects"
#define COMMANDS_FILE			"pubg_commands"
#define ITENS_FILE			"pubg_itens"
#define RESOURCES_FILE			"pubg_resources"
#define WIN_SOUNDS			"pubg_winsounds"
#define WEAPONS_MODELS			"pubg_weaponsmodels"

#define LOG_FILE			"effxs_battleroyale.log"
#define FILE_FOLDER			"pubg_mod"

#if defined _reapi_included
	#define isTeam(%1)			(1 <= get_member(%1, m_iTeam) <= 2)
	#define isHeadShot(%1)			(get_member(%1, m_LastHitGroup) == HIT_HEAD)
	#define fm_get_user_team(%1)		get_member(%1, m_iTeam)
	#define fm_set_user_team(%1,%2)		set_member(%1, m_iTeam, %2)
#else
	#define m_iId				43
	#define m_pPlayer			41
	#define m_flNextPrimaryAttack		46
	#define m_flNextSecondaryAttack		47
	#define m_flTimeWeaponIdle		48
	#define m_iPrimaryAmmoType		49
	#define m_iClip				51
	#define m_fInReload			54
	#define m_fSilent			74
	#define m_iLastHitGroup			75
	#define m_flNextAttack			83
	#define m_rgAmmo_player_Slot0		376
	#define m_iTeam			114

	#define isTeam(%1)			(1 <= get_user_team(%1) <= 2)
	#define isHeadShot(%1)			(get_pdata_int(%1, m_iLastHitGroup) == HIT_HEAD)
	#define fm_get_user_team(%1)		get_pdata_int(%1, m_iTeam)
	#define fm_set_user_team(%1,%2)		set_pdata_int(%1, m_iTeam, %2)
#endif

#define closeMenu(%1)			show_menu(%1, 0, "^n")
#define explodeGrenade(%1)		set_pev(%1, pev_dmgtime, 0.0)
#define lockedSound(%1)			client_cmd(%1, "spk buttons/button11")
#define message_begin_fl(%1,%2,%3,%4) 	engfunc(EngFunc_MessageBegin, %1, %2, %3, %4)
#define write_coord_fl(%1) 		engfunc(EngFunc_WriteCoord, %1)
#define resetUserSpeed(%1)		ExecuteHamB(Ham_Player_ResetMaxSpeed, %1)

#define ITEM_KEVLAR_1_ID		0
#define ITEM_KEVLAR_2_ID		1
#define ITEM_KEVLAR_3_ID		2
#define ITEM_MEDKIT_ID			3
#define ITEM_HELMET_1_ID		4
#define ITEM_HELMET_2_ID		5
#define ITEM_HELMET_3_ID		6
#define ITEM_BAG_1_ID			7
#define ITEM_BAG_2_ID			8
#define ITEM_BAG_3_ID			9
#define ITEM_IMPULSE_ID			10
#define ITEM_SCOPE2X_ID			11
#define ITEM_SCOPE4X_ID			12
#define ITEM_PENTEALONGADO_1_ID		13
#define ITEM_PENTEALONGADO_2_ID		14
#define ITEM_PENTEALONGADO_3_ID		15
#define ITEM_GUSPE_ID			16

#define AIRDROP_AWP_ID 			20
#define AIRDROP_FULLHP_ID 		21

#define XTRA_OFS_WEAPON			4
#define XTRA_OFS_PLAYER			5
#define MESSAGE_DELAY			1.5
#define MIN_PLAYERS			2
#define BLEEDS_NUM			3
#define MIN_PLAYERS_DUO			4
#define SCOPE_2X 			55
#define SCOPE_4X 			15
#define MAX_WEAPONS_ID			30
#define KNOCK_SPEED			80.0
#define UNKNOCK_HEALTH			20
#define HELP_DISTANCE			80
#define MAX_HEALTH			100
#if !defined MAX_PLAYERS
#define MAX_PLAYERS			32
#endif
#define MAX_WEAPONNAME_LENGH		20
#define MAX_MODEL_LENGH			64
#define MAX_SPAWNS			200
#define WEAPONS_CHANCE			3
#define AIRDROP_CHANCE			2
#define MAX_WEAPONS_GROUND			70
#define MAX_AIRDROP_ITEMS		5
#define MAX_VECS			20

#define TASK_SPAWN_AIRDROP		2313412
#define TASK_COUNTDOWN			3313181
#define TASK_WEAPONS			1236613
#define TASK_REMOVEHEALTH		3818301
#define TASK_KNOCK			8831831
#define TASK_PLAYERSCOUNT		1123131
#define TASK_PLANE 			3741719
#define TASK_AIRDROP			1231344
#define TASK_MEDKIT			2311313
#define TASK_SET_DAMAGEBLE		3913113
#define TASK_ADD_WEAPONS		3813139
#define TASK_CHECK_AIRDROP_DIST		8371041
#define TASK_UP_PLAYER			7516361

const g_usEvent =			114

const DMG_HE =				(1 << 24)
const DMG_AWP =				(1 << CSW_AWP)
const SILENT_BS	= 			(1 << CSW_USP)|(1 << CSW_M4A1)
const CONST_INVALID_WEAPONS =		(1 << CSW_KNIFE)|(1 << CSW_FLASHBANG)|(1 << CSW_SMOKEGRENADE)|(1 << CSW_HEGRENADE)
const CONST_SHOTGUNS =			(1 << CSW_M3)|(1 << CSW_XM1014)
const CONST_PISTOLS =			(1 << CSW_USP)|(1 << CSW_GLOCK18)|(1 << CSW_DEAGLE)|(1 << CSW_ELITE)|(1 << CSW_FIVESEVEN)|(1 << CSW_P228)

const CONST_RIFLES = 			(1 << CSW_MAC10)|(1 << CSW_AUG)|(1 << CSW_UMP45)|
					(1 << CSW_GALIL)|(1 << CSW_FAMAS)|(1 << CSW_MP5NAVY)|
					(1 << CSW_M4A1)|(1 << CSW_TMP)|(1 << CSW_SG552)|
					(1 << CSW_AK47)|(1 << CSW_P90)|(1 << CSW_SCOUT)

stock const g_iDftMaxClip[CSW_P90 + 1] = 
{
	-1,  13, -1, 10,  1,  7,    1, 30, 30,  1,  30, 
	20, 25, 30, 35, 25,   12, 20, 10, 30, 100, 
	8 , 30, 30, 20,  2,    7, 30, 30, -1,  50
}

stock const Float:g_fDelay[CSW_P90 + 1] = 
{
	0.00, 2.70, 0.00, 2.00, 0.00, 0.55, 0.00, 3.15, 3.30, 0.00, 4.50, 
	2.70, 3.50, 3.35, 2.45, 3.30, 2.70, 2.20, 2.50, 2.63, 4.70, 
	0.55, 3.05, 2.12, 3.50, 0.00, 2.20, 3.00, 2.45, 0.00, 3.40
}

stock const g_iReloadAnims[CSW_P90 + 1] = 
{
	-1, 5, -1, 3, -1, 6, -1, 1, 1, -1, 14, 
	4, 2, 3, 1, 1, 13, 7, 4, 1, 3, 
	6, 11, 1, 3, -1, 4, 1, 1, -1, 1
}

enum pCvars
{
	cFreezeTime,
	cNewRoundTime,
	cAirDropTime,
	cHelpTime, 
	cMedkitTime,
	cMedkitHealth,
	cPlayersNumShow,
	cPlayerTrail,
	cAirDropGrenades,
	cTeamMateEffect,
	cAllowVoice,
	cSpawnWeaponsTime,
	cAllowToJumpTime,
	cImpulseGrenadeImpact,
	cGameNameInfo,
	cGameNameSetInfo,
	cImpulseMultiplier,
	cImpulseGrenRadius,
	cParachuteFallSpeed
}

enum hHuds
{
	hFreezeTime,
	hFreezeTimeEnd,
	hEquipedHud,
	hKilledMate,
	hMateInfo,
	hHudInfo,
	hLostHelmet,
	hPosition,
	hGuspe
}

enum dUserData
{
	iKills,
	iKnocks,
	iPlayerWeaponID,
	iHelpingID,
	iHelperID,
	iKnockerID,
	iWeaponKnocker,
	iArmorLevel,
	iHelmetEnt,
	iHelmetLevel,
	iHeadHits,
	iInviter,
	iTeamMateID,
	iVitorias,
	iMedKits,
	iScopedWeaponID,
	iScopeLevel,
	iPenteAlongadoWeaponID,
	iPenteAlongadoLevel,
	iWeaponBullet,
	iBagLevel,
	Float:fUserGameTime,
	bool:bKnockedOut,
	bool:bIsOnPlane,
	bool:bCanUseParachute,
	bool:bChoosedDuo,
	bool:bIsFoved,
	bool:bAllowedToSeeInfos,
	bool:bAutoMenu,
	bool:bIsAirDropMenuOpen,
	bool:bDuoTalk,
	bool:bHasGuspe,
	szName[MAX_PLAYERS]
}

enum mModels
{
	mAirDrop,
	mTransporter,
	mAirDropPlane,
	mUserParachute,
	mAirDropParachute,
	mImpulseGrenadeViewModel,
	mImpulseGrenadePlayerModel
}

enum cClasses
{
	cAirDrop,
	cTransporter,
	cWeapons,
	cParachute
}

enum sSounds
{
	sAmmoPickup,
	sGunPickup,
	sHeartBeat,
	sDamagePain,
	sTransporter,
	sHelmetHit,
	sJetFlyBy,
	sAlarm,
	sPlaneDropSound,
	sImpulseGrenadeSound,
	sClipPickup,
	sScopeSound,
	sBounce,
	sUsingMedkit,
	sMateKnocked,
	sDeathSound,
	sGuspeSound,
	sAirdropTouchSound,
	sAirDropAppearSound,
	sUnknockingMate,
	sMateUnknocked,
	sCriticalHit,
	sKnockedAnEnemy,
	sParachuteLand,
	sCriticalKill,
	sParachuteOpen,
	sGuspeAlertSound,
	sAirDropLandingSound,
	sTeammateDropSound,
	sSkyDiveSound
}

enum pPlanes
{
	pPlaneTransporter,
	pPlaneAirDrop,
	iAirDropID
}

enum vVecsData
{
	vOrigin,
	vAngles,
	vVelocities,
	vRemoveTime
}

enum vIniData
{
	vecPlane,
	vecWeapons
}

enum cCommandsData
{
	cRank,
	cDuo,
	cGetoutDuo,
	cBag,
	cMainMenu,
}

enum mModelsData
{
	szModelName[MAX_MODEL_LENGH],
	iWeaponInfoID,
}

enum iItensList
{
	iKevlarLv1,
	iKevlarLv2,
	iKevlarLv3,
	iMedKit,
	iHelmetLv1,
	iHelmetLv2,
	iHelmetLv3,
	iBagLv1,
	iBagLv2,
	iBagLv3,
	iImpulseGrenade,
	iScope2x,
	iScope4x,
	iElongatedCombLv1,
	iElongatedCombLv2,
	iElongatedCombLv3,
	iGuspe
}

enum iItensData
{
	szItemModel[MAX_MODEL_LENGH],
	iItemID,
	iItemInfoLevel,
	bool:bIsItemEnabled
}

enum nNamesData
{
	nAK47,
	nM4A1,
	nAWP,
	nSCOUT,
	nFAMAS,
	nAUG,
	nGALIL,
	nSG552,
	nGLOCK18,
	nUSP,
	nDEAGLE,
	nFIVESEVEN,
	nELITE ,
	nP228,
	nXM1014,
	nM3,
	nHEGRENADE,
	nM249,
	nTMP,
	nMAC10,
	nMP5,
	nUMP45,
	nP90
}

new const g_szClassNamesData[cClasses][] =
{
	"pubg_airdrop",
	"pubg_transporter",
	"pubg_weapons",
	"pubg_parachute"
}

new g_hHuds[hHuds]
new g_pCvars[pCvars]
new g_iEnt[pPlanes]
new g_iTotalVecs[vVecsData]
new g_iVecsNum[vIniData]
new g_dUserData[MAX_PLAYERS + 1][dUserData]

new g_szWeaponsModels[MAX_PLAYERS][mModelsData], g_iWeaponsNum
new g_szSoundsData[sSounds][MAX_MODEL_LENGH]
new g_szModelsData[mModels][MAX_MODEL_LENGH]
new g_szItens[iItensList][iItensData]
new g_cCommands[cCommandsData][MAX_MODEL_LENGH]
new g_szWeaponsDisplayName[nNamesData][MAX_PLAYERS]

new g_iFreezeTime, g_iPlayerTrailSprite, g_iExplosionSprite, g_iAirDropSprite
new g_iAirDropItems[MAX_AIRDROP_ITEMS]

new Ham:Ham_Player_ResetMaxSpeed = Ham_Item_PreFrame
new Array:g_arWinSounds

new bool:g_bPlaneSpawned, bool:g_bHasRoundEnded, bool:g_bCanJump
new m_usEventSmokeGrenade

new g_forwardGameWinner, g_forwardGameDescription
new g_iReturn

new g_mMessageStatusIcon, g_mMessageSendAudio, g_mMessageTextMsg, g_mMessageBarTime, 
g_mMessageScreenFade, g_mMessageHostagePos, g_mMessageHostageK, g_mMessageSetFov

new g_iParachuteEnt[MAX_PLAYERS + 1]
new g_szMap[MAX_PLAYERS], g_szGameName[MAX_PLAYERS], g_szIniFile[4][125], g_szText[90]

new Float:g_fSpawnVecs[MAX_SPAWNS][3]

new Float:g_fPlaneOrigins[MAX_VECS][3]
new Float:g_fPlaneAngles[MAX_VECS][3]
new Float:g_fPlaneVelocities[MAX_VECS][3]
new Float:g_fPlaneRemoveTime[MAX_VECS]
new g_iRandomDirection

new const g_szVaultName[] = "PUBG_Wins"

// Used for test some duo stuff.
//#define TEST

public plugin_init() 
{
	register_plugin(PLUGIN, VERSION, AUTHOR)

#if defined _reapi_included
	if (!is_rehlds() || !is_regamedll())
	{
		set_fail_state("reHLDS and reGameDLL_CS are required, if not using them, please compile without reapi.inc");
	}
#endif

	register_dictionary("effxs_battleroyale.txt")

	CC_SetPrefix("^x04[EFFx S]^x01:")
	
	g_pCvars[cFreezeTime] = register_cvar("pubg_freezetime", "10")
	g_pCvars[cNewRoundTime] = register_cvar("pubg_newround_time", "7")
	g_pCvars[cSpawnWeaponsTime] =register_cvar("pubg_spawnweapons_time", "3")
	g_pCvars[cAirDropTime] = register_cvar("pubg_airdrop_time", "90")
	g_pCvars[cHelpTime] = register_cvar("pubg_knock_helptime", "6")
	g_pCvars[cMedkitTime] = register_cvar("pubg_medkit_time", "5")
	g_pCvars[cMedkitHealth] = register_cvar("pubg_medkit_health", "60")
	g_pCvars[cPlayersNumShow] = register_cvar("pubg_players_info", "1")
	g_pCvars[cPlayerTrail] = register_cvar("pubg_player_trail", "0")
	g_pCvars[cAirDropGrenades] = register_cvar("pubg_airdrop_grenades", "1")
	g_pCvars[cTeamMateEffect] = register_cvar("pubg_teammate_effect", "1")
	g_pCvars[cAllowToJumpTime] = register_cvar("pubg_plane_jump_delay", "4")
	g_pCvars[cAllowVoice] = register_cvar("pubg_allow_voice", "0")
	g_pCvars[cImpulseGrenadeImpact] = register_cvar("pubg_impulsegrenade_impact", "1")
	g_pCvars[cGameNameSetInfo] = register_cvar("pubg_gamename_set_info", "1")
	g_pCvars[cGameNameInfo] = register_cvar("pubg_gamename_info", "[PUBG Mod]")
	g_pCvars[cImpulseMultiplier] = register_cvar("pubg_impulsegrenade_multiplier", "4.0")
	g_pCvars[cImpulseGrenRadius] = register_cvar("pubg_impulsegrenade_radius", "200.0")
	g_pCvars[cParachuteFallSpeed] =	register_cvar("pubg_parachute_fallsped", "-200.0")
	
	g_mMessageStatusIcon = get_user_msgid("StatusIcon")
	g_mMessageSendAudio = get_user_msgid("SendAudio")
	g_mMessageScreenFade = get_user_msgid("ScreenFade")
	g_mMessageTextMsg = get_user_msgid("TextMsg")
	g_mMessageBarTime = get_user_msgid("BarTime")
	g_mMessageHostagePos = get_user_msgid("HostagePos")
	g_mMessageHostageK = get_user_msgid("HostageK")
	g_mMessageSetFov = get_user_msgid("SetFOV")

	for(new i;i < sizeof g_hHuds;i++)
	{
		g_hHuds[hHuds:i] = CreateHudSyncObj()
	}

	register_clcmd("amx_weapons_origins", "cmdWeaponsOriginsMenu", ADMIN_IMMUNITY)
	register_clcmd("amx_plane_origins", "cmdPlaneOriginsMenu", ADMIN_IMMUNITY)
	register_clcmd("plane_remove_time", "planeRemoveTime", ADMIN_IMMUNITY)

	g_forwardGameDescription = register_forward(FM_GetGameDescription, "forward_setModeDescription")

	remove_entity_name("armoury_entity")
	execCvars()

	new szConfig[100];
	get_configsdir(szConfig, charsmax(szConfig));
	formatex(szConfig, charsmax(szConfig), "%s/%s", szConfig, FILE_FOLDER);
	if (!dir_exists(szConfig)) mkdir(szConfig);

	formatex(g_szIniFile[1], charsmax(g_szIniFile[]), "%s/pubg_vectors/%s/ents_spawn_points.ini", szConfig, g_szMap);
	formatex(g_szIniFile[2], charsmax(g_szIniFile[]), "%s/pubg_vectors/%s/plane_vecs.ini", szConfig, g_szMap);
	formatex(g_szIniFile[3], charsmax(g_szIniFile[]), "%s/pubg_management/pubg_weaponsnames.ini", szConfig);


	if (!readIniData(COMMANDS_FILE) || !readEntsPoints() || !readPlanePoints() || !readWeaponsNames())
	{
		log_amx("[PUBG Mod]: Config files not loaded.");
		set_pcvar_string(g_pCvars[cGameNameInfo], "PUBG Mod | Load Error");
		return;
	}

	m_usEventSmokeGrenade = engfunc(EngFunc_PrecacheEvent, 1, "events/createsmoke.sc")
	g_forwardGameWinner = CreateMultiForward("pubg_winner", ET_STOP, FP_CELL, FP_CELL)

	set_msg_block(get_user_msgid("Radar"), BLOCK_SET)
	set_msg_block(get_user_msgid("DeathMsg"), BLOCK_SET)

	register_message(g_mMessageSetFov, "message_SetFov")
	register_message(g_mMessageStatusIcon, "message_StatusIcon")
	register_message(g_mMessageSendAudio, "message_SendAudio") 
	
	new szRadioCommands[][] = 
	{
		"radio1", "coverme", "takepoint", 
		"holdpos", "regroup", "followme", "takingfire",
		"radio2", "go", "fallback", "sticktog", 
		"getinpos", "stormfront", "report",
		"radio3", "roger", "enemyspot", "needbackup", 
		"sectorclear", "inposition", "reportingin", "getout", 
		"negative", "enemydown"
	}

	for(new i; i < sizeof szRadioCommands; i++)
	{
		register_clcmd(szRadioCommands[i], "ClientCommand_Radio")
	}

	register_clcmd("drop", "cmdDrop")
	register_clcmd("say", "cmdChatSay")
	register_clcmd("say_team", "cmdChatSayTeam")

	register_clcmd("amx_items_id_list", "cmdItemIdList", ADMIN_IMMUNITY)
	register_clcmd("amx_give_item", "cmdGiveItem", ADMIN_IMMUNITY, "<target name> <item id (type amx_items_id_list)> <item level> <item life>")
	
	#if defined TEST
	register_clcmd("amx_randomduo", "makeRandomDuo", ADMIN_IMMUNITY)
	#endif
	
	register_forward(FM_Touch, "forward_touchItem")
	register_forward(FM_AddToFullPack, "forward_AddToFullPack", 1)
	register_forward(FM_CmdStart, "forward_cmdStart")
	register_forward(FM_SetModel, "forward_SetModel")
	register_forward(FM_PlaybackEvent, "forward_PlaybackEvent", false)
	register_forward(FM_EmitSound, "forward_EmitSound")
	register_forward(FM_Voice_SetClientListening, "forward_SetClientListening")
	
	register_event("CurWeapon", "event_CurWeapon", "be", "1=1")
	register_event("HLTV", "event_NewRound", "a", "1=0", "2=0")  
	register_logevent("event_GameWill", 2, "1&Restart_Round_", "1=Game_Commencing")
	
	RegisterHam(Ham_BloodColor, "player", "ham_BloodColor")
	RegisterHam(Ham_Spawn, "player", "ham_PlayerSpawn", 1)
	RegisterHam(Ham_Killed, "player", "ham_PlayerKilled_Pre")
	RegisterHam(Ham_TakeDamage, "player", "ham_PlayerTakeDamage")
	RegisterHam(Ham_Player_Jump, "player", "ham_Player_Jump_Pre", 0)
	RegisterHam(Ham_Item_Deploy, "weapon_smokegrenade", "ham_SmokeDeploy_Post", 1)
	RegisterHam(Ham_Player_ImpulseCommands, "player", "ham_FlashLight")
	
	new szWeaponName[MAX_WEAPONNAME_LENGH]
	for(new iWeaponID = 1;iWeaponID <= MAX_WEAPONS_ID;iWeaponID++)
	{
		if(((1 << iWeaponID) & CONST_RIFLES) && get_weaponname(iWeaponID, szWeaponName, charsmax(szWeaponName)))
		{
			RegisterHam(Ham_Weapon_PrimaryAttack, szWeaponName , "ham_WeaponPrimaryAttack")
			RegisterHam(Ham_Item_PostFrame, szWeaponName, "Item_PostFrame")
				
			if(iWeaponID == CSW_SCOUT)
				continue
					
			RegisterHam(Ham_Weapon_Reload, szWeaponName, "ham_WeaponReload", 1)
		}
	}
}

execCvars()
{
	new szDir[125]
	get_configsdir(szDir, charsmax(szDir))
	formatex(szDir, charsmax(szDir), "%s/%s/pubg_management/pubg_cvars.cfg", szDir, FILE_FOLDER)
	if(!file_exists(szDir))
	{
		log_to_file(LOG_FILE, "[PUBG Mod]: %L", LANG_SERVER, "SERVER_NO_CONFIG_FILE", szDir)
		return
	}
	server_cmd("exec ^"%s^"", szDir)
	
	if(!get_pcvar_num(g_pCvars[cGameNameSetInfo]))
	{
		unregister_forward(FM_GetGameDescription, g_forwardGameDescription)
	}
	else get_pcvar_string(g_pCvars[cGameNameInfo], g_szGameName, charsmax(g_szGameName))
}

public plugin_natives()
{
	register_native("isTeamMate", "_isTeamMate")
	register_native("isUserKnockedOut", "_isUserKnockedOut")
	register_native("isUserInDuo", "_isUserInDuo")
	register_native("getUserWins", "_getUserWins")
}

public _isTeamMate()
{
	return bool:(g_dUserData[get_param(1)][iTeamMateID] == get_param(2))
}

public _isUserKnockedOut()
{
	return g_dUserData[get_param(1)][bKnockedOut]
}

public _isUserInDuo()
{
	return g_dUserData[get_param(1)][bChoosedDuo]
}

public _getUserWins()
{
	return g_dUserData[get_param(1)][iVitorias]
}

public plugin_precache()
{
	g_arWinSounds = ArrayCreate(MAX_MODEL_LENGH)
	
	readIniData(SOUND_EFFECTS_FILE)
	readIniData(ITENS_FILE)
	readIniData(RESOURCES_FILE)
	readIniData(WIN_SOUNDS)
	readIniData(WEAPONS_MODELS)
	
	get_mapname(g_szMap, charsmax(g_szMap))
	
	g_iPlayerTrailSprite = 	precache_model("sprites/smoke.spr")
	g_iExplosionSprite = 	precache_model("sprites/shockwave.spr")
	g_iAirDropSprite = 	precache_model("sprites/pubg/airdrop_smoke.spr") 
}

#if defined TEST
public makeRandomDuo(id, level, cid)
{
	if(!cmd_access(id, level, cid, 1))
		return PLUGIN_HANDLED
		
	give_item(id, "weapon_ak47")
	cs_set_user_bpammo(id, CSW_AK47, 999)
	
	new iPlayers[32], iNum, iRandomPlayer, iRandomDuoPlayer
	get_players(iPlayers, iNum, "aech", "CT")
	
	for(new i, iPlayer;i < iNum;i++)
	{
		iPlayer = iPlayers[i]
		
		g_dUserData[iPlayer][bChoosedDuo] = false
		g_dUserData[iPlayer][iTeamMateID] = 0
	}
	
	iRandomPlayer = iPlayers[random(iNum)]
	iRandomDuoPlayer = iPlayers[random(iNum)]
	
	get_players(iPlayers, iNum)
	for(new i;i < iNum;i++)
	{
		CC_SendMatched(iPlayers[i], CC_COLOR_GREY, "%L", iPlayers[i], "SERVER_RANDOM_DUO", g_dUserData[iRandomPlayer][szName], g_dUserData[iRandomDuoPlayer][szName])
	}

	g_dUserData[iRandomPlayer][bChoosedDuo] = true
	g_dUserData[iRandomPlayer][iTeamMateID] = iRandomDuoPlayer
		
	g_dUserData[iRandomDuoPlayer][bChoosedDuo] = true
	g_dUserData[iRandomDuoPlayer][iTeamMateID] = iRandomPlayer
	return PLUGIN_CONTINUE
}
#endif

public client_infochanged(id)
{
	get_user_info(id, "name", g_dUserData[id][szName], charsmax(g_dUserData[]))
}

public client_putinserver(id)
{
	get_user_info(id, "name", g_dUserData[id][szName], charsmax(g_dUserData[]))
	loadData(id)

	g_dUserData[id][bAutoMenu] = true
	if((get_playersnum() - 2) <= MIN_PLAYERS)
	{
		restartRound(1)
	}
}

public client_disconnected(id)
{
	set_task(0.1, "checkDisconnectNum")

	// Really needed?
	if (g_iParachuteEnt[id] > 0)
	{
		remove_entity(g_iParachuteEnt[id]);
		g_iParachuteEnt[id] = 0;
	}

	removeHelmet(id);
	checkTasks(id);
	saveData(id);
	resetDuos(id);
}

public checkDisconnectNum()
{
	new iPlayers[MAX_PLAYERS], playerCount
	get_players(iPlayers, playerCount, "ch")
	if(playerCount < MIN_PLAYERS_DUO)
	{
		for(new i; i < playerCount; i++)
		{
			resetDuos(iPlayers[i])
		}
	}

	checkNum()
}

public event_GameWill()
{
	client_cmd(0, "stopsound")
}

public event_NewRound()
{
	removeEntities()
	removeTasks()

	g_bCanJump = false
	g_bPlaneSpawned = false
	g_bHasRoundEnded = false

	g_iFreezeTime = (get_pcvar_num(g_pCvars[cFreezeTime]) + 1)

	if((get_playersnum() - 2) >= 1)
	{
		set_task(1.0, "hudCountDown", TASK_COUNTDOWN, .flags = "a", .repeat = g_iFreezeTime)
	}
}

public playersCount()
{
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ach")

	for(new i, iPlayer, szHelmetStatus[15], szMedKits[10], szKey[2][25], szKeyDescription[2][25];i < iNum;i++)
	{
		iPlayer = iPlayers[i]
		
		set_dhudmessage(255, 255, 255, 0.70, 0.0, 0, 0.5, 0.5)
		show_dhudmessage(iPlayer, "%d %L%s", getPlayersNum(), iPlayer, "SERVER_ALIVE", (getPlayersNum() > 1) ? "S" : "")
		
		set_dhudmessage(255, 0, 0, 0.85, 0.0, 0, 0.5, 0.5)
		show_dhudmessage(iPlayer, "%d %L%s", g_dUserData[iPlayer][iKills], iPlayer, "SERVER_KILLS", (g_dUserData[iPlayer][iKills] > 1) ? "S" : "")
	
		if(g_bCanJump && pev_valid(g_iEnt[pPlaneTransporter]))
		{
			if(g_dUserData[iPlayer][bIsOnPlane] || g_dUserData[iPlayer][bCanUseParachute])
			{
				if(g_dUserData[iPlayer][bIsOnPlane])
				{
					client_print(iPlayer, print_center, "%L", iPlayer, "SERVER_PLAYERS_IN_THE_PLANE", getPlayersInThePlaneNum())
				}

				formatex(szKey[0], charsmax(szKey[]), "%L", iPlayer, "SERVER_SPACE_KEY")
				formatex(szKey[1], charsmax(szKey[]), "%L", iPlayer, "SERVER_USE_KEY")
				
				formatex(szKeyDescription[0], charsmax(szKeyDescription[]), "%L", iPlayer, "SERVER_SPACE_KEY_DESCRIP")
				formatex(szKeyDescription[1], charsmax(szKeyDescription[]), "%L", iPlayer, "SERVER_USE_KEY_DESCRIP")
				
				set_dhudmessage(255, 0, 0, 0.73, 0.60, 0, 0.5, 0.5)
				show_dhudmessage(iPlayer, g_dUserData[iPlayer][bIsOnPlane] ? szKey[0] : szKey[1])
			
				set_dhudmessage(255, 255, 255, 0.70, 0.63, 0, 0.5, 0.5)
				show_dhudmessage(iPlayer, g_dUserData[iPlayer][bIsOnPlane] ? szKeyDescription[0] : szKeyDescription[1])
			}
		}
		
		if(g_dUserData[iPlayer][bAllowedToSeeInfos])
		{
			g_dUserData[iPlayer][iHelmetEnt] ? formatex(szHelmetStatus, charsmax(szHelmetStatus), "%d%%", (100 - getPercent(g_dUserData[iPlayer][iHeadHits], getMaxHeadHits(iPlayer)))) : formatex(szHelmetStatus, charsmax(szHelmetStatus), "%L", iPlayer, "SERVER_NO_HELMET")
			g_dUserData[iPlayer][iMedKits] ? formatex(szMedKits, charsmax(szMedKits), "%d", g_dUserData[iPlayer][iMedKits]) : formatex(szMedKits, charsmax(szMedKits), "%L", iPlayer, "SERVER_NO_MEDKITS")
			
			new szHelmet[15], szBag[15]
			formatex(szHelmet, charsmax(szHelmet), "%L", iPlayer, "SERVER_ITEM_HELMET")
			formatex(szBag, charsmax(szBag), "%L", iPlayer, "SERVER_ITEM_BAG")
			
			set_hudmessage(255, 255, 255, 0.02, 0.23, 0, 0.5, 0.5)
			ShowSyncHudMsg
			(
				iPlayer, 
				g_hHuds[hHudInfo], 
				"MedKits: %s^n%s: %s^n%s: %d%%",
				szMedKits,
				szHelmet,
				szHelmetStatus,
				szBag,
				getPercent((getUserItens(iPlayer) + getWeaponsNum(iPlayer)), getUserMaxItens(iPlayer))
			)
		}

		new iTeammateIndex = g_dUserData[iPlayer][iTeamMateID]
		if(!haveDuoLeft() || !is_user_alive(iTeammateIndex))
			continue
		
		if(!isTeam(iTeammateIndex))
		{
			resetDuos(iPlayer)
			continue
		}

		showOnRadar(iPlayer, iTeammateIndex)

		new Float:fPlayerOrigin[3], Float:fMateOrigin[3]
		pev(iPlayer, pev_origin, fPlayerOrigin)
		pev(iTeammateIndex, pev_origin, fMateOrigin)

		new szMate[25], szMeters[15], szLife[15], szMateKnocked[MAX_PLAYERS]
		formatex(szMate, charsmax(szMate), "%L", iPlayer, "SERVER_TEAMMATE")
		formatex(szMeters, charsmax(szMeters), "%L", iPlayer, "SERVER_METERS")
		formatex(szLife, charsmax(szLife), "%L", iPlayer, "SERVER_LIFE")
		formatex(szMateKnocked, charsmax(szMateKnocked), "%L", iPlayer, "SERVER_MATE_KNOCKED")
		
		set_hudmessage(255, 255, 255, -1.0, 0.76, 0, 0.5, 0.5)
		ShowSyncHudMsg(iPlayer, g_hHuds[hMateInfo], "%s: %d %s^n%s: %d", szMate, floatround(get_distance_f(fPlayerOrigin, fMateOrigin) * 0.0254), szMeters, szLife, get_user_health(iTeammateIndex))
		
		if(g_dUserData[iTeammateIndex][bKnockedOut])
		{
			set_dhudmessage(255, 0, 0, -1.0, 0.82, 0, 0.5, 0.5)
			show_dhudmessage(iPlayer, szMateKnocked)
		}
	}
}

public hudCountDown()
{
	--g_iFreezeTime
	
	set_hudmessage(220, 80, 0, -1.0, 0.2, 0, 1.0, 1.0)
	ShowSyncHudMsg(0, g_hHuds[hFreezeTime], "%L", LANG_PLAYER, "SERVER_ROUNDSTART_HUD_1", g_iFreezeTime, (g_iFreezeTime > 1) ? "s" : "")
	
	new szSound[8]
	num_to_word(g_iFreezeTime, szSound, charsmax(szSound))
	client_cmd(0, "spk fvox/%s", szSound)
	
	if(!g_iFreezeTime)
	{
		ClearSyncHud(0, g_hHuds[hFreezeTime])
		
		if(get_pcvar_num(g_pCvars[cPlayersNumShow]))
		{
			set_task(0.5, "playersCount", TASK_PLAYERSCOUNT, .flags = "b")
		}
		
		set_hudmessage(0, 255, 0, -1.0, 0.2, 0, 1.0, 4.0)
		ShowSyncHudMsg(0, g_hHuds[hFreezeTimeEnd], "%L", LANG_PLAYER, "SERVER_ROUNDSTART_HUD_2")

		new Float:fSpawnWeaponTime = get_pcvar_float(_:g_pCvars[cSpawnWeaponsTime])
		set_task(random_float((fSpawnWeaponTime / 2.0), fSpawnWeaponTime), "prepareWeaponsSpawn", TASK_WEAPONS)
	}
	
	message_begin(MSG_ALL, get_user_msgid("BombPickup"))
	message_end()
}

public prepareWeaponsSpawn()
{
	new iCvarAirDropTime = get_pcvar_num(g_pCvars[cAirDropTime])
	new iMinutes = (iCvarAirDropTime / 60), iSeconds = (iCvarAirDropTime % 60)
	new szMinutes[18], szSeconds[18]
		
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ch")
	for(new i, iPlayer, szPreposition[5];i < iNum;i++)
	{	
		iPlayer = iPlayers[i] 
		
		CC_SendMatched(iPlayer, CC_COLOR_GREY, "^x04=====================================")
		CC_SendMatched(iPlayer, CC_COLOR_TEAM, "%L", iPlayer, "SERVER_ROUNDSTART_MESSAGE_1")

		if(g_iVecsNum[vecPlane])
		{
			if(iMinutes)
			{
				formatex(szMinutes, charsmax(szMinutes), "%d %L%s", iMinutes, iPlayer, "SERVER_MINUTES", (iMinutes > 1) ? "s" : "")
			}
		
			if(iSeconds)
			{
				formatex(szPreposition, charsmax(szPreposition), " %L ", iPlayer, "SERVER_PREPOSITION")
				formatex(szSeconds, charsmax(szSeconds), "%s%d %L%s", iMinutes ? szPreposition : "", iSeconds, iPlayer, "SERVER_SECONDS", (iSeconds > 1) ? "s" : "")
			}
			CC_SendMatched(iPlayer, CC_COLOR_TEAM, "%L", iPlayer, "SERVER_ROUNDSTART_MESSAGE_2", szMinutes, szSeconds)
		}
		CC_SendMatched(iPlayer, CC_COLOR_GREY, "^x04=====================================")
	}

	if(g_iVecsNum[vecPlane])
	{
		createPlane()
		set_task(float(iCvarAirDropTime), "respawnAirDrop", TASK_SPAWN_AIRDROP, .flags = "b")
	}

	if(g_iVecsNum[vecWeapons])
	{
		set_task(0.1, "spawnWeapons", TASK_ADD_WEAPONS, .flags = "a", .repeat = (MAX_WEAPONS_GROUND + floatround((iNum * iNum) * 0.05))); // 822
	}
}

public respawnAirDrop()
{
	if (!g_bHasRoundEnded)
	{
		clientPlaySound(0, g_szSoundsData[sAlarm])
		set_task(5.0, "planeDrop")
	}
}

public client_PreThink(id)
{
	if(!is_user_alive(id))
	{
		return
	}
	
	static Float:fSpeed, Float:fVecVelocity[3]
	entity_get_vector(id, EV_VEC_velocity, fVecVelocity)
	fSpeed = vector_length(fVecVelocity)

	if(task_exists(id + TASK_MEDKIT) && (fSpeed >= 50.0))
	{
		client_cmd(id, "stopsound")
		barTime(id, .iTime = 0)

		remove_task(id + TASK_MEDKIT)
	}
	
	new bool:bIsOnGround = bool:(pev(id, pev_flags) & FL_ONGROUND)
	if((g_iParachuteEnt[id] > 0) && bIsOnGround)
	{
		remove_entity(g_iParachuteEnt[id])
		set_user_gravity(id, 1.0)
		g_iParachuteEnt[id] = 0
	}
	
	if((bIsOnGround && distanceToGround(id) <= 10.0) && g_dUserData[id][bCanUseParachute])
	{
		g_dUserData[id][bCanUseParachute] = false
		g_dUserData[id][bAllowedToSeeInfos] = true
			
		setDamageble(id, true)

		client_cmd(id, "stopsound")
		clientPlaySound(id, g_szSoundsData[sParachuteLand], true)
	}
	
	static Float:fGameTime, iButton
	fGameTime = get_gametime()
	iButton = get_user_button(id)
	
	if((fGameTime - g_dUserData[id][fUserGameTime]) > 0.5)
	{
		if((iButton & IN_ATTACK2) && (~iButton & IN_ATTACK))
		{
			if(get_user_weapon(id) == g_dUserData[id][iScopedWeaponID])
			{
				g_dUserData[id][fUserGameTime] = _:fGameTime
				makeFov(id, g_dUserData[id][bIsFoved] ? 0 : (g_dUserData[id][iScopeLevel] == 1) ? SCOPE_2X : SCOPE_4X)
			}
		}
	}

	if(!g_dUserData[id][bCanUseParachute]) 
		return

	static Float:fFallSpeed;fFallSpeed = get_pcvar_float(_:g_pCvars[cParachuteFallSpeed])
	static Float:fFrame
	
	static iOldButton;iOldButton = get_user_oldbutton(id)
	if(iButton & IN_USE)
	{
		if(fVecVelocity[2] < 0.0) 
		{
			if(g_iParachuteEnt[id] <= 0) 
			{
				g_iParachuteEnt[id] = create_entity("info_target")
				if(g_iParachuteEnt[id] > 0) 
				{
					clientPlaySound(id, g_szSoundsData[sParachuteOpen], true)

					entity_set_string(g_iParachuteEnt[id], EV_SZ_classname, g_szClassNamesData[cParachute])
					entity_set_edict(g_iParachuteEnt[id], EV_ENT_aiment, id)
					entity_set_edict(g_iParachuteEnt[id], EV_ENT_owner, id)
					entity_set_int(g_iParachuteEnt[id], EV_INT_movetype, MOVETYPE_FOLLOW)
					entity_set_model(g_iParachuteEnt[id], g_szModelsData[mUserParachute])
					entity_set_int(g_iParachuteEnt[id], EV_INT_sequence, 0)
					entity_set_int(g_iParachuteEnt[id], EV_INT_gaitsequence, 1)
					entity_set_float(g_iParachuteEnt[id], EV_FL_frame, 0.0)
					entity_set_float(g_iParachuteEnt[id], EV_FL_fuser1, 0.0)
				}
			}

			if(g_iParachuteEnt[id] > 0) 
			{
				set_user_gravity(id, 0.1)

				fVecVelocity[2] = (fVecVelocity[2] + 40.0 < fFallSpeed) ? fVecVelocity[2] + 40.0 : fFallSpeed
				entity_set_vector(id, EV_VEC_velocity, fVecVelocity)

				if(entity_get_int(g_iParachuteEnt[id], EV_INT_sequence) == 0) 
				{
					fFrame = entity_get_float(g_iParachuteEnt[id], EV_FL_fuser1) + 1.0
					entity_set_float(g_iParachuteEnt[id], EV_FL_fuser1, fFrame)
					entity_set_float(g_iParachuteEnt[id], EV_FL_frame, fFrame)

					if(fFrame > 100.0) 
					{
						entity_set_float(g_iParachuteEnt[id], EV_FL_animtime, 0.0)
						entity_set_float(g_iParachuteEnt[id], EV_FL_framerate, 0.4)
						entity_set_int(g_iParachuteEnt[id], EV_INT_sequence, 1)
						entity_set_int(g_iParachuteEnt[id], EV_INT_gaitsequence, 1)
						entity_set_float(g_iParachuteEnt[id], EV_FL_frame, 0.0)
						entity_set_float(g_iParachuteEnt[id], EV_FL_fuser1, 0.0)
					}
				}
			}
		}
		else if(g_iParachuteEnt[id] > 0) 
		{
			remove_entity(g_iParachuteEnt[id])
			set_user_gravity(id, 1.0)
			g_iParachuteEnt[id] = 0
		}
	}
	else if((iOldButton & IN_USE) && g_iParachuteEnt[id] > 0) 
	{
		remove_entity(g_iParachuteEnt[id])
		set_user_gravity(id, 1.0)
		g_iParachuteEnt[id] = 0
	}
}

showAirDropItems(id)
{
	new szTittle[60], szItemFormat[60], szID[3]
	formatex(szTittle, charsmax(szTittle), "%L", id, "SERVER_AIRDROP_MENU")
	new iMenu = menu_create(szTittle, "airdrop_menu_handler")
	
	if(g_iAirDropItems[0])
	{
		num_to_str(AIRDROP_AWP_ID, szID, charsmax(szID))

		new szAWPName[MAX_WEAPONNAME_LENGH]
		changeWeaponName(CSW_AWP, szAWPName)
		menu_additem(iMenu, szAWPName, szID)
	}
	
	if(g_iAirDropItems[1])
	{
		num_to_str(ITEM_MEDKIT_ID, szID, charsmax(szID))
		menu_additem(iMenu, "MedKit", szID)
	}
	
	if(g_iAirDropItems[2])
	{
		num_to_str(ITEM_KEVLAR_3_ID, szID, charsmax(szID))
		formatex(szItemFormat, charsmax(szItemFormat), "%L lv 3.", id, "SERVER_ITEM_KEVLAR")
		menu_additem(iMenu, szItemFormat, szID)
	}
	
	if(g_iAirDropItems[3])
	{
		num_to_str(AIRDROP_FULLHP_ID, szID, charsmax(szID))
		formatex(szItemFormat, charsmax(szItemFormat), "%L", id, "SERVER_AIRDROP_FULL_HP")
		menu_additem(iMenu, szItemFormat, szID)
	}
	
	if(g_iAirDropItems[4])
	{
		num_to_str(ITEM_HELMET_3_ID, szID, charsmax(szID))
		formatex(szItemFormat, charsmax(szItemFormat), "%L lv. 3", id, "SERVER_ITEM_HELMET")
		menu_additem(iMenu, szItemFormat, szID)
	}
	
	if(!szItemFormat[0])
	{
		killEntity(g_iEnt[iAirDropID])
		g_iEnt[iAirDropID] = 0
		return
	}
	menu_display(id, iMenu)
}

public airdrop_menu_handler(id, iMenu, iItem)
{

	if((iItem == MENU_EXIT) || !g_dUserData[id][bIsAirDropMenuOpen] || !pev_valid(g_iEnt[iAirDropID]))
	{
		g_dUserData[id][bIsAirDropMenuOpen] = false
		menu_destroy(iMenu)
		return
	}
	new szData[6], iAccess, item_callback
	menu_item_getinfo(iMenu, iItem, iAccess, szData, charsmax(szData), .callback = item_callback)

	if(checkItensNum(id))
	{
		g_dUserData[id][bIsAirDropMenuOpen] = false
		return
	}
		
	switch(str_to_num(szData))
	{
		case AIRDROP_AWP_ID:
		{
			new szWeaponName[MAX_PLAYERS], iWeapon = CSW_AWP
			get_weaponname(iWeapon, szWeaponName, charsmax(szWeaponName))
			give_item(id, szWeaponName)
			cs_set_user_bpammo(id, iWeapon, 30)
			
			g_iAirDropItems[0] = 0
		}
		case ITEM_MEDKIT_ID:
		{
			giveWeapons(id, 0, 0, .iEntityID = ITEM_MEDKIT_ID)
			g_iAirDropItems[1] = 0
		}
		case ITEM_KEVLAR_3_ID:
		{
			if(g_dUserData[id][iArmorLevel] == 3)
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
				lockedSound(id)
			}
			else
			{
				giveWeapons(id, 0, 0, .iEntityID = ITEM_KEVLAR_3_ID, .iValue = 100)
				g_iAirDropItems[2] = 0
			}
		}
		case AIRDROP_FULLHP_ID:
		{
			if(get_user_health(id) >= MAX_HEALTH)
			{
				CC_SendMatched(id, CC_COLOR_GREY, "%L", id, "SERVER_FULL_HEALTH")
				lockedSound(id)
			}
			else
			{
				set_user_health(id, MAX_HEALTH)
				g_iAirDropItems[3] = 0
			}
			
		}
		case ITEM_HELMET_3_ID:
		{
			if(g_dUserData[id][iHelmetLevel] == 3)
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
				lockedSound(id)
			}
			else
			{
				giveWeapons(id, 0, 0, .iEntityID = ITEM_HELMET_3_ID)
				g_iAirDropItems[4] = 0
			}
		}
	}
	g_dUserData[id][bIsAirDropMenuOpen] = false
	menu_destroy(iMenu)
}

public ham_BloodColor(const id)
{
	if(isTeamMate(g_dUserData[id][iTeamMateID], id))
	{
		SetHamReturnInteger(-1)
		return HAM_SUPERCEDE
	}
	return HAM_IGNORED
}

public ham_Player_Jump_Pre(id)
{
	if(g_dUserData[id][bIsOnPlane] && g_bCanJump)
	{
		dropPlayer(id)
		return HAM_HANDLED
	}
	
	if(g_dUserData[id][bKnockedOut])
	{
		static iOldbuttons; iOldbuttons = entity_get_int(id, EV_INT_oldbuttons)
		if(!(iOldbuttons & IN_JUMP))
		{
			entity_set_int(id, EV_INT_oldbuttons, (iOldbuttons | IN_JUMP))
			return HAM_HANDLED
		}
	}
	return HAM_HANDLED
}

public ham_WeaponReload(const iWeapon)
{
#if defined _reapi_included
	new id = get_member(iWeapon, m_pPlayer);
	if (is_user_connected(id) && g_dUserData[id][bIsFoved] && get_member(iWeapon, m_Weapon_fInReload))
#else
	new id = get_pdata_cbase(iWeapon, m_pPlayer, XTRA_OFS_WEAPON);
	if (is_user_connected(id) && g_dUserData[id][bIsFoved] && get_pdata_int(iWeapon, m_fInReload, XTRA_OFS_WEAPON))
#endif
	{
		makeFov(id, 0);
	}
	return HAM_IGNORED;
}

public ham_WeaponPrimaryAttack(const iWeapon)
{
#if defined _reapi_included
	new id = get_member(iWeapon, m_pPlayer);
#else
	new id = get_pdata_cbase(iWeapon, m_pPlayer, XTRA_OFS_WEAPON);
#endif
	if (is_user_connected(id) && g_dUserData[id][bIsFoved] && (cs_get_weapon_ammo(iWeapon) > 0))
		entity_set_vector(id, EV_VEC_punchangle, Float:{-0.3, 0.3, 0.0});
	return HAM_IGNORED;
}

//Code from Connor's Weapons MaxClip
#if defined _reapi_included
	public Item_PostFrame(iEnt)
	{
		static iId, id, fInReload, Float:flNextAttack, iAmmoType, iBpAmmo, iClip, iButton;
		iId = get_member(iEnt, m_iId);
		id = get_member(iEnt, m_pPlayer);

		if (iId != g_dUserData[id][iPenteAlongadoWeaponID]) return;

		fInReload = get_member(iEnt, m_Weapon_fInReload);
		flNextAttack = get_member(id, m_flNextAttack);

		iAmmoType = get_member(iEnt, m_Weapon_iPrimaryAmmoType);
		iBpAmmo = get_member(id, m_rgAmmo, iAmmoType);
		iClip = get_member(iEnt, m_Weapon_iClip);

		if (fInReload && flNextAttack <= 0.0)
		{
			new j = min(g_dUserData[id][iWeaponBullet] - iClip, iBpAmmo)

			set_member(iEnt, m_Weapon_iClip, iClip + j);
			set_member(id, m_rgAmmo, iBpAmmo - j, iAmmoType);
			set_member(iEnt, m_Weapon_fInReload, 0);
			fInReload = 0;
		}

		iButton = pev(id, pev_button);
		if ((iButton & IN_ATTACK2 && get_member(iEnt, m_Weapon_flNextSecondaryAttack) <= 0.0)
			|| (iButton & IN_ATTACK && get_member(iEnt, m_Weapon_flNextPrimaryAttack) <= 0.0))
		{
			return;
		}

		if (iButton & IN_RELOAD && !fInReload)
		{
			if (iClip >= g_dUserData[id][iWeaponBullet])
			{
				set_pev(id, pev_button, iButton & ~IN_RELOAD)
				if (SILENT_BS & (1 << iId) && !get_member(iEnt, m_Weapon_iWeaponState))
				{
					SendWeaponAnim(id, (iId == CSW_USP) ? 8 : 7);
				}
				else
				{
					SendWeaponAnim(id, 0);
				}
			}
			else if (iClip == g_iDftMaxClip[iId] && iBpAmmo)
			{
				set_member(id, m_flNextAttack, g_fDelay[iId]);

				if (SILENT_BS & (1 << iId) && get_member(iEnt, m_Weapon_iWeaponState))
				{
					SendWeaponAnim(id, (iId == CSW_USP) ? 5 : 4);
				}
				else
				{
					SendWeaponAnim(id, g_iReloadAnims[iId]);
				}
				set_member(iEnt, m_Weapon_fInReload, 1);
				set_member(iEnt, m_flTimeWeaponIdle, g_fDelay[iId] + 0.5);
			}
		}
	}
#else
	public Item_PostFrame(iEnt)
	{
		static iId;iId = get_pdata_int(iEnt, m_iId, XTRA_OFS_WEAPON)
		static id;id = get_pdata_cbase(iEnt, m_pPlayer, XTRA_OFS_WEAPON)
		
		if(iId != g_dUserData[id][iPenteAlongadoWeaponID])
			return

		static fInReload;fInReload = get_pdata_int(iEnt, m_fInReload, XTRA_OFS_WEAPON)
		static Float:flNextAttack;flNextAttack = get_pdata_float(id, m_flNextAttack, XTRA_OFS_PLAYER)
		
		static iAmmoType;iAmmoType = m_rgAmmo_player_Slot0 + get_pdata_int(iEnt, m_iPrimaryAmmoType, XTRA_OFS_WEAPON)
		static iBpAmmo;iBpAmmo = get_pdata_int(id, iAmmoType, XTRA_OFS_PLAYER)
		static iClip;iClip = get_pdata_int(iEnt, m_iClip, XTRA_OFS_WEAPON)
		
		if(fInReload && flNextAttack <= 0.0)
		{
			new j = min(g_dUserData[id][iWeaponBullet] - iClip, iBpAmmo)
			
			set_pdata_int(iEnt, m_iClip, iClip + j, XTRA_OFS_WEAPON)
			set_pdata_int(id, iAmmoType, iBpAmmo - j, XTRA_OFS_PLAYER)
			set_pdata_int(iEnt, m_fInReload, 0, XTRA_OFS_WEAPON)
			fInReload = 0
		}
		
		static iButton;iButton = pev(id, pev_button)
		if((iButton & IN_ATTACK2 && get_pdata_float(iEnt, m_flNextSecondaryAttack, XTRA_OFS_WEAPON) <= 0.0)
		|| (iButton & IN_ATTACK && get_pdata_float(iEnt, m_flNextPrimaryAttack, XTRA_OFS_WEAPON) <= 0.0))
		{
			return
		}

		if(iButton & IN_RELOAD && !fInReload)
		{
			if(iClip >= g_dUserData[id][iWeaponBullet])
			{
				set_pev(id, pev_button, iButton & ~IN_RELOAD)
				if(SILENT_BS & (1 << iId) && !get_pdata_int(iEnt, m_fSilent, XTRA_OFS_WEAPON))
				{
					SendWeaponAnim(id, (iId == CSW_USP) ? 8 : 7)
				}
				else
				{
					SendWeaponAnim(id, 0)
				}
			}
			else if(iClip == g_iDftMaxClip[iId])
			{
				if(iBpAmmo)
				{
					set_pdata_float(id, m_flNextAttack, g_fDelay[iId], XTRA_OFS_PLAYER)
					
					if(SILENT_BS & (1 << iId) && get_pdata_int(iEnt, m_fSilent, XTRA_OFS_WEAPON))
					{
						SendWeaponAnim(id, (iId == CSW_USP) ? 5 : 4)
					}
					else
					{
						SendWeaponAnim(id, g_iReloadAnims[iId])
					}
					set_pdata_int(iEnt, m_fInReload, 1, XTRA_OFS_WEAPON)
					set_pdata_float(iEnt, m_flTimeWeaponIdle, g_fDelay[iId] + 0.5, XTRA_OFS_WEAPON)
				}
			}
		}
	}
#endif

public forward_SetClientListening(iReceiver, iSender, bool:bListen)
{
	if(g_dUserData[iSender][bDuoTalk])
	{
		if(g_dUserData[iSender][iTeamMateID] == iReceiver)
		{
			engfunc(EngFunc_SetClientListening, iReceiver, iSender, true)
			forward_return(FMV_CELL, true)
			return FMRES_SUPERCEDE
		}
		
		engfunc(EngFunc_SetClientListening, iReceiver, iSender, false)
		forward_return(FMV_CELL, false)
		return FMRES_SUPERCEDE
	}
	
	if(!get_pcvar_num(g_pCvars[cAllowVoice]))
	{
		if(!is_user_alive(iSender) && is_user_alive(iReceiver))
		{
			engfunc(EngFunc_SetClientListening, iReceiver, iSender, false)
			forward_return(FMV_CELL, false)
			return FMRES_SUPERCEDE
		}

		engfunc(EngFunc_SetClientListening, iReceiver, iSender, true)
		forward_return(FMV_CELL, true)
		return FMRES_SUPERCEDE
	}
	return FMRES_IGNORED
}

public forward_cmdStart(id, uc_handle)
{
	if(!is_user_alive(id))
		return FMRES_IGNORED
		
	static Float:fGameTime;fGameTime = get_gametime()
	new iButton = get_uc(uc_handle, UC_Buttons)
		
	if(g_dUserData[id][bChoosedDuo])
	{
		if(get_uc(uc_handle, UC_Impulse) == 100) 
		{
			g_dUserData[id][bDuoTalk] = !g_dUserData[id][bDuoTalk]
			client_cmd(id, "%svoicerecord", g_dUserData[id][bDuoTalk] ? "+" : "-")
			set_uc(uc_handle, UC_Impulse, 0)
		} 
	}
	
	if(g_dUserData[id][bKnockedOut] || g_dUserData[id][bIsOnPlane])
	{
		if((iButton & IN_ATTACK) || (iButton & IN_ATTACK2))
		{
			set_uc(uc_handle, UC_Buttons, (iButton & ~IN_ATTACK) & ~IN_ATTACK2)
		}
	}
	
	if(g_dUserData[id][bKnockedOut] && !g_dUserData[id][iHelperID])
	{
		if(!task_exists(id + TASK_REMOVEHEALTH))
		{
			set_task(g_dUserData[id][iKnocks] ? 1.0 : 2.0, "takeHealth", id + TASK_REMOVEHEALTH, .flags = "b")
		}

		if(get_user_weapon(id) != CSW_KNIFE)
		{
			client_cmd(id, "weapon_knife")
		}
		
		if(pev(id, pev_maxspeed) > KNOCK_SPEED)
		{
			set_pev(id, pev_maxspeed, KNOCK_SPEED)
		}
		
		if((fGameTime - g_dUserData[id][fUserGameTime]) >= (MESSAGE_DELAY * 2))
		{
			g_dUserData[id][fUserGameTime] = _:fGameTime
			CC_SendMatched(id, CC_COLOR_GREY, "^x03%L", id, "SERVER_YOURE_KNOCKED")
		}
	}
	
	if((iButton & IN_USE) && !g_dUserData[id][bKnockedOut])
	{
		new iTarget, iBody
		get_user_aiming(id, iTarget, iBody, HELP_DISTANCE)
		
		if(!is_user_connected(iTarget))
		{
			if(g_dUserData[id][iHelpingID])
			{
				cleanData(id, g_dUserData[id][iHelpingID])
			}

			if(g_dUserData[id][iMedKits] && (pev(id, pev_flags) & FL_ONGROUND))
			{
				if(!task_exists(id + TASK_MEDKIT))
				{
					if(get_user_health(id) >= MAX_HEALTH)
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) > MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_GREY, "%L", id, "SERVER_FULL_HEALTH")
							lockedSound(id)
						}
						return FMRES_IGNORED
					}
					new iCvarMedkitTime = get_pcvar_num(g_pCvars[cMedkitTime])
					set_task(float(iCvarMedkitTime), "startRegen", id + TASK_MEDKIT)
	
					clientPlaySound(id, g_szSoundsData[sUsingMedkit], true)
					barTime(id, .iTime = iCvarMedkitTime)
					return FMRES_IGNORED
				}
			}
			return FMRES_IGNORED
		}
			
		if(!g_dUserData[iTarget][bKnockedOut] || g_dUserData[iTarget][iHelperID])
			return FMRES_IGNORED

		if(g_dUserData[iTarget][iTeamMateID] != id)
		{
			if((fGameTime - g_dUserData[id][fUserGameTime]) > MESSAGE_DELAY)
			{
				g_dUserData[id][fUserGameTime] = _:fGameTime
				CC_SendMatched(id, CC_COLOR_GREY, "%L", id, "SERVER_ONLY_DUOS")
				lockedSound(id)
			}
			return FMRES_IGNORED
		}
			
		if(!task_exists(id + TASK_KNOCK))
		{
			g_dUserData[id][iHelpingID] = iTarget
			g_dUserData[iTarget][iHelperID] = id
		
			new Float:fHelpTime = get_pcvar_float(g_pCvars[cHelpTime])
			set_task(fHelpTime, "unknockMate", id + TASK_KNOCK)
			barTime(id, iTarget, floatround(fHelpTime))

			clientPlaySound(id, g_szSoundsData[sUnknockingMate], true)
		}
	}
	else
	{
		if(g_dUserData[id][iHelpingID])
		{
			cleanData(id, g_dUserData[id][iHelpingID])
		}
	}
	return FMRES_IGNORED
}

public startRegen(id)
{
	id -= TASK_MEDKIT

	ScreenFade(id, {20, 255, 20}, 105)
	set_user_health(id, clamp((get_user_health(id) + get_pcvar_num(g_pCvars[cMedkitHealth])), 0, MAX_HEALTH))
	
	g_dUserData[id][iMedKits]--
}

public unknockMate(id)
{
	id -= TASK_KNOCK

	clientPlaySound(g_dUserData[id][iTeamMateID], g_szSoundsData[sMateUnknocked], true)
	cleanData(id, g_dUserData[id][iTeamMateID], true)
}

public forward_setModeDescription() 
{ 
	forward_return(FMV_STRING, g_szGameName)
	return FMRES_SUPERCEDE
}

public ClientCommand_Radio(id) 
{ 
	return PLUGIN_HANDLED 
} 

public cmdItemIdList(id, iLevel, iCid)
{
	if(!cmd_access(id, iLevel, iCid, 1))
		return PLUGIN_HANDLED
		
	for(new i;i < sizeof g_szItens - 1;i++)
	{
		if(!g_szItens[iItensList:i][bIsItemEnabled])
			continue
				
		new szCmdItemName[25]
		getItemNameByID(id, i, szCmdItemName)
		console_print(id, "%d. ID - %d [%s]", (i + 1), g_szItens[iItensList:i][iItemID], szCmdItemName)
	}
	return PLUGIN_HANDLED
}

public cmdGiveItem(id, iLevel, iCid)
{
	if(!cmd_access(id, iLevel, iCid, 2))
		return PLUGIN_HANDLED

	new szTarget[MAX_PLAYERS], szItemID[3], szItemLevel[3], szItemLife[4]
	read_argv(1, szTarget, charsmax(szTarget))
	read_argv(2, szItemID, charsmax(szItemID))
	read_argv(3, szItemLevel, charsmax(szItemLevel))
	read_argv(4, szItemLife, charsmax(szItemLife))

	new iTarget = cmd_target(id, szTarget, CMDTARGET_NO_BOTS)
	if(!iTarget)
		return PLUGIN_HANDLED

	new iCmdItemID = str_to_num(szItemID)
	new iCmdItemLevel = clamp(str_to_num(szItemLevel), 1, 3)
	new iCmdItemLife = clamp(str_to_num(szItemLife), 1, 100)
	
	new szCmdItemName[25]
	getItemNameByID(id, iCmdItemID, szCmdItemName)
	
	if(!szCmdItemName[0])
	{
		console_print(id, "%L", LANG_PLAYER, "SERVER_CMD_INVALID_ITEM")
		return PLUGIN_HANDLED
	}

	giveWeapons(iTarget, 0, 0, iCmdItemID, iCmdItemLife, iCmdItemLevel)
	
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum)
	for(new i, iPlayer;i < iNum;i++)
	{
		iPlayer = iPlayers[i]
		
		getItemNameByID(iPlayer, iCmdItemID, szCmdItemName)
		CC_SendMatched(iPlayer, CC_COLOR_RED, "%L", iPlayer, "SERVER_ADMIN_GIVE_AN_ITEM_1", g_dUserData[id][szName], szCmdItemName, g_dUserData[iTarget][szName])
	}
	
	getItemNameByID(iTarget, iCmdItemID, szCmdItemName)
	CC_SendMatched(iTarget, CC_COLOR_RED, "%L", LANG_PLAYER, "SERVER_ADMIN_GIVE_AN_ITEM_2", g_dUserData[id][szName], szCmdItemName)
	return PLUGIN_HANDLED
}

public cmdWeaponsOriginsMenu(id, iLevel, iCid)
{
	if(!cmd_access(id, iLevel, iCid, 1))
		return PLUGIN_HANDLED

	showMenu(id, 0)
	return PLUGIN_HANDLED
}

public cmdPlaneOriginsMenu(id, iLevel, iCid)
{
	if(!cmd_access(id, iLevel, iCid, 1))
		return PLUGIN_HANDLED

	showMenu(id, 1)
	return PLUGIN_HANDLED
}

showMenu(id, iType)
{
	new szTittle[60], szItemFormat[60], szType[2]
	formatex(szTittle, charsmax(szTittle), "%L", id, "SERVER_POINTS_MENU")
	new iMenu = menu_create(szTittle, "weapons_point_handler")
	
	num_to_str(iType, szType, charsmax(szType))
	formatex(szItemFormat, charsmax(szItemFormat), "%L", id, "SERVER_ADD_POINT")
	menu_additem(iMenu, szItemFormat, szType)
	
	menu_display(id, iMenu)
}

public weapons_point_handler(id, iMenu, iItem)
{
	if(iItem == MENU_EXIT)
	{
		menu_destroy(iMenu)
		return
	}
	new szData[6], iAccess, item_callback
	menu_item_getinfo(iMenu, iItem, iAccess, szData, charsmax(szData), .callback = item_callback)
	
	new Float:fOrigin[3]
	pev(id, pev_origin, fOrigin)
	
	new szText[80], iData = str_to_num(szData), bIsPlaneVectors = bool:(iData == 1)
	if(bIsPlaneVectors)
	{
		new Float:fAngles[3], Float:fVelocity[3]
		pev(id, pev_angles, fAngles)
		velocity_by_aim(id, 300, fVelocity)
		formatex(szText, charsmax(szText), "%.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f", fOrigin[0], fOrigin[1], fOrigin[2], fAngles[0], fAngles[1], fAngles[2], fVelocity[0], fVelocity[1], fVelocity[2])
		copy(g_szText, charsmax(g_szText), szText)
		
		client_cmd(id, "messagemode plane_remove_time")
	}
	else
	{
		formatex(szText, charsmax(szText), "%.0f %.0f %.0f", fOrigin[0], fOrigin[1], fOrigin[2])
		write_file(g_szIniFile[iData + 1], szText)

		client_print(id, print_center, "%L", id, "SERVER_POINT_ADDED")
		menu_display(id, iMenu)
	}
}

public planeRemoveTime(id, iLevel, iCid)
{
	if(!cmd_access(id, iLevel, iCid, 1))
		return PLUGIN_HANDLED
		
	new szArgs[3]
	read_argv(1, szArgs, charsmax(szArgs))
	if(0 >= strlen(szArgs) >= 3)
	{
		CC_SendMatched(id, CC_COLOR_RED, "%L", id, "SERVER_WRONG_VALUE")
		client_cmd(id, "messagemode plane_remove_time")
		return PLUGIN_HANDLED
	}
	
	new szText[85]
	formatex(szText, charsmax(szText), "%s %s", g_szText, szArgs)
	write_file(g_szIniFile[2], szText)
	client_print(id, print_center, "%L", id, "SERVER_POINT_ADDED")
	showMenu(id, 1)
	return PLUGIN_HANDLED
}

public forward_AddToFullPack(es_handle, e, iEnt, iHost, hostflags, player, pSet)
{
	if (player && is_user_alive(iHost) && is_user_alive(iEnt) && get_pcvar_num(g_pCvars[cTeamMateEffect]) && g_dUserData[iEnt][iTeamMateID] == iHost)
	{
		set_es(es_handle, ES_RenderFx, kRenderFxDistort)
		set_es(es_handle, ES_RenderColor, {0, 0, 0})
		set_es(es_handle, ES_RenderMode, kRenderTransAdd)
		set_es(es_handle, ES_RenderAmt, 127)
	}
	return FMRES_IGNORED;
}

public message_SetFov(iMsgId, iMsgDest, id)
{
	return (is_user_alive(id) && isScopedWeapon(get_user_weapon(id)) && (0 < get_msg_arg_int(1) < 90)) ? PLUGIN_HANDLED : PLUGIN_CONTINUE
}

public message_SendAudio(iMsgId, iMsgDest, id)
{
	if(get_msg_block(g_mMessageTextMsg) == BLOCK_NOT)
	{
		set_msg_block(g_mMessageTextMsg, BLOCK_ONCE)
	}
	
	new szSound[20]
	get_msg_arg_string(2, szSound, charsmax(szSound))
	if(equal(szSound, "%!MRAD_FIREINHOLE"))
		return PLUGIN_HANDLED

	return PLUGIN_CONTINUE
}

public cmdDrop(id)
{
	if(!is_user_alive(id))
		return

	if((get_user_weapon(id) == CSW_KNIFE) && g_dUserData[id][bHasGuspe] && !g_dUserData[id][bKnockedOut])
	{
		if(pev(id, pev_waterlevel) > 1)
		{
			CC_SendMatched(id, CC_COLOR_RED, "%L", id, "SERVER_CANT_USE_UNDER_WATER")
			lockedSound(id)
			return
		}
		
		g_dUserData[id][bHasGuspe] = false
		
		screenFade(id, 1)
		set_pev(id, pev_flags, pev(id, pev_flags) | FL_FROZEN)
		clientPlaySound(id, g_szSoundsData[sGuspeSound], true)
		
		set_task(1.7, "upPlayer", id + TASK_UP_PLAYER)
			
		if(g_dUserData[id][bChoosedDuo])
		{
			new iTeammateIndex = g_dUserData[id][iTeamMateID]
			if(is_user_alive(iTeammateIndex))
			{
				new Float:fOrigin[3], Float:fMateOrigin[3]
				pev(id, pev_origin, fOrigin)
				pev(iTeammateIndex, pev_origin, fMateOrigin)
				if(get_distance_f(fOrigin, fMateOrigin) <= 40.0)
				{
					clientPlaySound(iTeammateIndex, g_szSoundsData[sGuspeSound], true)
					screenFade(iTeammateIndex, 1)
					
					set_task(1.7, "upPlayer", iTeammateIndex + TASK_UP_PLAYER)
				}
			}
		}
		return
	}
		
	new szWeapon[MAX_WEAPONNAME_LENGH], iWeapon
	read_argv(1, szWeapon, charsmax(szWeapon))
	iWeapon = szWeapon[0] ? get_weaponid(szWeapon) : get_user_weapon(id)

	new Float:fOrigin[3]
	pev(id, pev_origin, fOrigin)
	fOrigin[1] += 40.0
	
	if(g_dUserData[id][iScopedWeaponID] == iWeapon)
	{
		spawnItens(fOrigin, (g_dUserData[id][iScopeLevel] == 1) ? ITEM_SCOPE2X_ID : ITEM_SCOPE4X_ID, g_dUserData[id][iScopeLevel])

		g_dUserData[id][iScopedWeaponID] = 0
		g_dUserData[id][iScopeLevel] = 0
	}
	
	if(g_dUserData[id][iPenteAlongadoWeaponID] == iWeapon)
	{
		spawnItens(fOrigin, getPenteAlongadoID(id), g_dUserData[id][iPenteAlongadoLevel])
			
		g_dUserData[id][iPenteAlongadoWeaponID] = 0
		g_dUserData[id][iPenteAlongadoLevel] = 0
	}
}

public upPlayer(id)
{
	id -= TASK_UP_PLAYER

	g_dUserData[id][bCanUseParachute] = true
	
	screenFade(id, 0)
	set_pev(id, pev_flags, pev(id, pev_flags) & ~FL_FROZEN)
	clientPlaySound(id, g_szSoundsData[sSkyDiveSound])
	
	new Float:fOrigin[3]
	pev(id, pev_origin, fOrigin)
	
	fOrigin[2] = GetMaxHeightByStartOrigin(fOrigin)

	createBlast(fOrigin, 1000.0)
	engfunc(EngFunc_SetOrigin, id, fOrigin)
	
	new iPlayers[MAX_PLAYERS], playerCount
	get_players(iPlayers, playerCount, "ach")
	for(new i, iPlayer; i < playerCount; i++)
	{
		iPlayer = iPlayers[i]

		if(iPlayer != id)
		{
			clientPlaySound(iPlayer, g_szSoundsData[sGuspeAlertSound])
		}
	}
}

screenFade(id, iFade)
{
	message_begin(MSG_ONE, g_mMessageScreenFade, .player = id)
	write_short(2096)
	write_short(1024)
	write_short(iFade)
	write_byte(255)
	write_byte(255)
	write_byte(255)
	write_byte(255)
	message_end()
}

public cmdChatSayTeam(id)
{
	if(!g_dUserData[id][bChoosedDuo] || !is_user_connected(g_dUserData[id][iTeamMateID]))
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_ISNOT_IN_DUO")
		lockedSound(id)
		return PLUGIN_HANDLED
	}

	new szMessage[192]
	read_argv(1, szMessage, charsmax(szMessage))
	remove_quotes(szMessage)

	if(!szMessage[0])
		return PLUGIN_HANDLED

	CC_SendMatched(id, CC_COLOR_RED, "%L&x04 %s", id, "SERVER_DUOCHAT_SENDER", szMessage)
	CC_SendMatched(g_dUserData[id][iTeamMateID], CC_COLOR_RED, "%L &x04%s&x01 :&x03 %s", g_dUserData[id][iTeamMateID], "SERVER_DUOCHAT_RECEIVER", g_dUserData[id][szName], szMessage)
	return PLUGIN_HANDLED
}

public cmdChatSay(id)
{
	new szMessage[192]
	read_argv(1, szMessage, charsmax(szMessage))
	remove_quotes(szMessage)

	if(equal(szMessage, g_cCommands[cRank]))
	{
		cmdShowRank(id)
		return PLUGIN_HANDLED
	}
	else if(equal(szMessage, g_cCommands[cDuo]))
	{
		cmdDuo(id)
		return PLUGIN_HANDLED
	}
	else if(equal(szMessage, g_cCommands[cGetoutDuo]))
	{
		cmdOutDuo(id)
		return PLUGIN_HANDLED
	}
	else if(equal(szMessage, g_cCommands[cMainMenu]))
	{
		openPUBGMenu(id)
		return PLUGIN_HANDLED
	}
	else if(equal(szMessage, g_cCommands[cBag]))
	{
		if(!is_user_alive(id))
		{
			CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_MUST_BE_ALIVE")
			return PLUGIN_HANDLED
		}
		cmdBag(id)
		return PLUGIN_HANDLED
	}
	return PLUGIN_CONTINUE
}

openPUBGMenu(id)
{
	new szMenuTittle[60], szItemFmt[80]
	formatex(szMenuTittle, charsmax(szMenuTittle), "%L", id, "SERVER_MAIN_MENU_TITTLE", g_cCommands[cMainMenu])
	new iMenu = menu_create(szMenuTittle, "pubg_main_handler")
		
	formatex(szItemFmt, charsmax(szItemFmt), "%L", id, "SERVER_MAIN_MENU_VICTORIES")
	menu_additem(iMenu, szItemFmt)
	
	formatex(szItemFmt, charsmax(szItemFmt), "%L", id, "SERVER_MAIN_MENU_DUOS")
	menu_additem(iMenu, szItemFmt)
	
	formatex(szItemFmt, charsmax(szItemFmt), "%L", id, "SERVER_MAIN_MENU_DUOS_OUT")
	menu_additem(iMenu, szItemFmt)
	
	formatex(szItemFmt, charsmax(szItemFmt), "%L^n-----------------", id, "SERVER_MAIN_MENU_DROP")
	menu_additem(iMenu, szItemFmt)
		
	new szItem[75]
	formatex(szItem, charsmax(szItem), "%L", id, "SERVER_MAIN_MENU_AUTO_MENU", g_dUserData[id][bAutoMenu] ? "\y" : "\r", g_dUserData[id][bAutoMenu] ? "[ON]" : "[OFF]")
	menu_additem(iMenu, szItem)
	
	menu_display(id, iMenu)
}

public pubg_main_handler(id, iMenu, iItem)
{
	if(iItem == MENU_EXIT)
	{
		menu_destroy(iMenu)
		return
	}
	
	switch(iItem)
	{
		case 0, 1, 2, 3: 
		{
			client_cmd(id, "say ^"%s^"", g_cCommands[cCommandsData:iItem])
		}
		case 4:		
		{
			g_dUserData[id][bAutoMenu] = !g_dUserData[id][bAutoMenu]
			openPUBGMenu(id)
			return
		}
	}
	menu_destroy(iMenu)
}

public ham_FlashLight(id)
{
	if (is_user_alive(id) && (pev(id, pev_impulse) == 201))
	{
		set_pev(id, pev_impulse, 0)
		cmdBag(id)
		return HAM_HANDLED
	}
	return HAM_IGNORED
}

cmdBag(id)
{
	new szMenuTittle[60]
	formatex(szMenuTittle, charsmax(szMenuTittle), "%L", id, "SERVER_BAG_MENU")
	new iMenu = menu_create(szMenuTittle, "item_drop_handler")

	new CsArmorType:iArmorValue, szItemID[10], szItemFormat[25]
	if(cs_get_user_armor(id, iArmorValue))
	{
		num_to_str(getKevlarID(id), szItemID, charsmax(szItemID))

		formatex(szItemFormat, charsmax(szItemFormat), "%L lv. %d", id, "SERVER_ITEM_KEVLAR", g_dUserData[id][iArmorLevel])
		menu_additem(iMenu, szItemFormat, szItemID)
	}
		
	if(g_dUserData[id][iMedKits])
	{
		num_to_str(ITEM_MEDKIT_ID, szItemID, charsmax(szItemID))
		menu_additem(iMenu, "MedKit", szItemID)
	}
	
	if(g_dUserData[id][iHelmetEnt])
	{
		num_to_str(getUserHelmetID(id), szItemID, charsmax(szItemID))

		formatex(szItemFormat, charsmax(szItemFormat), "%L lv. %d", id, "SERVER_ITEM_HELMET", g_dUserData[id][iHelmetLevel])
		menu_additem(iMenu, szItemFormat, szItemID)
	}
	
	if(g_dUserData[id][iBagLevel])
	{
		num_to_str(getBagID(id), szItemID, charsmax(szItemID))

		formatex(szItemFormat, charsmax(szItemFormat), "%L lv. %d", id, "SERVER_ITEM_BAG", g_dUserData[id][iBagLevel])
		menu_additem(iMenu, szItemFormat, szItemID)
	}
	
	if(user_has_weapon(id, CSW_SMOKEGRENADE))
	{
		num_to_str(ITEM_IMPULSE_ID, szItemID, charsmax(szItemID))
		formatex(szItemFormat, charsmax(szItemFormat), "%L", id, "SERVER_ITEM_IMPULSE")
		menu_additem(iMenu, szItemFormat, szItemID)
	}
	
	if(g_dUserData[id][iScopeLevel])
	{	
		new bool:bIs2x = bool:(g_dUserData[id][iScopeLevel] == 1)
		num_to_str(bIs2x ? ITEM_SCOPE2X_ID : ITEM_SCOPE4X_ID, szItemID, charsmax(szItemID))
		
		formatex(szItemFormat, charsmax(szItemFormat), "%L %s", id, "SERVER_ITEM_SCOPE", bIs2x ? "2x" : "4x")
		menu_additem(iMenu, szItemFormat, szItemID)
	}
	
	if(g_dUserData[id][iPenteAlongadoWeaponID])
	{
		num_to_str(getPenteAlongadoID(id), szItemID, charsmax(szItemID))

		formatex(szItemFormat, charsmax(szItemFormat), "%L lv. %d", id, "SERVER_ITEM_ELONGATED_COMB", g_dUserData[id][iPenteAlongadoLevel])
		menu_additem(iMenu, szItemFormat, szItemID)
	}

	if(g_dUserData[id][bHasGuspe])
	{
		num_to_str(ITEM_GUSPE_ID, szItemID, charsmax(szItemID))
		menu_additem(iMenu, "Guspe", szItemID)
	}
	
	if(!szItemID[0])
	{
		num_to_str(-1, szItemID, charsmax(szItemID))
		formatex(szItemFormat, charsmax(szItemFormat), "%L", id, "SERVER_NO_ITEMS")
		menu_additem(iMenu, szItemFormat, szItemID)
	}
	
	menu_display(id, iMenu)
}

public item_drop_handler(id, iMenu, iItem)
{
	if(iItem == MENU_EXIT)
	{
		menu_destroy(iMenu)
		return
	}

	new Float:fOrigin[3], iLevel
	pev(id, pev_origin, fOrigin)
	fOrigin[1] += 40.0
	
	new szData[6], szItemName[MAX_PLAYERS]
	new _access, item_callback
	menu_item_getinfo(iMenu, iItem, _access, szData, charsmax(szData), szItemName, charsmax(szItemName), item_callback)
	
	new iMenuItemID = str_to_num(szData), Float:fArmorValue, bool:bIsKevlar
	switch(iMenuItemID)
	{
		case -1:
		{
			CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_NO_ITEMS")
			lockedSound(id)
			menu_display(id, iMenu)
			return 	
		}
		
		case ITEM_KEVLAR_1_ID, ITEM_KEVLAR_2_ID, ITEM_KEVLAR_3_ID:
		{
			new CsArmorType:iArmorValue
			if(!cs_get_user_armor(id, iArmorValue))
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			iLevel = g_dUserData[id][iArmorLevel]
			fArmorValue = entity_get_float(id, EV_FL_armorvalue)
			bIsKevlar = true
			
			cs_set_user_armor(id, 0, iArmorValue)
			g_dUserData[id][iArmorLevel] = 0
		}
		case ITEM_MEDKIT_ID:
		{
			if(!g_dUserData[id][iMedKits])
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			g_dUserData[id][iMedKits]--
		}
		case ITEM_HELMET_1_ID, ITEM_HELMET_2_ID, ITEM_HELMET_3_ID:
		{
			if(!g_dUserData[id][iHelmetEnt])
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			iLevel = g_dUserData[id][iHelmetLevel]
			removeHelmet(id)
		}
		case ITEM_BAG_1_ID, ITEM_BAG_2_ID, ITEM_BAG_3_ID:
		{
			if(!g_dUserData[id][iBagLevel])
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			iLevel = g_dUserData[id][iBagLevel]
			g_dUserData[id][iBagLevel] = 0
			
			for(new i;i < ((getUserItens(id) + getWeaponsNum(id)) - getUserMaxItens(id));i++)
			{
				dropItems(id, true)
			}
		}
		case ITEM_IMPULSE_ID:
		{
			if(!user_has_weapon(id, CSW_SMOKEGRENADE))
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			stripWeapon(id, "weapon_smokegrenade")
			spawnItens(fOrigin, ITEM_IMPULSE_ID, .szItemName = szItemName, .id = id)
			return
		}
		case ITEM_SCOPE2X_ID, ITEM_SCOPE4X_ID:
		{
			if(!g_dUserData[id][iScopeLevel])
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
		}
		case ITEM_PENTEALONGADO_1_ID, ITEM_PENTEALONGADO_2_ID, ITEM_PENTEALONGADO_3_ID:
		{
			if(!g_dUserData[id][iPenteAlongadoWeaponID])
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			iLevel = g_dUserData[id][iPenteAlongadoLevel]
			iItem += 1
			
			new szWeaponName[MAX_WEAPONNAME_LENGH], iWeaponID = g_dUserData[id][iPenteAlongadoWeaponID]
			get_weaponname(iWeaponID, szWeaponName, charsmax(szWeaponName))
			
			new iMaxClip = getWeaponDefaultClip(iWeaponID), csWeaponID = find_ent_by_owner(-1, szWeaponName, id), iClip = cs_get_weapon_ammo(csWeaponID)
			if(iClip > iMaxClip)
			{
				cs_set_weapon_ammo(csWeaponID, iMaxClip)
				cs_set_user_bpammo(id, iWeaponID, (cs_get_user_bpammo(id, iWeaponID) + (iClip - iMaxClip)))
			}

			g_dUserData[id][iPenteAlongadoLevel] = 0
			g_dUserData[id][iPenteAlongadoWeaponID] = 0
		}
		case ITEM_GUSPE_ID:
		{
			if(!g_dUserData[id][bHasGuspe])
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HAVENOT_THAT_ITEM")
				lockedSound(id)
				menu_display(id, iMenu)
				return 
			}
			
			g_dUserData[id][bHasGuspe] = false
		}
	}
	
	new bool:bIsScopeItem = bool:((iMenuItemID == ITEM_SCOPE2X_ID) || (iMenuItemID == ITEM_SCOPE4X_ID))
	spawnItens(fOrigin, bIsScopeItem ? (g_dUserData[id][iScopeLevel] == 1) ? ITEM_SCOPE2X_ID : ITEM_SCOPE4X_ID : iMenuItemID, iLevel, bIsKevlar ? fArmorValue : 0.0, szItemName, id)
		
	if(bIsScopeItem)
	{
		g_dUserData[id][iScopeLevel] = 0
		g_dUserData[id][iScopedWeaponID] = 0
	}
	cmdBag(id)
	menu_destroy(iMenu)
}

public ham_SmokeDeploy_Post(const iSmoke)
{
	static id;
#if defined _reapi_included
	id = get_member(iSmoke, m_pPlayer);
#else
	id = get_pdata_cbase(iSmoke, m_pPlayer, XTRA_OFS_WEAPON);
#endif
	if (is_user_connected(id))
	{
		set_pev(id, pev_viewmodel2, g_szModelsData[mImpulseGrenadeViewModel]);
		set_pev(id, pev_weaponmodel2, g_szModelsData[mImpulseGrenadePlayerModel]);
	}
}

public event_CurWeapon(id)
{
	if((read_data(2) != g_dUserData[id][iScopedWeaponID]) && g_dUserData[id][bIsFoved])
	{
		makeFov(id, 0)
	}
}

public forward_SetModel(iEnt, const szModel[])
{
	if (pev_valid(iEnt) && equal(szModel, "models/w_smokegrenade.mdl"))
	{
		engfunc(EngFunc_SetModel, iEnt, g_szItens[iImpulseGrenade][szItemModel])
		return FMRES_SUPERCEDE
	}
	return FMRES_IGNORED
}

public forward_EmitSound(iEnt, channel, const szSample[])
{
	if(equal(szSample, "common/wpn_denyselect.wav")) 
	{ 
		return FMRES_SUPERCEDE 
	} 

	if(equal(szSample, "player/headshot1.wav")
	|| equal(szSample, "player/headshot2.wav")
	|| equal(szSample, "player/headshot3.wav"))
	{ 
		return FMRES_SUPERCEDE 
	} 

	if(!equal(szSample, "weapons/sg_explode.wav") || !pev_valid(iEnt))
	{
		return FMRES_IGNORED
	}
	
	new Float:fEntOrigin[3], Float:fPlayerOrigin[3], Float:fRadius = get_pcvar_float(_:g_pCvars[cImpulseGrenRadius])
	pev(iEnt, pev_origin, fEntOrigin)
	
	createBlast(fEntOrigin, get_pcvar_float(_:g_pCvars[cImpulseGrenRadius]))
	emit_sound(iEnt, CHAN_ITEM, g_szSoundsData[sImpulseGrenadeSound], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)

	new iVictim = -1, iOwner = pev(iEnt, pev_owner)
	while((iVictim = engfunc(EngFunc_FindEntityInSphere, iVictim, fEntOrigin, fRadius)) != 0)
	{
		if(!is_user_alive(iVictim))
			continue
			
		pev(iVictim, pev_origin, fPlayerOrigin)
		impulsePlayer(iVictim, iEnt, iOwner, (fRadius - (get_distance_f(fEntOrigin, fPlayerOrigin) * 0.0254)))
	}
	set_pev(iEnt, pev_flags, pev(iEnt, pev_flags) | FL_KILLME)
	return FMRES_SUPERCEDE
}

public forward_PlaybackEvent(Flags, id, m_usFireEvent, Float:Delay, Float:Origin[3], Float:Angles[3], Float:DirectionX, Float:DirectionY, PunchAngleX, PunchAngleY, bool:Dummy1, bool:Dummy2)
{
	if(m_usEventSmokeGrenade == m_usFireEvent)
	{
		return FMRES_SUPERCEDE
	}
	return FMRES_IGNORED
}  

public forward_touchItem(iTouched, id)
{	
	if(pev_valid(id) 
	&& get_pcvar_num(g_pCvars[cImpulseGrenadeImpact])
	&& isSolid(iTouched) 
	&& isAvaliableGrenade(id))
	{
		explodeGrenade(id)
					
		entity_set_float(id, EV_FL_nextthink, get_gametime() + 0.001)
		entity_set_int(id, EV_INT_flags, entity_get_int(id, EV_INT_flags) | FL_ONGROUND)
	}
	
	static szModel[MAX_MODEL_LENGH]
	pev(iTouched, pev_model, szModel, charsmax(szModel))
	if(equal(szModel, "models/w_backpack.mdl"))
	{
		killEntity(iTouched)
		return FMRES_IGNORED
	}
	
	if(!pev_valid(iTouched) || !is_user_connected(id) || g_dUserData[id][bKnockedOut]) 
	{
		return FMRES_IGNORED
	}
	
	static szClassName[MAX_PLAYERS]
	pev(iTouched, pev_classname, szClassName, charsmax(szClassName))
	if(equal(szClassName, g_szClassNamesData[cWeapons]))
	{
		new bool:bContainWeaponModel = true
		for(new i;i < sizeof g_szItens;i++)
		{
			if(equal(szModel, g_szItens[iItensList:i][szItemModel]))
			{
				bContainWeaponModel = false
			}
		}

		giveWeapons(id, bContainWeaponModel, iTouched)
	}
	return FMRES_IGNORED
}

public ham_PlayerSpawn(id)
{
	if(is_user_alive(id))
	{		
		remove_task(id + TASK_REMOVEHEALTH)
		g_dUserData[id][bKnockedOut] = false
		
		if(g_bPlaneSpawned && !g_dUserData[id][bIsOnPlane])
		{
			user_kill(id, 1)
			CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_REMOVED_FROM_ROUND")
		}
		
		g_dUserData[id][bIsOnPlane] = false
		g_dUserData[id][bCanUseParachute] = false
		g_dUserData[id][bIsFoved] = false
		g_dUserData[id][bAllowedToSeeInfos] = false
		g_dUserData[id][bIsAirDropMenuOpen] = false
		g_dUserData[id][bHasGuspe] = false

		g_dUserData[id][iPenteAlongadoWeaponID] = 0
		g_dUserData[id][iScopedWeaponID] = 0
		g_dUserData[id][iKnocks] = 0
		g_dUserData[id][iHelpingID] = 0
		g_dUserData[id][iHelperID] = 0
		g_dUserData[id][iKills] = 0
		g_dUserData[id][iKnockerID] = 0
		g_dUserData[id][iMedKits] = 0
		
		g_dUserData[id][iPenteAlongadoLevel] = 0
		g_dUserData[id][iArmorLevel] = 0
		g_dUserData[id][iHelmetLevel] = 0
		g_dUserData[id][iBagLevel] = 0
		g_dUserData[id][iScopeLevel] = 0 
	
		set_user_armor(id, 0)
		strip_user_weapons(id)
		give_item(id, "weapon_knife")
		client_cmd(id, "drop weapon_c4")

		checkTasks(id)
		removeHelmet(id)
		resetUserSpeed(id)

		airDropMenu(id, false)
		setDamageble(id, false)
		setInvisibility(id, false)
		
		if(g_dUserData[id][bAutoMenu])
		{
			openPUBGMenu(id)
		}
	}
}

public message_StatusIcon(iMsgId, iMsgDest, id)
{
	static szIcon[5];
	get_msg_arg_string(2, szIcon, charsmax(szIcon))

	if ((szIcon[0] == 'b') && (szIcon[2] == 'y') && (szIcon[3] == 'z'))
	{
#if defined _reapi_included
		set_member(id, m_signals, get_member(id, m_signals, US_State) & ~(1<<0), US_State); // O era US_Signal?
#else
		const offsetBuyZone = 235;
		set_pdata_int(id, offsetBuyZone, get_pdata_int(id, offsetBuyZone) & ~(1<<0));
#endif
		return PLUGIN_HANDLED
	}

	return PLUGIN_CONTINUE
}

public ham_PlayerTakeDamage(iVictim, iInflictor, iAttacker, Float:fDamage, iDamageBits)
{
	if (!is_user_connected(iVictim) || g_iFreezeTime && !g_bPlaneSpawned)
		return HAM_IGNORED
		
	new bool:bIsAttackerConnected = bool:is_user_connected(iAttacker)
	if(bIsAttackerConnected && (g_dUserData[iAttacker][iTeamMateID] == iVictim))
	{
		CC_SendMatched(iAttacker, CC_COLOR_RED, "%L", iAttacker, "SERVER_YOUR_TEAMMATE")
		SetHamReturnInteger(0)
		return HAM_SUPERCEDE
	}
		
	if((iVictim != iAttacker) && bIsAttackerConnected)
	{
		clientPlaySound(iVictim, g_szSoundsData[sDamagePain])

		new iTeam = fm_get_user_team(iVictim), bool:bIsAWP = bool:(iDamageBits & DMG_AWP)
		if(iTeam == fm_get_user_team(iAttacker))
		{
			fm_set_user_team(iVictim, (iTeam == 1) ? 2 : 1)
			if(pev_valid(g_dUserData[iVictim][iHelmetEnt]) && isHeadShot(iVictim))
			{
				ExecuteHamB(Ham_TakeDamage, iVictim, iInflictor, iAttacker, bIsAWP ? fDamage : (fDamage / (g_dUserData[iVictim][iHelmetLevel] + 1)), iDamageBits)
				emit_sound(iVictim, CHAN_ITEM, g_szSoundsData[sHelmetHit], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
			
				checkHeadHits(iVictim, g_dUserData[iVictim][iHelmetLevel])
			}
			else
			{
				ExecuteHamB(Ham_TakeDamage, iVictim, iInflictor, iAttacker, bIsAWP ? fDamage : (fDamage / 1.8), iDamageBits)
			}
			fm_set_user_team(iVictim, iTeam)
			return HAM_SUPERCEDE
		}
		else
		{
			if(isHeadShot(iVictim) && floatround(fDamage) < get_user_health(iVictim))
			{
				clientPlaySound(iAttacker, g_szSoundsData[sCriticalHit])
			}
		}
	}
	
	new iUserTeamMateID = g_dUserData[iVictim][iTeamMateID]
	if (bIsAttackerConnected)
	{
		g_dUserData[iAttacker][iPlayerWeaponID] = (iInflictor != iAttacker) ? CSW_HEGRENADE : get_user_weapon(iAttacker)
	}

	if(floatround(fDamage) >= get_user_health(iVictim)
	&& !g_dUserData[iVictim][bKnockedOut] 
	&& g_dUserData[iVictim][bChoosedDuo]
	&& is_user_alive(iUserTeamMateID)
	&& !g_dUserData[iUserTeamMateID][bKnockedOut])
	{
		set_task(g_dUserData[iVictim][iKnocks] ? 1.0 : 2.0, "takeHealth", iVictim + TASK_REMOVEHEALTH, .flags = "b")
		set_user_health(iVictim, 100)
		
		clientPlaySound(iVictim, g_szSoundsData[sMateKnocked])
		clientPlaySound(iUserTeamMateID, g_szSoundsData[sMateKnocked])

		g_dUserData[iVictim][bKnockedOut] = true
		g_dUserData[iVictim][iWeaponKnocker] = (bIsAttackerConnected ? g_dUserData[iAttacker][iPlayerWeaponID] : 0);
		g_dUserData[iVictim][iKnockerID] = (bIsAttackerConnected ? iAttacker : 0); // 822: Changed -1 to 0
		g_dUserData[iVictim][iKnocks]++
	
		new szKillMessage[2][150]
		if(bIsAttackerConnected)
		{
			clientPlaySound(iAttacker, g_szSoundsData[sKnockedAnEnemy])

			new szWeaponName[MAX_WEAPONNAME_LENGH]
			changeWeaponName(g_dUserData[iAttacker][iPlayerWeaponID], szWeaponName)
			
			set_dhudmessage(220, 255, 220, -1.0, 0.65, 0, 1.0, 2.0)
			show_dhudmessage(iAttacker, "%L", iAttacker, "SERVER_YOU_KNOCKED_HUD", g_dUserData[iVictim][szName], szWeaponName)
			CC_SendMatched(iAttacker, CC_COLOR_RED, "%L", iAttacker, "SERVER_YOU_KNOCKED_MESSAGE", g_dUserData[iVictim][szName], szWeaponName)
		
			formatex(szKillMessage[0], charsmax(szKillMessage[]), "%L", iVictim, "SERVER_KNOCKED_VICTIM_HUD", g_dUserData[iAttacker][szName], szWeaponName)
			formatex(szKillMessage[1], charsmax(szKillMessage[]), "^x03%L", iVictim, "SERVER_KNOCKED_VICTIM_MESSAGE", g_dUserData[iAttacker][szName], szWeaponName)
		}
		else
		{
			formatex(szKillMessage[0], charsmax(szKillMessage[]), "%L", iVictim, "SERVER_KNOCKED_BY_FALLING")
			formatex(szKillMessage[1], charsmax(szKillMessage[]), "%L", iVictim, "SERVER_KNOCKED_BY_FALLING")
		}
		set_dhudmessage(220, 255, 220, -1.0, 0.65, 0, 1.0, 2.0)
		show_dhudmessage(iVictim, szKillMessage[0])

		CC_RemoveColors(szKillMessage[1], charsmax(szKillMessage[]))
		CC_SendMatched(iVictim, CC_COLOR_RED, szKillMessage[1])
		
		SetHamReturnInteger(0)
		return HAM_SUPERCEDE
	}
	return HAM_IGNORED
}

public ham_PlayerKilled_Pre(iVictim, iKiller, iShouldGib)
{
	if(!is_user_connected(iVictim))
		return HAM_IGNORED

	if(isHeadShot(iVictim))
	{
		clientPlaySound(iKiller, g_szSoundsData[sCriticalKill])
	}

	airDropMenu(iVictim, false)
	if((iVictim != iKiller) && is_user_connected(iKiller))
	{
		new iTeam = fm_get_user_team(iVictim)
		if(iTeam == fm_get_user_team(iKiller))
		{
			fm_set_user_team(iVictim, (iTeam == 1) ? 2 : 1)
			ExecuteHamB(Ham_Killed, iVictim, iKiller, iShouldGib)
			fm_set_user_team(iVictim, iTeam)
			return HAM_SUPERCEDE
		}
	}
	
	if(!roundEnded())
	{
		clientPlaySound(iVictim, g_szSoundsData[sDeathSound], true)
		dropItems(iVictim)

		new szWeapons[MAX_PLAYERS], iNum
		get_user_weapons(iVictim, szWeapons, iNum)
		for(new i, iWeaponID, szWeaponName[MAX_WEAPONNAME_LENGH];i < iNum;i++)
		{
			iWeaponID = szWeapons[i]
			
			if((1 << iWeaponID) & CONST_INVALID_WEAPONS)
				continue
				
			get_weaponname(iWeaponID, szWeaponName, charsmax(szWeaponName))
			client_cmd(iVictim, "drop %s %d", szWeaponName, cs_get_user_bpammo(iVictim, iWeaponID))
		}
	}
	
	if(g_dUserData[iVictim][iHelmetLevel])
	{
		removeHelmet(iVictim)
	}

	// Really Needed?
	if (g_iParachuteEnt[iVictim] > 0) 
	{
		remove_entity(g_iParachuteEnt[iVictim]);
		set_user_gravity(iVictim, 1.0);
		g_iParachuteEnt[iVictim] = 0;
	}

	if(g_bHasRoundEnded)
		return HAM_IGNORED
	
	new iArg[2]
	iArg[0] = iVictim
	iArg[1] = iKiller
	
	set_task(0.1, "ham_ExecuteThings", _, iArg, sizeof iArg)
	return HAM_IGNORED
}

public ham_ExecuteThings(iArg[2])
{
	new iVictim = iArg[0], iKiller = iArg[1], iTeammateIndex = g_dUserData[iVictim][iTeamMateID]
	if(!is_user_connected(iKiller))
	{
		checkSuicide(iTeammateIndex)
		displayMessages(0, iVictim)
		return
	}
	
	if(task_exists(iVictim + TASK_MEDKIT))
	{
		remove_task(iVictim + TASK_MEDKIT)
		barTime(iVictim, .iTime = 0)
	}
	
	new bool:bIsSuicide = bool:(iKiller == iVictim)
	if(bIsSuicide && !g_dUserData[iVictim][iKnockerID])
	{
		checkSuicide(iTeammateIndex)
		displayMessages(0, iVictim)
	}
	else
	{
		new szWeaponName[MAX_WEAPONNAME_LENGH]
		if(g_dUserData[iTeammateIndex][bKnockedOut])
		{
			knockKillManage(iKiller, iTeammateIndex)
		}
		
		if(g_dUserData[iVictim][bKnockedOut])
		{
			removeSomeKnockInfo(iVictim)
			if(iKiller != g_dUserData[iVictim][iKnockerID])
			{
				if(is_user_connected(g_dUserData[iVictim][iKnockerID]) && (g_dUserData[iVictim][iKnockerID] != iVictim))
				{
					changeWeaponName(g_dUserData[iKiller][iPlayerWeaponID], szWeaponName)
					displayMessages(1, iKiller, iVictim, szWeaponName)
					return
				}
				else
				{
					g_dUserData[iVictim][iKnockerID] = iKiller
				}
			}
		}
		
		changeWeaponName(g_dUserData[iKiller][iPlayerWeaponID], szWeaponName)
		displayMessages(bIsSuicide ? 0 : 1, iKiller, iVictim, szWeaponName)
	}
}

public takeHealth(id)
{
	id -= TASK_REMOVEHEALTH

	if(g_dUserData[id][iHelperID])
		return 
	
	new iHPToTake = (g_dUserData[id][iKnocks] * 2), iUserHealth = get_user_health(id)
	if(iHPToTake >= iUserHealth)
	{
		is_user_connected(g_dUserData[id][iKnockerID]) ? executeKill(id, g_dUserData[id][iKnockerID]) : user_kill(id, 1)
		remove_task(id + TASK_REMOVEHEALTH)
		
		client_cmd(id, "stopsound")
		cleanData(g_dUserData[id][iHelperID], id)
		return
	}
	
	startBleeding(id)
	set_user_health(id, (iUserHealth - iHPToTake))
}

public planeDrop()
{
	if (g_bHasRoundEnded || getRedZoneLevel() >= 2)
		return
		
	new Float:fOrigin[3], Float:fAngle[3], Float:fVelocity[3]
	getRandomPlaneDirection(fOrigin, fAngle, fVelocity)
	
	new iEnt = create_entity("info_target")
	fmCreatePlane
	(
		iEnt, 
		g_szClassNamesData[cTransporter], 
		g_szModelsData[mAirDropPlane], 
		fOrigin, 
		fVelocity, 
		fAngle
	)

	g_iEnt[pPlaneAirDrop] = iEnt
	clientPlaySound(0, g_szSoundsData[sJetFlyBy])
	
	set_task(7.0, "jetFlyBySound")
	set_task(3.0, "dropBox", TASK_AIRDROP, .flags = "b")
	set_task(g_fPlaneRemoveTime[g_iRandomDirection], "removeAirDropPlane", TASK_PLANE)
}

public removeAirDropPlane()
{
	if(task_exists(TASK_AIRDROP))
	{
		killAirDropPlane()
		remove_task(TASK_AIRDROP)
	}
}

public jetFlyBySound()
{
	if(task_exists(TASK_AIRDROP))
	{
		clientPlaySound(0, g_szSoundsData[sJetFlyBy])
		set_task(7.0, "jetFlyBySound")
	}
}

public dropBox()
{
	if (isInRedZone(g_iEnt[pPlaneAirDrop]))
		return
	
	if(random_num(0, AIRDROP_CHANCE) == AIRDROP_CHANCE)
	{
		new iBox = create_entity("info_target")
		if(pev_valid(iBox))
		{
			g_iEnt[iAirDropID] = iBox
			
			for(new i;i < sizeof g_iAirDropItems;i++)
			{
				g_iAirDropItems[i] = 1
			}

			remove_task(TASK_PLANE)
			remove_task(TASK_AIRDROP)
		
			new Float:fOrigin[3]
			pev(g_iEnt[pPlaneAirDrop], pev_origin, fOrigin)
				
			engfunc(EngFunc_SetOrigin, iBox, fOrigin)
			engfunc(EngFunc_SetModel, iBox, g_szModelsData[mAirDrop])
					
			set_pev(iBox, pev_gravity, 0.1)
			set_pev(iBox, pev_movetype, MOVETYPE_TOSS)
			set_pev(iBox, pev_solid, SOLID_TRIGGER)
			set_pev(iBox, pev_classname, g_szClassNamesData[cAirDrop])
			
			clientPlaySound(0, g_szSoundsData[sAirDropAppearSound])
			clientPlaySound(iBox, g_szSoundsData[sAirDropLandingSound], true)
			
			new iBoxParachute = create_entity("info_target")
			if(pev_valid(iBoxParachute))
			{
				set_pev(iBoxParachute, pev_movetype, MOVETYPE_FOLLOW)
				set_pev(iBoxParachute, pev_aiment, iBox)
				engfunc(EngFunc_SetModel, iBoxParachute, g_szModelsData[mAirDropParachute])
				entity_set_int(iBoxParachute, EV_INT_sequence, 3)
				entity_set_edict(iBoxParachute, EV_ENT_owner, iBox)
				entity_set_int(iBoxParachute, EV_INT_gaitsequence, 1)
				entity_set_float(iBoxParachute, EV_FL_frame, 1.0)
				entity_set_float(iBoxParachute, EV_FL_framerate, 1.0)
	
				set_task(4.0, "killBoxParachute", iBoxParachute)
			}
		}
		set_task(3.0, "killAirDropPlane")
	}
}

public killAirDropPlane()
{
	killEntity(g_iEnt[pPlaneAirDrop])
	g_iEnt[pPlaneAirDrop] = 0
}

public killBoxParachute(iEnt)
{
	new iBox = pev(iEnt, pev_owner)
	if(pev_valid(iBox) && pev(iBox, pev_flags) & FL_ONGROUND)
	{
		killEntity(iEnt)
		showSmoke()
		return
	}
	set_task(1.0, "killBoxParachute", iEnt)
}

public restartRound(iTime)
{
#if defined _reapi_included
	rg_round_end(float(iTime), WINSTATUS_DRAW, ROUND_END_DRAW);
#else
	server_cmd("sv_restartround %d", iTime);
#endif
}

public createPlane()
{
	ClearSyncHud(0, g_hHuds[hFreezeTimeEnd])
	
	new Float:fOrigin[3], Float:fAngle[3], Float:fVelocity[3]
	getRandomPlaneDirection(fOrigin, fAngle, fVelocity)

	new iEnt = create_entity("info_target")
	fmCreatePlane
	(
		iEnt, 
		g_szClassNamesData[cTransporter], 
		g_szModelsData[mTransporter], 
		fOrigin,
		fVelocity,
		fAngle
	)
	
	g_bPlaneSpawned = true
	g_iEnt[pPlaneTransporter] = iEnt
	fOrigin[2] -= 40.0
	
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ach")
	for(new i, id;i < iNum;i++)
	{
		id = iPlayers[i]
		
		setInvisibility(id, true)
		clientPlaySound(id, g_szSoundsData[sTransporter])
		
		set_pev(id, pev_origin, fOrigin)
		set_pev(id, pev_angles, fAngle)
		set_pev(id, pev_fixangle, 1)
		set_pev(id, pev_velocity, fVelocity)
		set_pev(id, pev_gravity, 0.000001)
		set_pev(id, pev_maxspeed, 0.000001)

		g_dUserData[id][bIsOnPlane] = true
		g_dUserData[id][bCanUseParachute] = false
	}
	
	set_task(get_pcvar_float(_:g_pCvars[cAllowToJumpTime]), "allowJump")
	set_task(g_fPlaneRemoveTime[g_iRandomDirection], "checkPlaneOrigin", TASK_PLANE)
}

public allowJump()
{
	g_bCanJump = true
}

public checkPlaneOrigin()
{
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ach")
	for(new i, id;i < iNum;i++)
	{
		id = iPlayers[i]
			
		if(g_dUserData[id][bIsOnPlane])
		{
			dropPlayer(id)
		}
	}
	
	killEntity(g_iEnt[pPlaneTransporter])
	g_iEnt[pPlaneTransporter] = 0
}

public cmdShowRank(id)
{
	new szMessage[50]
	switch(g_dUserData[id][iVitorias])
	{
		case 0: 	formatex(szMessage, charsmax(szMessage), "%L", id, "SERVER_NO_WINS")
		case 1: 	formatex(szMessage, charsmax(szMessage), "%L", id, "SERVER_WON_ONCE")
		default:	formatex(szMessage, charsmax(szMessage), "%L", id, "SERVER_WON_TWICE", g_dUserData[id][iVitorias])
	}
	CC_SendMatched(id, CC_COLOR_GREY, "%s", szMessage)
	return PLUGIN_HANDLED
}

public cmdOutDuo(id)
{
	if(!g_dUserData[id][bChoosedDuo])
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_ISNOT_IN_DUO")
		return PLUGIN_HANDLED
	}

	CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_OUT_OF_DUO_1")
	CC_SendMatched(g_dUserData[id][iTeamMateID], CC_COLOR_RED, "^x03%L", g_dUserData[id][iTeamMateID], "SERVER_GOT_OUT_OF_DUO_2")
	
	resetDuos(id)
	return PLUGIN_HANDLED
}

public cmdDuo(id)
{
	if(!g_iFreezeTime)
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_DUO_IN_COUNTDOWN")
		lockedSound(id)
		return PLUGIN_HANDLED
	}
	
	new iPlayers[MAX_PLAYERS], playerCount
	get_players(iPlayers, playerCount, "ch")
	if(playerCount < MIN_PLAYERS_DUO)
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_DUO_MIN_PLAYERS", MIN_PLAYERS_DUO)
		lockedSound(id)
		return PLUGIN_HANDLED
	}
	
	if(!isTeam(id))
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_DUO_MUST_PLAY")
		lockedSound(id)
		return PLUGIN_HANDLED
	}

	if(g_dUserData[id][bChoosedDuo])
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_IN_DUO_ALREADY")
		lockedSound(id)
		return PLUGIN_HANDLED
	}
	
	new szMenuTittle[60]
	formatex(szMenuTittle, charsmax(szMenuTittle), "%L", id, "SERVER_PLAYERS_MENU")
	new iMenu = menu_create(szMenuTittle, "choose_players_handler")
	
	for(new i, iPlayer; i < playerCount; i++)
	{
		iPlayer = iPlayers[i]
		
		if((iPlayer == id) || !isTeam(iPlayer))
			continue
			
		menu_additem(iMenu, g_dUserData[iPlayer][szName])
	}
	menu_display(id, iMenu)
	return PLUGIN_HANDLED
}

public choose_players_handler(id, iMenu, iItem)
{
	if((iItem == MENU_EXIT) || !isTeam(id))
	{
		menu_destroy(iMenu)
		return
	}

	static szData[6], szItemName[MAX_MODEL_LENGH]
	new _access, item_callback
	menu_item_getinfo(iMenu, iItem, _access, szData,charsmax(szData), szItemName, charsmax(szItemName), item_callback)
	
	new iTarget = find_player("a", szItemName)
	if(!iTarget || !isTeam(iTarget))
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_INVALID_PLAYER")
		lockedSound(id)
		menu_display(id, iMenu)
		return
	}
	
	if(g_dUserData[iTarget][bChoosedDuo])
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_PLAYER_ALREADY_IN_DUO")
		lockedSound(id)
		menu_display(id, iMenu)
		return
	}

	inviteMenu(id, iTarget)
	CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_PLAYER_INVITED")
}

inviteMenu(id, iTarget)
{
	g_dUserData[iTarget][iInviter] = id
	
	new iMenu, szTittle[80], szItemFmt[15]
	formatex(szTittle, charsmax(szTittle), "%s %L", g_dUserData[id][szName], iTarget, "SERVER_INVITE_MENU")
	iMenu = menu_create(szTittle, "invite_handler")
	
	formatex(szItemFmt, charsmax(szItemFmt), "\y%L", iTarget, "SERVER_YES")
	menu_additem(iMenu, szItemFmt)
	
	formatex(szItemFmt, charsmax(szItemFmt), "\r%L", iTarget, "SERVER_NO")
	menu_additem(iMenu, szItemFmt)

	menu_display(iTarget, iMenu)
}

public invite_handler(id, iMenu, iItem)
{
	if(iItem == MENU_EXIT)
	{
		menu_destroy(iMenu)
		return
	}
	
	new iInviterID = g_dUserData[id][iInviter]
	if(g_dUserData[iInviterID][bChoosedDuo])
	{
		CC_SendMatched(iInviterID, CC_COLOR_GREY, "%L", iInviterID, "SERVER_PLAYER_IS_ALREADY_IN_DUO", g_dUserData[iInviterID][szName])
		return
	}
	
	g_dUserData[id][iTeamMateID] = iInviterID
		
	if(!iItem)
	{
		g_dUserData[iInviterID][iTeamMateID] = id
		g_dUserData[iInviterID][bChoosedDuo] = true
		g_dUserData[id][bChoosedDuo] = true
		
		CC_SendMatched(iInviterID, CC_COLOR_GREY, "%L", iInviterID, "SERVER_ACCEPTED_DUO_INVITER", g_dUserData[id][szName])
		CC_SendMatched(id, CC_COLOR_GREY, "%L", id, "SERVER_ACCEPTED_DUO_INVITED", g_dUserData[iInviterID][szName])
	}
	else
	{
		CC_SendMatched(iInviterID, CC_COLOR_GREY, "%L", iInviterID, "SERVER_REFUSED_DUO_INVITER", g_dUserData[id][szName])
		CC_SendMatched(id, CC_COLOR_GREY, "%L", id, "SERVER_REFUSED_DUO_INVITED", g_dUserData[iInviterID][szName])
	
		g_dUserData[id][iTeamMateID] = g_dUserData[id][iInviter] = 0
	}
}

showOnRadar(idToShow, idToGetOrigin)
{
	new iOrigins[3]
	get_user_origin(idToGetOrigin, iOrigins)
		
	message_begin(MSG_ONE_UNRELIABLE, g_mMessageHostagePos, .player = idToShow)
	write_byte(idToShow)
	write_byte(idToGetOrigin)		
	write_coord(iOrigins[0])
	write_coord(iOrigins[1])
	write_coord(iOrigins[2])
	message_end()
		
	message_begin(MSG_ONE_UNRELIABLE, g_mMessageHostageK, .player = idToShow)
	write_byte(idToGetOrigin)
	message_end()
}

setInvisibility(id, bool:bInvisible)
{
	set_user_rendering(id, kRenderFxNone, 0, 0, 0, bInvisible ? kRenderTransAlpha : kRenderNormal, 0)
}

barTime(id, iTarget = 0, iTime)
{
	if(is_user_connected(iTarget))
	{
		message_begin(MSG_ONE, g_mMessageBarTime, .player = iTarget)
		write_short(iTime)
		message_end()  
	}

	if(is_user_connected(id))
	{
		message_begin(MSG_ONE, g_mMessageBarTime, .player = id)
		write_short(iTime)
		message_end() 
	}
}

createBlast(Float:fStartOrigin[3], Float:fValue)
{
	message_begin_fl(MSG_PVS, SVC_TEMPENTITY, fStartOrigin, 0)
	write_byte(TE_BEAMCYLINDER)
	write_coord_fl(fStartOrigin[0])
	write_coord_fl(fStartOrigin[1])
	write_coord_fl(fStartOrigin[2])
	write_coord_fl(fStartOrigin[0])
	write_coord_fl(fStartOrigin[1])
	write_coord_fl(fStartOrigin[2] + fValue)
	write_short(g_iExplosionSprite)
	write_byte(0)
	write_byte(3)
	write_byte(10)
	write_byte(80)
	write_byte(0)
	write_byte(255)
	write_byte(255)
	write_byte(255)
	write_byte(100)
	write_byte(0)
	message_end()
}

// Code took from Weapon Physics edited by me.
impulsePlayer(iVictim, iInflictor, iAttacker, Float:fDamage)
{
	static Float:fPlayerVelocity[3], Float:fPlayerOrigin[3], Float:fGrenadeOrigin[3], Float:fTemp[3]
	pev(iVictim, pev_velocity, fPlayerVelocity)
	pev(iVictim, pev_origin, fPlayerOrigin)
	pev(iInflictor, pev_origin, fGrenadeOrigin)
	xs_vec_sub(fPlayerOrigin, fGrenadeOrigin, fTemp)
	xs_vec_normalize(fTemp, fTemp)
	xs_vec_mul_scalar(fTemp, fDamage, fTemp)
	xs_vec_mul_scalar(fTemp, get_pcvar_float(g_pCvars[cImpulseMultiplier]), fTemp)
	xs_vec_add(fPlayerVelocity, fTemp, fPlayerVelocity)
	set_pev(iVictim, pev_velocity, fPlayerVelocity)

	static Float:fAVelocity[3]
	fAVelocity[1] = random_float(-1000.0, 1000.0)
	set_pev(iVictim, pev_avelocity, fAVelocity)
	
	new Float:fDamageTaken = (fDamage / 22.5)
	if(floatround(fDamageTaken) >= get_user_health(iVictim))
	{
		if(iAttacker != iVictim)
		{
			executeKill(iVictim, iAttacker)
			g_dUserData[iAttacker][iPlayerWeaponID] = CSW_SMOKEGRENADE
		}
		else
		{
			user_silentkill(iAttacker)
			return
		}
	}
	else	setBeamFollow(iVictim, {255, 255, 255}, 100)

	ExecuteHam(Ham_TakeDamage, iVictim, iInflictor, iAttacker, fDamageTaken, DMG_HE)
}

displayMessages(iType, iAttacker, iVictim = 0, szWeaponName[MAX_WEAPONNAME_LENGH] = "")
{
	new iPlayers[MAX_PLAYERS], iNum, szLeft[23]
	get_players(iPlayers, iNum)

	if(!iType)
	{
		new iVictimID = iAttacker ? iAttacker : iVictim
		new szEliminated[20], szRedZoneDeath[35]
		for(new i, iPlayer;i < iNum;i++)
		{
			iPlayer = iPlayers[i]

			formatex(szEliminated, charsmax(szEliminated), "%L", iPlayer, "SERVER_ELIMINATED")
			formatex(szRedZoneDeath, charsmax(szRedZoneDeath), "%L", iPlayer, "SERVER_ELIMINATE_REDZONE")
			
			getLeftMessage(iPlayer, szLeft)
			CC_SendMatched(iPlayer, CC_COLOR_RED, "%L", iPlayer, "SERVER_SELF_DEATH", g_dUserData[iVictimID][szName], isInRedZone(iVictimID) ? szRedZoneDeath : szEliminated, szLeft)
		}
		checkNum()
		return
	}
	
	new bool:bShowHudKills = true
	if(g_dUserData[iVictim][iKnockerID] && (iAttacker != g_dUserData[iVictim][iKnockerID]))
	{
		bShowHudKills = false
	}
	else
	{
		g_dUserData[iAttacker][iKills]++
	}
	
	if(!szWeaponName[0])
	{
		for(new i, iPlayer;i < iNum;i++)
		{
			iPlayer = iPlayers[i]
			
			getLeftMessage(iPlayer, szLeft)
			CC_SendMatched(iPlayer, CC_COLOR_GREY, "%L", iPlayer, "SERVER_FINNALY_KILLED", g_dUserData[iAttacker][szName], g_dUserData[iVictim][szName], szLeft)
		}
	}
	else
	{
		for(new i, iPlayer;i < iNum;i++)
		{
			iPlayer = iPlayers[i]
			
			getLeftMessage(iPlayer, szLeft)
			CC_SendMatched(iPlayer, CC_COLOR_GREY, "%L", iPlayer, "SERVER_HAS_KILLED", g_dUserData[iAttacker][szName], iPlayer, bShowHudKills ? "SERVER_KILLED" : "SERVER_FINALIZED", g_dUserData[iVictim][szName], szWeaponName, szLeft)
		}
	}

	getLeftMessage(iAttacker, szLeft)
	set_dhudmessage(220, 255, 220, -1.0, 0.65, 0, 1.0, 5.0)
	show_dhudmessage(iAttacker, "%L", iAttacker, "SERVER_YOU_KILLED", iAttacker, bShowHudKills ? "SERVER_KILLED" : "SERVER_FINALIZED", g_dUserData[iVictim][szName], szWeaponName, szLeft)

	if(bShowHudKills)
	{
		set_dhudmessage(220, 0, 0, -1.0, 0.68, 0, 1.0, 5.0)
		show_dhudmessage(iAttacker, "%d %L%s", g_dUserData[iAttacker][iKills], iAttacker, "SERVER_KILLS", (g_dUserData[iAttacker][iKills] > 1) ? "S" : "")
	}
	checkNum()
	
	new iTeammateIndex = g_dUserData[iVictim][iTeamMateID], iPosition = is_user_alive(g_dUserData[iAttacker][iTeamMateID]) ? (getPlayersNum() + 2) : (getPlayersNum() + 1)
	if((iTeammateIndex && is_user_alive(iTeammateIndex)) || (iPosition == 1))
		return

	new szInfo[15]
	formatex(szInfo, charsmax(szInfo), "%L", iVictim, iTeammateIndex ? "SERVER_YOUR_TEAM" : "SERVER_YOU")
	
	set_hudmessage(225, 20, 0, -1.0, 0.25, 0, 1.0, 6.0)
	ShowSyncHudMsg(iVictim, g_hHuds[hPosition], "%s %L #%d", szInfo, iVictim, "SERVER_PLACED", iPosition)
}

getLeftMessage(id, szLeft[23])
{
	new iLeft = getPlayersNum()
	if(roundEnded())
	{
		formatex(szLeft, charsmax(szLeft), "^x03%L", id, "SERVER_ROUND_ENDED")
	}
	else
	{
		formatex(szLeft, charsmax(szLeft), "^x03%d^x01 %L", iLeft, id, "SERVER_LEFT")
	}
}

giveWeapons(id, iType, iTouched, iEntityID = 0, iValue = 0, iCmdItemLevel = 0)
{
	if(!is_user_connected(id))
		return
		
	static Float:fGameTime;fGameTime = get_gametime()
	switch(iType)
	{
		case 0:
		{
			new iEntID = iEntityID ? iEntityID : pev(iTouched, pev_iuser1), iItemLevel = iCmdItemLevel ? iCmdItemLevel : pev(iTouched, pev_iuser2)
			if(!iItemLevel)
			{
				iItemLevel = 3
			}
			
			new Float:fOrigin[3]
			pev(id, pev_origin, fOrigin)
			fOrigin[1] += 40.0
			
			switch(iEntID)
			{
				case ITEM_KEVLAR_1_ID, ITEM_KEVLAR_2_ID, ITEM_KEVLAR_3_ID:
				{
					if(checkItensNum(id))
					{
						return
					}
					
					if(iItemLevel < g_dUserData[id][iArmorLevel])
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_LOWER_LEVEL")
							lockedSound(id)
						}
						return
					}

					if(g_dUserData[id][iArmorLevel] != iItemLevel)
					{
						if(g_dUserData[id][iArmorLevel])
						{
							spawnItens(fOrigin, getKevlarID(id), g_dUserData[id][iArmorLevel], entity_get_float(id, EV_FL_armorvalue))
						}
						
						equipedHud(id, _, "%L lv. %d", id, "SERVER_ITEM_KEVLAR", iItemLevel)
						cs_set_user_armor(id, iValue ? iValue : pev(iTouched, pev_iuser3), (iItemLevel == 1) ? CS_ARMOR_KEVLAR : CS_ARMOR_VESTHELM)
						emit_sound(id, CHAN_ITEM, g_szSoundsData[sAmmoPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
	
						g_dUserData[id][iArmorLevel] = iItemLevel
					}
					else
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
							lockedSound(id)
						}
						return
					}
				}
				case ITEM_MEDKIT_ID:
				{
					if(checkItensNum(id))
					{
						return
					}
					
					g_dUserData[id][iMedKits]++

					equipedHud(id, ITEM_MEDKIT_ID, "MedKit")
					emit_sound(id, CHAN_ITEM, g_szSoundsData[sGunPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
				}
				case ITEM_HELMET_1_ID, ITEM_HELMET_2_ID, ITEM_HELMET_3_ID:
				{
					if(checkItensNum(id))
					{
						return
					}
					
					if(iItemLevel < g_dUserData[id][iHelmetLevel])
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_LOWER_LEVEL")
							lockedSound(id)
						}
						return
					}
					
					if(g_dUserData[id][iHelmetLevel] != iItemLevel)
					{
						new iEnt = create_entity("info_target")
						if(pev_valid(iEnt))
						{
							if(g_dUserData[id][iHelmetLevel])
							{	
								spawnItens(fOrigin, getUserHelmetID(id), g_dUserData[id][iHelmetLevel])
								removeHelmet(id)
							}
							
							g_dUserData[id][iHelmetEnt] = iEnt
							g_dUserData[id][iHelmetLevel] = iItemLevel

							set_pev(iEnt, pev_movetype, MOVETYPE_FOLLOW)
							set_pev(iEnt, pev_aiment, id)
				
							engfunc(EngFunc_SetModel, iEnt, g_szItens[iItensList:getUserHelmetID(id)][szItemModel])

							equipedHud(id, _, "%L lv. %d", id, "SERVER_ITEM_HELMET", iItemLevel)
							emit_sound(id, CHAN_ITEM, g_szSoundsData[sGunPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
						}
					}
					else
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
							lockedSound(id)
						}
						return
					}
				}
				case ITEM_BAG_1_ID, ITEM_BAG_2_ID, ITEM_BAG_3_ID:
				{
					if(iItemLevel < g_dUserData[id][iBagLevel])
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_LOWER_LEVEL")
							lockedSound(id)
						}
						return
					}
					
					if(iItemLevel != g_dUserData[id][iBagLevel])
					{						
						if(g_dUserData[id][iBagLevel])
						{
							spawnItens(fOrigin, getBagID(id), g_dUserData[id][iBagLevel])
						}
						
						g_dUserData[id][iBagLevel] = iItemLevel
						
						equipedHud(id, _, "%L lv. %d", id, "SERVER_ITEM_BAG", iItemLevel)
						emit_sound(id, CHAN_ITEM, g_szSoundsData[sGunPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
					}
					else
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
							lockedSound(id)
						}
						return
					}
				}
				case ITEM_IMPULSE_ID:
				{
					if(checkItensNum(id))
					{
						return
					}
					
					if(user_has_weapon(id, CSW_SMOKEGRENADE))
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_ALREADY_HAS_IMPULSEGRENADE")
							lockedSound(id)
						}
						return
					}
					
					give_item(id, "weapon_smokegrenade")
				}
				case ITEM_SCOPE2X_ID, ITEM_SCOPE4X_ID:
				{
					if(checkItensNum(id))
					{
						return
					}
					
					new bool:bIs2x = bool:(iEntID == ITEM_SCOPE2X_ID)
					if((bIs2x && (g_dUserData[id][iScopeLevel] != 1))
					|| (!bIs2x && (g_dUserData[id][iScopeLevel] != 2)))
					{
						if(g_dUserData[id][iScopeLevel])
						{
							spawnItens(fOrigin, (g_dUserData[id][iScopeLevel] == 1) ? ITEM_SCOPE2X_ID : ITEM_SCOPE4X_ID)
						}
						
						new iWeapon = get_user_weapon(id)
						if(!((1 << iWeapon) & CONST_RIFLES) || (iWeapon == CSW_SCOUT))
						{
							if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
							{
								g_dUserData[id][fUserGameTime] = _:fGameTime
								CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_INVALID_WEAPON")
								lockedSound(id)
							}
							return
						}
						g_dUserData[id][iScopeLevel] = bIs2x ? 1 : 2
						g_dUserData[id][iScopedWeaponID] = iWeapon
						
						equipedHud(id, _, "%L %s", id, "SERVER_ITEM_SCOPE", bIs2x ? "2X" : "4X")
						emit_sound(id, CHAN_ITEM, g_szSoundsData[sGunPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
					}
					else
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
							lockedSound(id)
						}
					}
				}
				case ITEM_PENTEALONGADO_1_ID, ITEM_PENTEALONGADO_2_ID, ITEM_PENTEALONGADO_3_ID:
				{
					if(checkItensNum(id))
					{
						return
					}
					
					if(iItemLevel < g_dUserData[id][iPenteAlongadoLevel])
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_LOWER_LEVEL")
							lockedSound(id)
						}
						return
					}
					
					if(g_dUserData[id][iPenteAlongadoLevel] != iItemLevel)
					{
						if(g_dUserData[id][iPenteAlongadoLevel])
						{
							spawnItens(fOrigin, getPenteAlongadoID(id), g_dUserData[id][iPenteAlongadoLevel])
						}
						
						new iWeapon = get_user_weapon(id)
						if(!((1 << iWeapon) & CONST_RIFLES))
						{
							if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
							{
								g_dUserData[id][fUserGameTime] = _:fGameTime
								CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_INVALID_WEAPON")
								lockedSound(id)
							}
							return
						}
	
						g_dUserData[id][iPenteAlongadoWeaponID] = iWeapon
						g_dUserData[id][iPenteAlongadoLevel] = iItemLevel
						
						switch(iItemLevel)
						{
							case 1:		g_dUserData[id][iWeaponBullet] = 35
							case 2:		g_dUserData[id][iWeaponBullet] = 42
							case 3:		g_dUserData[id][iWeaponBullet] = 48
						}
						
						if(iWeapon == CSW_SCOUT)
						{
							switch(iItemLevel)
							{
								case 1:		g_dUserData[id][iWeaponBullet] = 12
								case 2:		g_dUserData[id][iWeaponBullet] = 15
								case 3:		g_dUserData[id][iWeaponBullet] = 18
							}
						}
						else if(iWeapon == CSW_GALIL)
						{
							if(iItemLevel == 1)
							{
								g_dUserData[id][iWeaponBullet] += 3
							}
						}
						else if(iWeapon == CSW_P90)
						{
							g_dUserData[id][iWeaponBullet] += (g_dUserData[id][iWeaponBullet] / 2)
						}

						new szWeaponName[MAX_WEAPONNAME_LENGH]
						get_weaponname(iWeapon, szWeaponName, charsmax(szWeaponName))
			
						new csWeaponID = find_ent_by_owner(-1, szWeaponName, id), iClip = cs_get_weapon_ammo(csWeaponID)
						if(iClip > g_dUserData[id][iWeaponBullet])
						{
							cs_set_weapon_ammo(csWeaponID, g_dUserData[id][iWeaponBullet])
							cs_set_user_bpammo(id, iWeapon, (cs_get_user_bpammo(id, iWeapon) + (g_dUserData[id][iWeaponBullet] - iClip)))
						}
						
						equipedHud(id, _, "%L lv. %d", id, "SERVER_ITEM_ELONGATED_COMB", iItemLevel)
						emit_sound(id, CHAN_ITEM, g_szSoundsData[sGunPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
					}
					else
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_GOT_THE_SAME_ITEM_LEVEL")
							lockedSound(id)
						}
						return
					}
				}
				case ITEM_GUSPE_ID:
				{
					if(checkItensNum(id))
					{
						return
					}

					if(g_dUserData[id][bHasGuspe])
					{
						if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
						{
							g_dUserData[id][fUserGameTime] = _:fGameTime
							CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_ALREADY_HAS_GUSPE")
							lockedSound(id)
						}
						return
					}
					
					g_dUserData[id][bHasGuspe] = true

					equipedHud(id, ITEM_GUSPE_ID, "Guspe")
					emit_sound(id, CHAN_ITEM, g_szSoundsData[sGunPickup], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
				}
			}
			killEntity(iTouched)
		}
		case 1:
		{
			if(checkItensNum(id))
			{
				return
			}

			new iCswWeapon = pev(iTouched, pev_iuser1)
			if(user_has_weapon(id, iCswWeapon))
				return

			new iSlot = getWeaponBoxSlot(iCswWeapon)
			if (1 <= iSlot <= 2)
			{
#if defined _reapi_included
				if (get_member(id, m_rgpPlayerItems, iSlot) > 0)
#else
				const m_rpgPlayerItems = (1468 / 4)
				const m_rpgPlayerItemsDiff = ((1488 / 4) - m_rpgPlayerItems) 

				if (get_pdata_cbase(id, (m_rpgPlayerItems + iSlot), m_rpgPlayerItemsDiff) > 0)
#endif
				{
					return
				}
			}

			new szWeap[MAX_PLAYERS], iBpAmmo = getMaxBpAmmo(iCswWeapon)
			get_weaponname(iCswWeapon, szWeap, charsmax(szWeap))
			give_item(id, szWeap)
			cs_set_user_bpammo(id, iCswWeapon, random_num((iBpAmmo > 2) ? (iBpAmmo / 2) : iBpAmmo, iBpAmmo))
		
			killEntity(iTouched)
		}
	}
}

getWeaponBoxSlot(iWeaponID)
{
	new iReturnValue
	if(((1 << iWeaponID) & CONST_RIFLES) || ((1 << iWeaponID) & CONST_SHOTGUNS))
	{
		iReturnValue = 1
	}
	else if((1 << iWeaponID) & CONST_PISTOLS)
	{
		iReturnValue = 2
	}
	return iReturnValue
}

resetDuos(id)
{
	if(g_dUserData[id][bChoosedDuo])
	{
		g_dUserData[g_dUserData[id][iTeamMateID]][bChoosedDuo] = false
		g_dUserData[id][bChoosedDuo] = false
		
		g_dUserData[g_dUserData[id][iTeamMateID]][iTeamMateID] = 0
		g_dUserData[id][iTeamMateID] = 0
	}
	
	g_dUserData[id][iInviter] = 0
}

dropPlayer(id)
{
	resetUserSpeed(id)
	set_pev(id, pev_gravity, 1.0)
	
	if(get_pcvar_num(g_pCvars[cPlayerTrail]))
	{
		setBeamFollow(id, {255, 255, 255}, 192)
	}
	
	setInvisibility(id, false)
	set_task(1.0, "setPlayerDamageble", id + TASK_SET_DAMAGEBLE)

	client_cmd(id, "stopsound")
	clientPlaySound(id, g_szSoundsData[sPlaneDropSound])
	clientPlaySound(id, g_szSoundsData[sSkyDiveSound])

	if(g_dUserData[id][iTeamMateID])
	{
		clientPlaySound(g_dUserData[id][iTeamMateID], g_szSoundsData[sTeammateDropSound])
	}

	g_dUserData[id][bIsOnPlane] = false
	g_dUserData[id][bCanUseParachute] = true
}

public setPlayerDamageble(id)
{
	setDamageble((id - TASK_SET_DAMAGEBLE), true)
}

setBeamFollow(id, iRGB[3], iIntensity)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_BEAMFOLLOW)
	write_short(id)
	write_short(g_iPlayerTrailSprite)
	write_byte(4)
	write_byte(3)
	write_byte(iRGB[0])
	write_byte(iRGB[1])
	write_byte(iRGB[2])
	write_byte(iIntensity)
	message_end()
}

startBleeding(id)
{
	new Float:fOrigin[3], iOrigin[3]
	pev(id, pev_origin, fOrigin)

	iOrigin[0] = floatround(fOrigin[0])
	iOrigin[1] = floatround(fOrigin[1])
	iOrigin[2] = floatround(fOrigin[2])

	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_BLOODSTREAM)
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2] + 10)
	write_coord(random_num(-360, 360))
	write_coord(random_num(-360, 360)) 
	write_coord(-10) 
	write_byte(70)
	write_byte(random_num(50, 100))
	message_end()

	for(new i;i < BLEEDS_NUM; i++) 
	{
		message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
		write_byte(TE_WORLDDECAL)
		write_coord(iOrigin[0] + random_num(-100, 100))
		write_coord(iOrigin[1] + random_num(-100, 100))
		write_coord(iOrigin[2] - 36)
		write_byte(190)
		message_end()
	}
}

cleanData(id, iTarget, bool:bUnknock = false)
{
	if(bUnknock)
	{
		g_dUserData[iTarget][bKnockedOut] = false
		g_dUserData[iTarget][iWeaponKnocker] = 0
		g_dUserData[iTarget][iKnockerID] = 0

		remove_task(iTarget + TASK_REMOVEHEALTH)
		client_cmd(iTarget, "lastinv")
		
		resetUserSpeed(iTarget)
		ScreenFade(iTarget, {255, 255, 255}, 105)
		set_user_health(iTarget, UNKNOCK_HEALTH)
	}
	else
	{
		if(task_exists(id + TASK_KNOCK))
		{
			remove_task(id + TASK_KNOCK)
		}
	}
	g_dUserData[id][iHelpingID] = 0
	g_dUserData[iTarget][iHelperID] = 0

	barTime(id, iTarget, 0)
}

public spawnWeapons()
{	
	new iEnt = create_entity("info_target"), iRandomWeapon
	if(pev_valid(iEnt))
	{	
		new iList[MAX_SPAWNS], iFinal = -1, iTotal, iRandomSpawn
		new Float:fLoc[MAX_SPAWNS][3], iNum, iLocNum
			
		new iEntities = -1
		while((iEntities = find_ent_by_class(iEntities, g_szClassNamesData[cWeapons])))
		{
			pev(iEntities, pev_origin, fLoc[iLocNum])
			iLocNum++
		}
		
		while(iNum <= g_iVecsNum[vecWeapons])
		{
			if(iNum == g_iVecsNum[vecWeapons])
				break
					
			iRandomSpawn = random_num(0, g_iVecsNum[vecWeapons] - 1)
			if(!iList[iRandomSpawn])
			{
				iList[iRandomSpawn] = 1
				iNum++
			} 
			else 
			{
				iTotal++
				if(iTotal > MAX_SPAWNS)
				{
					break
				}
				continue
			}
			
			if(iLocNum < 1)
			{
				iFinal = iRandomSpawn
				break
			}
			
			iFinal = iRandomSpawn
			for(new x; x < iLocNum; x++)
			{
				new Float:fDistance = get_distance_f(g_fSpawnVecs[iRandomSpawn], fLoc[x])
				if(fDistance < 80.0)
				{
					iFinal = -1
					break
				}
			}
			
			if(iFinal != -1)
				break
		}
		
		engfunc(EngFunc_SetOrigin, iEnt, g_fSpawnVecs[iRandomSpawn])
		if(random_num(0, 5) <= WEAPONS_CHANCE)
		{	
			iRandomWeapon = random_num(0, g_iWeaponsNum - 1)
			
			engfunc(EngFunc_SetModel, iEnt, g_szWeaponsModels[iRandomWeapon][szModelName])
			set_pev(iEnt, pev_iuser1, g_szWeaponsModels[iRandomWeapon][iWeaponInfoID])
		}
		else
		{
			new iRandomItem = random_num(0, sizeof g_szItens - 1)
			if(!g_szItens[iItensList:iRandomItem][bIsItemEnabled])
				return
					
			new iUser3, iRandomLevel = g_szItens[iItensList:iRandomItem][iItemInfoLevel] 
			new iItemInfoID = g_szItens[iItensList:iRandomItem][iItemID]
			if(iItemInfoID <= ITEM_KEVLAR_3_ID)
			{
				switch(iRandomLevel)
				{
					case 1, 3:	iUser3 = 100
					case 2:		iUser3 = 50
				}
			}
				
			engfunc(EngFunc_SetModel, iEnt, g_szItens[iItensList:iRandomItem][szItemModel])
				
			set_pev(iEnt, pev_iuser1, iItemInfoID)
			set_pev(iEnt, pev_iuser2, iRandomLevel)
			set_pev(iEnt, pev_iuser3, iUser3)
		}
		
		static Float:fMaxs[3] = {5.0, 3.0, 6.0}
		static Float:fMins[3] = {-1.0, -3.0, -6.0}
			
		set_pev(iEnt, pev_solid, SOLID_TRIGGER)
		set_pev(iEnt, pev_gravity, 1.0)
		set_pev(iEnt, pev_movetype, MOVETYPE_TOSS)
		set_pev(iEnt, pev_classname, g_szClassNamesData[cWeapons])
			
		set_pev(iEnt, pev_rendermode, kRenderNormal)
		set_pev(iEnt, pev_renderfx, kRenderFxNone)
		set_pev(iEnt, pev_renderamt, 255)
			
		engfunc(EngFunc_SetSize, iEnt, fMins, fMaxs)
		engfunc(EngFunc_DropToFloor, iEnt)
	}
}

roundEnd(id = 0, iMate = 0, bool:bNewRound = true)
{
	removeTasks()
	if(g_bHasRoundEnded)
		return
		
	g_bHasRoundEnded = true
	
	if(ArraySize(g_arWinSounds))
	{
		new szSound[MAX_MODEL_LENGH]
		ArrayGetString(g_arWinSounds, random_num(0, ArraySize(g_arWinSounds) - 1), szSound, charsmax(szSound))

		client_cmd(0, "stopsound")
		clientPlaySound(0, szSound)
	}
	
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ch")
		
	new iNewRound = get_pcvar_num(g_pCvars[cNewRoundTime])
	if(!is_user_connected(id))
	{
		for(new i, iPlayer;i < iNum;i++)
		{
			iPlayer = iPlayers[i]

			CC_SendMatched(iPlayer, CC_COLOR_GREY, "%L", iPlayer, "SERVER_NO_WINNER")
			CC_SendMatched(iPlayer, CC_COLOR_GREY, "^x03--------------------------------------------------------------------------")
		
			CC_SendMatched(iPlayer, CC_COLOR_GREY, "%L", iPlayer, "SERVER_NEW_ROUND_MSG", iNewRound)
		}
		set_task(float(iNewRound), "restartRound", 1)
		return
	}
	
	if(bNewRound)
	{
		new szMessage[2][100], iUserKills = g_dUserData[id][iKills], iDuoKills = (iUserKills + g_dUserData[iMate][iKills])
		g_dUserData[id][iVitorias]++
		
		checkTasks(id)
		setDamageble(id, false)

		if(iMate)
		{
			formatex(szMessage[0], charsmax(szMessage[]), "%L", id, "SERVER_PLAYER_DUO_WIN_MSG", g_dUserData[iMate][szName], iDuoKills, (iDuoKills > 1) ? "s" : "")
			g_dUserData[iMate][iVitorias]++
	
			checkTasks(iMate)
			setDamageble(iMate, false)
			
			CC_SendMatched(iMate, CC_COLOR_GREY, "%L", iMate, "SERVER_PLAYER_DUO_WIN_MSG", g_dUserData[id][szName], iDuoKills, (iDuoKills > 1) ? "s" : "")
			CC_SendMatched(iMate, CC_COLOR_GREY, "^x03--------------------------------------------------------------------------")
		}
		else
		{
			formatex(szMessage[0], charsmax(szMessage[]), "%L", id, "SERVER_PLAYER_SOLO_WIN_MSG", iUserKills, (iUserKills > 1) ? "s" : "")
		}
		
		CC_SendMatched(id, CC_COLOR_GREY, "%s", szMessage[0])
		CC_SendMatched(id, CC_COLOR_GREY, "^x03 --------------------------------------------------------------------------")
		
		for(new i, iPlayer;i < iNum;i++)
		{
			iPlayer = iPlayers[i]

			formatex(szMessage[1], charsmax(szMessage[]), "%L", iPlayer, iMate ? "SERVER_DUO_WIN_MSG" : "SERVER_SOLO_WIN_MSG", g_dUserData[id][szName], g_dUserData[iMate][szName], iDuoKills, (iDuoKills > 1) ? "s" : "")
			CC_SendMatched(iPlayer, CC_COLOR_GREY, "%s %L", szMessage[1], iPlayer, "SERVER_NEW_ROUND_MSG", iNewRound)
		}
		set_task(float(iNewRound), "restartRound", 1)
	}

	ExecuteForward(g_forwardGameWinner, g_iReturn, id, iMate)
}

removeTasks()
{
	remove_task(TASK_SPAWN_AIRDROP)
	remove_task(TASK_WEAPONS)
	remove_task(TASK_COUNTDOWN)
	remove_task(TASK_PLAYERSCOUNT)
	remove_task(TASK_PLANE)
	remove_task(TASK_AIRDROP)
	remove_task(TASK_ADD_WEAPONS)
	remove_task(TASK_CHECK_AIRDROP_DIST)
}

fmCreatePlane(iEnt, szEntName[], szModel[], Float:fOrigin[3], Float:fVelocity[3], Float:fAngle[3])
{
	if(!pev_valid(iEnt))
		return

	set_pev(iEnt, pev_origin, fOrigin)
	engfunc(EngFunc_SetModel, iEnt, szModel)
	set_pev(iEnt, pev_movetype, MOVETYPE_FLY)
	
	set_pev(iEnt, pev_velocity, fVelocity)
	set_pev(iEnt, pev_solid, SOLID_NOT)
	set_pev(iEnt, pev_angles, fAngle)
	set_pev(iEnt, pev_classname, szEntName)
}

removeEntities()
{
	new iEnt = -1
	while((iEnt = find_ent_by_class(iEnt, g_szClassNamesData[cWeapons])))
	{
		killEntity(iEnt)
	}
	
	while((iEnt = find_ent_by_class(iEnt,g_szClassNamesData[cTransporter])))
	{
		killEntity(iEnt)
	}
	
	while((iEnt = find_ent_by_class(iEnt, g_szClassNamesData[cAirDrop])))
	{
		killEntity(iEnt)
	}
}

showSmoke()
{
	emit_sound(g_iEnt[iAirDropID], CHAN_ITEM, g_szSoundsData[sAirdropTouchSound], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)

	new Float:fOrigin[3]
	pev(g_iEnt[iAirDropID], pev_origin, fOrigin)

	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_FIREFIELD)
	write_coord_fl(fOrigin[0])
	write_coord_fl(fOrigin[1] + 70.0)
	write_coord_fl(fOrigin[2] + 130.0)
	write_short(100)
	write_short(g_iAirDropSprite)
	write_byte(200)
	write_byte(TEFIRE_FLAG_ALPHA)
	write_byte(600)
	message_end()

	set_task(0.3, "checkPlayersDistanceToAirDrop", TASK_CHECK_AIRDROP_DIST, .flags = "b")
}

public checkPlayersDistanceToAirDrop()
{
	if(!pev_valid(g_iEnt[iAirDropID]))
	{
		remove_task(TASK_CHECK_AIRDROP_DIST)
		return
	}
	
	new Float:fOrigin[3]
	pev(g_iEnt[iAirDropID], pev_origin, fOrigin)

	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ach")
	for(new i, id;i < iNum;i++)
	{
		id = iPlayers[i]

		if(g_dUserData[id][bKnockedOut])
			continue

		new Float:fPlayerOrigin[3]
		pev(id, pev_origin, fPlayerOrigin)
		
		if(get_distance_f(fOrigin, fPlayerOrigin) <= 80.0)
		{
			if(!g_dUserData[id][bIsAirDropMenuOpen])
			{
				airDropMenu(id, true)
			}
		}
		else
		{
			if(g_dUserData[id][bIsAirDropMenuOpen])
			{
				airDropMenu(id, false)
			}
		}
	}
}

public checkNum()
{	
	if(roundEnded())
	{
		new iLastPlayerID = getPlayersNum(1, 0)
		roundEnd(iLastPlayerID, g_dUserData[iLastPlayerID][iTeamMateID])
	}
}

bool:roundEnded()
{
	if((getPlayersNum() <= 1) || (getPlayersNum() == MIN_PLAYERS) && haveDuoLeft())
	{
		return true 
	}
	return false
}

getPlayersNum(iType = 0, iID = 0)
{
	new iPlayers[MAX_PLAYERS], iNum
	get_players(iPlayers, iNum, "ach")
	return iType ? iPlayers[iID] : iNum
}

checkHeadHits(id, iLevel)
{
	if(++g_dUserData[id][iHeadHits] >= getMaxHeadHits(id))
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_HELMET_TOOK_OFF_MSG", iLevel)
		
		set_hudmessage(255, 0, 0, -1.0, 0.3, 0, 1.0, 5.0)
		ShowSyncHudMsg(id, g_hHuds[hLostHelmet], "%L", id, "SERVER_HELMET_TOOK_OFF_HUD", iLevel)
						
		removeHelmet(id)
	}
}

removeHelmet(id)
{
	killEntity(g_dUserData[id][iHelmetEnt])
	
	g_dUserData[id][iHelmetLevel] = 0
	g_dUserData[id][iHelmetEnt] = 0	
	g_dUserData[id][iHeadHits] = 0
}

haveDuoLeft()
{
	new iPlayers[MAX_PLAYERS], iNum, iDuoNum
	get_players(iPlayers, iNum, "ach")
	for(new i, id;i < iNum;i++)
	{
		id = iPlayers[i]

		if(g_dUserData[id][iTeamMateID] && is_user_alive(g_dUserData[id][iTeamMateID]))
		{
			iDuoNum++
		}
	}
	return iDuoNum
}

ScreenFade(id, iRGB[3], Intensity)
{ 
	message_begin(MSG_ONE, g_mMessageScreenFade, .player = id)
	write_short(4300)
	write_short(0)
	write_short(0)
	write_byte(iRGB[0])
	write_byte(iRGB[1]) 
	write_byte(iRGB[2]) 
	write_byte(Intensity) 
	message_end()
}

checkTasks(id)
{
	if(task_exists(id + TASK_REMOVEHEALTH))
	{
		remove_task(id + TASK_REMOVEHEALTH)
	}
	
	if(task_exists(id + TASK_MEDKIT))
	{
		remove_task(id + TASK_MEDKIT)
	}
	
	if(task_exists(id + TASK_KNOCK))
	{
		remove_task(id + TASK_KNOCK)
	}

	if(task_exists(id + TASK_SET_DAMAGEBLE))
	{
		remove_task(id + TASK_SET_DAMAGEBLE)
	}
}

saveData(id)
{
	new szSteam[35]
	get_user_authid(id, szSteam, charsmax(szSteam)) 
	
	new szData[16]
	num_to_str(g_dUserData[id][iVitorias], szData, charsmax(szData))
	fvault_set_data(g_szVaultName, szSteam, szData)
}

loadData(id) 
{
	new szSteam[35]
	get_user_authid(id, szSteam, charsmax(szSteam)) 
    
	new szData[16]
	if(fvault_get_data(g_szVaultName, szSteam, szData, charsmax(szData)))
	{
		g_dUserData[id][iVitorias] = str_to_num(szData)
	}
	else g_dUserData[id][iVitorias] = 0
}

getRandomPlaneDirection(Float:fOrigin[3], Float:fAngle[3], Float:fVelocity[3])
{
	g_iRandomDirection = random_num(0, g_iVecsNum[vecPlane] - 1)

	fOrigin[0] = 	g_fPlaneOrigins[g_iRandomDirection][0]
	fOrigin[1] = 	g_fPlaneOrigins[g_iRandomDirection][1]
	fOrigin[2] = 	g_fPlaneOrigins[g_iRandomDirection][2]

	fAngle[0] = 	g_fPlaneAngles[g_iRandomDirection][0]
	fAngle[1] = 	g_fPlaneAngles[g_iRandomDirection][1]
	fAngle[2] = 	g_fPlaneAngles[g_iRandomDirection][2]

	fVelocity[0] = 	g_fPlaneVelocities[g_iRandomDirection][0]
	fVelocity[1] = 	g_fPlaneVelocities[g_iRandomDirection][1]
	fVelocity[2] = 	g_fPlaneVelocities[g_iRandomDirection][2]
}

bool:checkItensNum(id)
{
	if((getUserItens(id) + getWeaponsNum(id)) >= getUserMaxItens(id))
	{
		new Float:fGameTime = get_gametime()
		if((fGameTime - g_dUserData[id][fUserGameTime]) >= MESSAGE_DELAY)
		{
			g_dUserData[id][fUserGameTime] = _:fGameTime
			CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_NO_ENOUGH_SPACE_MSG_1", g_cCommands[cBag])
					
			if(g_dUserData[id][iBagLevel] < 3)
			{
				CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_NO_ENOUGH_SPACE_MSG_2")
			}
			lockedSound(id)
		}
		return true
	}
	return false
}

getMaxBpAmmo(iWeapon)
{
	new iMaxBpAmmo
	switch(iWeapon)
	{
		case CSW_AK47, CSW_M4A1, CSW_FAMAS, CSW_GALIL, CSW_AUG, CSW_SG552, CSW_TMP: 	iMaxBpAmmo = 90
		case CSW_SCOUT: 								iMaxBpAmmo = 30
		case CSW_MP5NAVY: 								iMaxBpAmmo = 120
		case CSW_P90, CSW_MAC10, CSW_UMP45: 						iMaxBpAmmo = 100
		
		case CSW_M3, CSW_XM1014:							iMaxBpAmmo = 32

		case CSW_DEAGLE: 								iMaxBpAmmo = 35
		case CSW_USP, CSW_FIVESEVEN:							iMaxBpAmmo = 100
		case CSW_GLOCK18, CSW_ELITE:							iMaxBpAmmo = 120
		case CSW_P228:									iMaxBpAmmo = 52
	
		case CSW_HEGRENADE, CSW_FLASHBANG:						iMaxBpAmmo = 1
	}
	return iMaxBpAmmo
}

dropItems(id, bool:bBagCheck = false)
{
	new Float:fOrigin[3]
	pev(id, pev_origin, fOrigin)
	
	if(g_dUserData[id][iArmorLevel])
	{
		spawnItens(fOrigin, getKevlarID(id), g_dUserData[id][iArmorLevel], entity_get_float(id, EV_FL_armorvalue))
		
		if(bBagCheck)
		{
			g_dUserData[id][iArmorLevel] = 0
			set_user_armor(id, 0)
			return
		}
	}
	
	if(g_dUserData[id][iMedKits])
	{
		for(new i;i < g_dUserData[id][iMedKits];i++)
		{
			spawnItens(fOrigin, ITEM_MEDKIT_ID)
		}
		
		if(bBagCheck)
		{
			g_dUserData[id][iMedKits] = 0
			return
		}
	}
	
	if(g_dUserData[id][iHelmetEnt])
	{
		spawnItens(fOrigin, getUserHelmetID(id), g_dUserData[id][iHelmetLevel])
		
		if(bBagCheck)
		{
			removeHelmet(id)
			return
		}
	}
	
	if(g_dUserData[id][iBagLevel])
	{
		spawnItens(fOrigin, getBagID(id), g_dUserData[id][iBagLevel])
	}
	
	if(user_has_weapon(id, CSW_SMOKEGRENADE))
	{
		spawnItens(fOrigin, ITEM_IMPULSE_ID)
		
		if(bBagCheck)
		{
			stripWeapon(id, "weapon_smokegrenade")
			return
		}
	}
	
	if(g_dUserData[id][iScopeLevel])
	{
		spawnItens(fOrigin, (g_dUserData[id][iScopeLevel] == 1) ? ITEM_SCOPE2X_ID : ITEM_SCOPE4X_ID)
		
		if(bBagCheck)
		{
			g_dUserData[id][iScopeLevel] = 0
			return
		}
	}
	
	if(g_dUserData[id][iPenteAlongadoWeaponID])
	{
		spawnItens(fOrigin, getPenteAlongadoID(id), g_dUserData[id][iPenteAlongadoLevel])
		
		if(bBagCheck)
		{
			g_dUserData[id][iPenteAlongadoLevel] = 0
			g_dUserData[id][iPenteAlongadoWeaponID] = 0  
			return
		}
	}

	if(g_dUserData[id][bHasGuspe])
	{
		spawnItens(fOrigin, ITEM_GUSPE_ID)
		
		if(bBagCheck)
		{
			g_dUserData[id][bHasGuspe] = false 
			return
		}
	}
}

getWeaponsNum(id)
{
	new szWeapons[MAX_PLAYERS], iNum
	get_user_weapons(id, szWeapons, iNum)
	return (iNum - 1)
}

getUserItens(id)
{
	new iItensNum
	if(g_dUserData[id][iArmorLevel])
	{
		iItensNum++
	}
	
	if(g_dUserData[id][iMedKits])
	{
		for(new i;i < g_dUserData[id][iMedKits];i++)
		{
			iItensNum++
		}
	}
	
	if(g_dUserData[id][iHelmetEnt])
	{
		iItensNum++
	}
	
	if(g_dUserData[id][iScopeLevel])
	{
		iItensNum++
	}
	
	if(g_dUserData[id][iPenteAlongadoWeaponID])
	{
		iItensNum++
	}

	if(g_dUserData[id][bHasGuspe])
	{
		iItensNum++
	}

	return iItensNum
}

spawnItens(Float:fOrigin[3], iType, iLevel = 0, Float:fArmorValue = 0.0, szItemName[MAX_PLAYERS] = "", id = 0)
{
	if(szItemName[0])
	{
		CC_SendMatched(id, CC_COLOR_RED, "^x03%L", id, "SERVER_ITEM_DROPPED", szItemName)
	}
	
	new iEnt = create_entity("info_target")
	engfunc(EngFunc_SetOrigin, iEnt, fOrigin)
	
	new szModel[MAX_MODEL_LENGH]
	formatex(szModel, charsmax(szModel), g_szItens[iItensList:iType][szItemModel])
	engfunc(EngFunc_SetModel, iEnt, szModel)
		
	new Float:fEntVelocity[3]
	if(is_user_alive(id))
	{
		velocity_by_aim(id, 400, fEntVelocity)
	}
	else
	{
		fEntVelocity[0] = random_float(-250.0, 250.0)
		fEntVelocity[1] = random_float(-250.0, 250.0)
	}
	set_pev(iEnt, pev_velocity, fEntVelocity)
	set_pev(iEnt, pev_iuser1, iType)
	if(fArmorValue)
	{
		set_pev(iEnt, pev_iuser3, floatround(fArmorValue))
	}
	
	set_pev(iEnt, pev_solid, SOLID_TRIGGER)
	set_pev(iEnt, pev_gravity, 1.0)
	set_pev(iEnt, pev_movetype, MOVETYPE_TOSS)
	set_pev(iEnt, pev_classname, g_szClassNamesData[cWeapons])
	
	set_pev(iEnt, pev_iuser2, iLevel)
}

makeFov(id, iAmmount)
{
	g_dUserData[id][bIsFoved] = bool:iAmmount

	emit_sound(id, CHAN_ITEM, g_szSoundsData[sScopeSound], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)

	message_begin(MSG_ONE, g_mMessageSetFov, .player = id)
	write_byte(iAmmount)
	message_end()
}

equipedHud(id, iID = 0, szItem[], any:...)
{
	static szMsg[30]
	vformat(szMsg, charsmax(szMsg), szItem, 4)

	new szDescrip[55]
	switch(iID)
	{
		case ITEM_MEDKIT_ID:	formatex(szDescrip, charsmax(szDescrip), "%L", id, "SERVER_MEDKIT_COMMAND_INFO")	
		case ITEM_GUSPE_ID:	formatex(szDescrip, charsmax(szDescrip), "%L", id, "SERVER_GUSPE_COMMAND_INFO")
	}
	
	set_hudmessage(255, 255, 255, -1.0, 0.3, 0, 1.0, 4.0)
	ShowSyncHudMsg(id, g_hHuds[hEquipedHud], "%s %L^n%s", szMsg, id, "SERVER_EQUIPED", szDescrip)
}

SendWeaponAnim(id, iAnim)
{
	set_pev(id, pev_weaponanim, iAnim)

	message_begin(MSG_ONE_UNRELIABLE, SVC_WEAPONANIM, .player = id)
	write_byte(iAnim)
	write_byte(pev(id, pev_body))
	message_end()
}

getPercent(iItensNum, iMaxItens)
{
	return (iItensNum >= 0) ? clamp(floatround(floatmul(float(iItensNum) / float(iMaxItens), 100.0)), 0, 100) : 0
}

bool:isScopedWeapon(iWeapon)
{
	return bool:((iWeapon == CSW_AUG) || (iWeapon == CSW_SG552))
}

stripWeapon(id, szWeapon[])
{
	if(!equal(szWeapon, "weapon_", 7)) 
		return

	new iWeaponId = get_weaponid(szWeapon)
	if(!iWeaponId) 
		return

	new iEnt = -1
	while((iEnt = engfunc(EngFunc_FindEntityByString, iEnt, "classname", szWeapon)) && pev(iEnt, pev_owner) != id) {}
		
	if(!iEnt) 
		return
	
	if(get_user_weapon(id) == iWeaponId) 
	{
		ExecuteHamB(Ham_Weapon_RetireWeapon, iEnt)
	}
	
	if(!ExecuteHamB(Ham_RemovePlayerItem, id, iEnt)) 
		return
		
	ExecuteHamB(Ham_Item_Kill, iEnt)
	set_pev(id, pev_weapons, pev(id, pev_weapons) & ~(1 << iWeaponId))
}

getUserMaxItens(id)
{
	new iReturnValue
	switch(g_dUserData[id][iBagLevel])
	{
		case 0: 	iReturnValue = 4
		case 1:		iReturnValue = 6
		case 2:		iReturnValue = 9
		case 3:		iReturnValue = 11
	}
	return iReturnValue
}

getMaxHeadHits(id)
{
	new iReturnValue
	switch(g_dUserData[id][iHelmetLevel])
	{
		case 1:		iReturnValue = 2
		case 2:		iReturnValue = 4
		case 3:		iReturnValue = 6
	}
	return iReturnValue
}

getWeaponDefaultClip(iWeapon)
{
	new iReturnValue
	switch(iWeapon)
	{
		case CSW_AK47, CSW_M4A1, CSW_MAC10, CSW_MP5NAVY, CSW_AUG, CSW_SG552, CSW_TMP, CSW_ELITE:				iReturnValue = 30
		case CSW_P90:													iReturnValue = 50
		case CSW_M3:													iReturnValue = 8
		case CSW_GALIL:													iReturnValue = 35
		case CSW_UMP45, CSW_FAMAS:											iReturnValue = 25
		case CSW_USP:													iReturnValue = 12
		case CSW_DEAGLE, CSW_XM1014:											iReturnValue = 7
		case CSW_GLOCK18, CSW_FIVESEVEN:										iReturnValue = 20
		case CSW_P228:													iReturnValue = 13
		case CSW_SCOUT:													iReturnValue = 10
	}
	return iReturnValue
}

bool:isSolid(iEnt)
{
	return (pev(iEnt, pev_solid) > SOLID_TRIGGER) ? true : false
}

isAvaliableGrenade(iEnt)  
{ 
#if defined _reapi_included
	return (get_member(iEnt, m_Grenade_usEvent) == m_usEventSmokeGrenade);
#else
	return (get_pdata_int(iEnt, g_usEvent) == m_usEventSmokeGrenade);
#endif
}  

setDamageble(id, bool:bDamageble)
{
	set_pev(id, pev_takedamage, bDamageble ? DAMAGE_AIM : DAMAGE_NO)
	set_pev(id, pev_solid, bDamageble ? SOLID_BBOX : SOLID_NOT)
}

changeWeaponName(iWeaponID, szWeaponName[MAX_WEAPONNAME_LENGH])
{
	if(!iWeaponID)	
		return

	switch(iWeaponID)
	{
		case CSW_KNIFE: 	formatex(szWeaponName, charsmax(szWeaponName), "%L", LANG_PLAYER, "SERVER_OWN_HANDS")
		case CSW_SMOKEGRENADE:	formatex(szWeaponName, charsmax(szWeaponName), "Impulse Grenade")
		case CSW_AK47:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nAK47])
		case CSW_M4A1:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nM4A1])
		case CSW_AWP:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nAWP])
		case CSW_SCOUT:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nSCOUT])
		case CSW_FAMAS:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nFAMAS])
		case CSW_AUG:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nAUG])
		case CSW_GALIL:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nGALIL])
		case CSW_SG552:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nSG552])
		case CSW_GLOCK18:	formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nGLOCK18])
		case CSW_USP:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nUSP])
		case CSW_DEAGLE:	formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nDEAGLE])
		case CSW_FIVESEVEN:	formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nFIVESEVEN])
		case CSW_ELITE:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nELITE])
		case CSW_P228:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nP228])
		case CSW_XM1014:	formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nXM1014])
		case CSW_M3:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nM3])
		case CSW_HEGRENADE:	formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nHEGRENADE])
		case CSW_M249:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nM249])
		case CSW_TMP:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nTMP])
		case CSW_MAC10:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nMAC10])
		case CSW_MP5NAVY:	formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nMP5])
		case CSW_UMP45:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nUMP45])
		case CSW_P90:		formatex(szWeaponName, charsmax(szWeaponName), g_szWeaponsDisplayName[nP90])
	}
}

knockKillManage(iKiller, iTeammateIndex)
{
	removeSomeKnockInfo(iTeammateIndex)

	set_hudmessage(225, 0, 0, -1.0, 0.35, 0, 1.0, 4.0)
	ShowSyncHudMsg(iTeammateIndex, g_hHuds[hKilledMate], "%L", iTeammateIndex, "SERVER_MATE_DIED")

	new iKnockedKiller = g_dUserData[iTeammateIndex][iKnockerID]
	if(iKiller != g_dUserData[iTeammateIndex][iKnockerID])
	{
		if(is_user_connected(iKnockedKiller) && (g_dUserData[iTeammateIndex][iKnockerID] != iTeammateIndex))
		{
			executeKill(iTeammateIndex, iKnockedKiller)
			return
		}
		else g_dUserData[iTeammateIndex][iKnockerID] = iKiller
	}
	executeKill(iTeammateIndex, iKnockedKiller)
}

checkSuicide(iTeammateIndex)
{
	if(g_dUserData[iTeammateIndex][bKnockedOut])
	{
		new iTeammateKnockerID = g_dUserData[iTeammateIndex][iKnockerID]
		if(is_user_connected(iTeammateKnockerID))
		{
			knockKillManage(iTeammateKnockerID, iTeammateIndex)
		}
		else
		{
			user_kill(iTeammateIndex, 1)
		}
	}
}

removeSomeKnockInfo(iVictim)
{
	if(task_exists(iVictim + TASK_REMOVEHEALTH))
	{
		remove_task(iVictim + TASK_REMOVEHEALTH)
	}

	client_cmd(iVictim, "stopsound")
	g_dUserData[iVictim][bKnockedOut] = false
}

getUserHelmetID(id)
{
	new iHelmetID
	switch(g_dUserData[id][iHelmetLevel])
	{
		case 1: iHelmetID = ITEM_HELMET_1_ID
		case 2: iHelmetID = ITEM_HELMET_2_ID
		case 3: iHelmetID = ITEM_HELMET_3_ID
	}
	return iHelmetID
}

getPenteAlongadoID(id)
{
	new iPenteAlongadoID
	switch(g_dUserData[id][iPenteAlongadoLevel])
	{
		case 1: iPenteAlongadoID = ITEM_PENTEALONGADO_1_ID
		case 2: iPenteAlongadoID = ITEM_PENTEALONGADO_2_ID
		case 3: iPenteAlongadoID = ITEM_PENTEALONGADO_3_ID
	}
	return iPenteAlongadoID
}

getKevlarID(id)
{
	new iKevlarID
	switch(g_dUserData[id][iArmorLevel])
	{
		case 1: iKevlarID = ITEM_KEVLAR_1_ID
		case 2: iKevlarID = ITEM_KEVLAR_2_ID
		case 3: iKevlarID = ITEM_KEVLAR_3_ID
	}
	return iKevlarID
}

getBagID(id)
{
	new iBagID
	switch(g_dUserData[id][iBagLevel])
	{
		case 1: iBagID = ITEM_BAG_1_ID
		case 2: iBagID = ITEM_BAG_2_ID
		case 3: iBagID = ITEM_BAG_3_ID
	}
	return iBagID
}

readIniData(szFileName[])
{
	new szConfig[MAX_PLAYERS]
	get_configsdir(szConfig, charsmax(szConfig))
	formatex(g_szIniFile[0], charsmax(g_szIniFile[]), "%s/%s/pubg_management/%s.ini", szConfig, FILE_FOLDER, szFileName)
	if(!file_exists(g_szIniFile[0])) 
	{
		log_to_file("pubg_log.log", "[PUBG Mod]: %L", LANG_SERVER, "SERVER_NO_CONFIG_FILE", g_szIniFile[0])
		return 0;
	}

	new iFile = fopen(g_szIniFile[0], "rt")
	while(!feof(iFile))
	{
		new szBuffer[512], szKey[50], szValue[MAX_MODEL_LENGH]
		fgets(iFile, szBuffer, charsmax(szBuffer))
		trim(szBuffer)
		
		if(szBuffer[0] == EOS || szBuffer[0] == ';' || (szBuffer[0] == '/' && szBuffer[1] == '/'))
			continue

		strtok(szBuffer, szKey, charsmax(szKey), szValue, charsmax(szValue), '=')

		trim(szKey)
		trim(szValue)

		// Commands
		if(equal(szFileName, COMMANDS_FILE))
		{
			if(equali(szKey, "RANK_COMMAND"))
			{
				g_cCommands[cRank] = szValue
			}
			else if(equali(szKey, "DUO_COMMAND"))
			{
				g_cCommands[cDuo] = szValue
			}
			else if(equali(szKey, "OUT_DUO_COMMAND"))
			{
				g_cCommands[cGetoutDuo] = szValue
			}
			else if(equali(szKey, "BAG_COMMAND"))
			{
				g_cCommands[cBag] = szValue
			}
			else if(equali(szKey, "MAIN_MENU_COMMAND"))
			{
				g_cCommands[cMainMenu] = szValue
			}
		}

		// Sounds
		else if(equal(szFileName, SOUND_EFFECTS_FILE))
		{
			if(equali(szKey, "PICKUP_KEVLAR_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sAmmoPickup] = szValue
				}
			}
			else if(equali(szKey, "PICKUP_ITEM_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sGunPickup] = szValue
				}
			}
			else if(equali(szKey, "DAMAGE_PAIN_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sDamagePain] = szValue
				}
			}
			else if(equali(szKey, "TRANSPORTER_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sTransporter] = szValue
				}
			}
			else if(equali(szKey, "HELMET_HS_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sHelmetHit] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_PLANE_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sJetFlyBy] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_ALERT_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sAlarm] = szValue
				}
			}
			else if(equali(szKey, "TRANSPORTER_DROP_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sPlaneDropSound] = szValue
				}
			}
			else if(equali(szKey, "IMPULSE_GRENADE_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sImpulseGrenadeSound] = szValue
				}
			}
			else if(equali(szKey, "WEAPON_ZOOM_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sScopeSound] = szValue
				}
			}
			else if(equali(szKey, "USING_MEDKIT_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sUsingMedkit] = szValue
				}
			}
			else if(equali(szKey, "TEAMMATE_KNOCKED_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sMateKnocked] = szValue
				}
			}
			else if(equali(szKey, "DEATH_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sDeathSound] = szValue
				}
			}
			else if(equali(szKey, "GUSPE_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sGuspeSound] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_TOUCH_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sAirdropTouchSound] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_APPEAR_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sAirDropAppearSound] = szValue
				}
			}
			else if(equali(szKey, "UNKNOCKING_MATE_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sUnknockingMate] = szValue
				}
			}
			else if(equali(szKey, "MATE_UNKNOCKED_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sMateUnknocked] = szValue
				}
			}
			else if(equali(szKey, "CRITICAL_HIT_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sCriticalHit] = szValue
				}
			}
			else if(equali(szKey, "KNOCKED_AN_ENEMY_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sKnockedAnEnemy] = szValue
				}
			}
			else if(equali(szKey, "PARACHUTE_LAND_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sParachuteLand] = szValue
				}
			}
			else if(equali(szKey, "CRITICAL_KILL_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sCriticalKill] = szValue
				}
			}
			else if(equali(szKey, "PARACHUTE_OPEN_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sParachuteOpen] = szValue
				}
			}
			else if(equali(szKey, "GUSPE_ALERT_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sGuspeAlertSound] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_LANDING_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sAirDropLandingSound] = szValue
				}
			}
			else if(equali(szKey, "TEAMMATE_DROPPED_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sTeammateDropSound] = szValue
				}
			}
			else if(equali(szKey, "SKYDIVE_SOUND"))
			{
				if(precacheSound(szValue))
				{
					g_szSoundsData[sSkyDiveSound] = szValue
				}
			}
		}
		// Itens Data
		else if(equal(szFileName, ITENS_FILE))
		{
			// KEVLAR
			if(equali(szKey, "KEVLAR_ENABLED")) 
			{
				g_szItens[iKevlarLv1][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iKevlarLv2][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iKevlarLv3][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "KEVLAR_LV1_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iKevlarLv1][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "KEVLAR_LV1_ID"))
			{
				g_szItens[iKevlarLv1][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "KEVLAR_LV1_LEVEL"))
			{
				g_szItens[iKevlarLv1][iItemInfoLevel] = str_to_num(szValue)

			}
			else if(equali(szKey, "KEVLAR_LV2_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iKevlarLv2][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "KEVLAR_LV2_ID"))
			{
				g_szItens[iKevlarLv2][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "KEVLAR_LV2_LEVEL"))
			{
				g_szItens[iKevlarLv2][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "KEVLAR_LV3_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iKevlarLv3][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "KEVLAR_LV3_ID"))
			{
				g_szItens[iKevlarLv3][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "KEVLAR_LV3_LEVEL"))
			{
				g_szItens[iKevlarLv3][iItemInfoLevel] = str_to_num(szValue)
			}
			
			//MED KIT
			if(equali(szKey, "MEDKIT_ENABLED")) 
			{
				g_szItens[iMedKit][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "MEDKIT_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iMedKit][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "MEDKIT_ID"))
			{
				g_szItens[iMedKit][iItemID] = str_to_num(szValue)
			}
			
			// HELMET
			if(equali(szKey, "HELMET_ENABLED")) 
			{
				g_szItens[iHelmetLv1][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iHelmetLv2][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iHelmetLv3][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "HELMET_LV1_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iHelmetLv1][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "HELMET_LV1_ID"))
			{
				g_szItens[iHelmetLv1][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "HELMET_LV1_LEVEL"))
			{
				g_szItens[iHelmetLv1][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "HELMET_LV2_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iHelmetLv2][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "HELMET_LV2_ID"))
			{
				g_szItens[iHelmetLv2][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "HELMET_LV2_LEVEL"))
			{
				g_szItens[iHelmetLv2][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "HELMET_LV3_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iHelmetLv3][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "HELMET_LV3_ID"))
			{
				g_szItens[iHelmetLv3][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "HELMET_LV3_LEVEL"))
			{
				g_szItens[iHelmetLv3][iItemInfoLevel] = str_to_num(szValue)
			}
			
			// BAG
			if(equali(szKey, "BAG_ENABLED")) 
			{
				g_szItens[iBagLv1][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iBagLv2][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iBagLv3][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "BAG_LV1_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iBagLv1][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "BAG_LV1_ID"))
			{
				g_szItens[iBagLv1][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "BAG_LV1_LEVEL"))
			{
				g_szItens[iBagLv1][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "BAG_LV2_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iBagLv2][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "BAG_LV2_ID"))
			{
				g_szItens[iBagLv2][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "BAG_LV2_LEVEL"))
			{
				g_szItens[iBagLv2][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "BAG_LV3_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iBagLv3][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "BAG_LV3_ID"))
			{
				g_szItens[iBagLv3][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "BAG_LV3_LEVEL"))
			{
				g_szItens[iBagLv3][iItemInfoLevel] = str_to_num(szValue)
			}
			
			// IMPULSE GRENADE
			if(equali(szKey, "IMPULSE_ENABLED")) 
			{
				g_szItens[iImpulseGrenade][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "IMPULSE_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iImpulseGrenade][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "IMPULSE_ID"))
			{
				g_szItens[iImpulseGrenade][iItemID] = str_to_num(szValue)
			}
			
			// SCOPE 2X
			if(equali(szKey, "SCOPE_2X_ENABLED")) 
			{
				g_szItens[iScope2x][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "SCOPE_2X_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iScope2x][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "SCOPE_2X_ID"))
			{
				g_szItens[iScope2x][iItemID] = str_to_num(szValue)
			}
			
			// SCOPE 4X
			if(equali(szKey, "SCOPE_4X_ENABLED")) 
			{
				g_szItens[iScope4x][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "SCOPE_4X_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iScope4x][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "SCOPE_4X_ID"))
			{
				g_szItens[iScope4x][iItemID] = str_to_num(szValue)
			}
			
			// ELONGATED COMB
			if(equali(szKey, "ELONGATED_COMB_ENABLED")) 
			{
				g_szItens[iElongatedCombLv1][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iElongatedCombLv2][bIsItemEnabled] = bool:str_to_num(szValue)
				g_szItens[iElongatedCombLv3][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "ELONGATED_COMB_LV1_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iElongatedCombLv1][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "ELONGATED_COMB_LV1_ID"))
			{
				g_szItens[iElongatedCombLv1][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "ELONGATED_COMB_LV1_LEVEL"))
			{
				g_szItens[iElongatedCombLv1][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "ELONGATED_COMB_LV2_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iElongatedCombLv2][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "ELONGATED_COMB_LV2_ID"))
			{
				g_szItens[iElongatedCombLv2][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "ELONGATED_COMB_LV2_LEVEL"))
			{
				g_szItens[iElongatedCombLv2][iItemInfoLevel] = str_to_num(szValue)
			}
			else if(equali(szKey, "ELONGATED_COMB_LV3_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iElongatedCombLv3][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "ELONGATED_COMB_LV3_ID"))
			{
				g_szItens[iElongatedCombLv3][iItemID] = str_to_num(szValue)
			}
			else if(equali(szKey, "ELONGATED_COMB_LV3_LEVEL"))
			{
				g_szItens[iElongatedCombLv3][iItemInfoLevel] = str_to_num(szValue)
			}
			
			//GUSPE
			if(equali(szKey, "GUSPE_ENABLED")) 
			{
				g_szItens[iGuspe][bIsItemEnabled] = bool:str_to_num(szValue)
			}
			else if(equali(szKey, "GUSPE_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szItens[iGuspe][szItemModel] = szValue
				}
			}
			else if(equali(szKey, "GUSPE_ID"))
			{
				g_szItens[iGuspe][iItemID] = str_to_num(szValue)
			}
		}
		else if(equal(szFileName, RESOURCES_FILE))
		{
			if(equali(szKey, "PLANE_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szModelsData[mTransporter] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_PLANE_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szModelsData[mAirDropPlane] = szValue
				}
			}
			else if(equali(szKey, "PLAYER_PARACHUTE_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szModelsData[mUserParachute] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_BOX_MODEL"))
			{
				if(precacheModel(szValue))
				{
					g_szModelsData[mAirDrop] = szValue
				}
			}
			else if(equali(szKey, "AIRDROP_PARACHUTE_MODEL"))
			{				
				if(precacheModel(szValue))
				{
					g_szModelsData[mAirDropParachute] = szValue
				}
			}
			else if(equali(szKey, "IMPULSE_GRENADE_VIEW_MODEL"))
			{				
				if(precacheModel(szValue))
				{
					g_szModelsData[mImpulseGrenadeViewModel] = szValue
				}
			}
			else if(equali(szKey, "IMPULSE_GRENADE_WORLD_MODEL"))
			{		
				if(precacheModel(szValue))
				{
					g_szModelsData[mImpulseGrenadePlayerModel] = szValue
				}
			}
		}
		else if(equal(szFileName, WIN_SOUNDS))
		{
			if(precacheSound(szKey))
			{
				ArrayPushString(g_arWinSounds, szKey)
			}
		}
		else if(equal(szFileName, WEAPONS_MODELS))
		{
			new szKeyModel[MAX_MODEL_LENGH], szKeyModelID[MAX_PLAYERS]
			parse
			(
				szKey, 
				szKeyModel, charsmax(szKeyModel),
				szKeyModelID, charsmax(szKeyModelID)
			)

			g_szWeaponsModels[g_iWeaponsNum][iWeaponInfoID] = str_to_num(szKeyModelID)
			if(szValue[0] && file_exists(szValue))
			{
				precacheModel(szValue)
				g_szWeaponsModels[g_iWeaponsNum][szModelName] = szValue
			}
			else
			{
				new szModel[MAX_MODEL_LENGH]
				replace(szKeyModel, charsmax(szKeyModel), "weapon_", "")
				formatex(szModel, charsmax(szModel), "models/w_%s.mdl", szKeyModel)
				if(containi(szModel, "navy") != -1)
				{
					replace(szModel, charsmax(szModel), "navy", "")
				}
				precacheModel(szModel)
				g_szWeaponsModels[g_iWeaponsNum][szModelName] = szModel
			}
			g_iWeaponsNum++
		}
	}
	fclose(iFile)
	return 1;
}

readEntsPoints()
{
	g_iVecsNum[vecWeapons] = 0
	if (!file_exists(g_szIniFile[1])) 
	{
		log_to_file(LOG_FILE, "[PUBG Mod]: %L", LANG_SERVER, "SERVER_NO_SPAWN_POINTS", g_szIniFile[1])
		return 0;
	}

	new iFile = fopen(g_szIniFile[1], "rt")
	while(!feof(iFile) && (g_iVecsNum[vecWeapons] < MAX_SPAWNS))
	{
		new szBuffer[512], iPos[4][8]
		fgets(iFile, szBuffer, charsmax(szBuffer))
		trim(szBuffer)
		
		if(szBuffer[0] == EOS || szBuffer[0] == ';' || (szBuffer[0] == '/' && szBuffer[1] == '/'))
			continue

		parse
		(
			szBuffer, 
			iPos[1], charsmax(iPos[]),
			iPos[2], charsmax(iPos[]),  
			iPos[3], charsmax(iPos[])
		)
			
		g_fSpawnVecs[g_iVecsNum[vecWeapons]][0] = str_to_float(iPos[1])
		g_fSpawnVecs[g_iVecsNum[vecWeapons]][1] = str_to_float(iPos[2])
		g_fSpawnVecs[g_iVecsNum[vecWeapons]][2] = str_to_float(iPos[3])

		g_iVecsNum[vecWeapons]++
	} 

	fclose(iFile)
	return (g_iVecsNum[vecWeapons] ? 1 : 0);
}

readWeaponsNames()
{
	if(!file_exists(g_szIniFile[3])) 
	{
		log_to_file(LOG_FILE, "[PUBG Mod]: %L", LANG_SERVER, "SERVER_NO_CONFIG_FILE", g_szIniFile[3])
		return 0;
	}

	new iFile = fopen(g_szIniFile[3], "rt")
	while(!feof(iFile))
	{
		new szBuffer[512], szKey[50], szValue[MAX_MODEL_LENGH]
		fgets(iFile, szBuffer, charsmax(szBuffer))
		trim(szBuffer)
		
		if(szBuffer[0] == EOS || szBuffer[0] == ';' || (szBuffer[0] == '/' && szBuffer[1] == '/'))
			continue

		strtok(szBuffer, szKey, charsmax(szKey), szValue, charsmax(szValue), '=')

		trim(szKey)
		trim(szValue)

		if(equal(szKey, "AK47")) 	formatex(g_szWeaponsDisplayName[nAK47], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "M4A1")) 	formatex(g_szWeaponsDisplayName[nM4A1], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "AWP")) 	formatex(g_szWeaponsDisplayName[nAWP], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "SCOUT")) 	formatex(g_szWeaponsDisplayName[nSCOUT], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "FAMAS")) 	formatex(g_szWeaponsDisplayName[nFAMAS], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "AUG")) 	formatex(g_szWeaponsDisplayName[nAUG], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "GALIL")) 	formatex(g_szWeaponsDisplayName[nGALIL], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "SG552")) 	formatex(g_szWeaponsDisplayName[nSG552], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "GLOCK18"))	formatex(g_szWeaponsDisplayName[nGLOCK18], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "USP")) 	formatex(g_szWeaponsDisplayName[nUSP], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "DEAGLE")) 	formatex(g_szWeaponsDisplayName[nDEAGLE], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "FIVESEVEN")) 	formatex(g_szWeaponsDisplayName[nFIVESEVEN], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "ELITE")) 	formatex(g_szWeaponsDisplayName[nELITE], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "P228")) 	formatex(g_szWeaponsDisplayName[nP228], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "XM1014")) 	formatex(g_szWeaponsDisplayName[nXM1014], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "M3")) 		formatex(g_szWeaponsDisplayName[nM3], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "HEGRENADE")) 	formatex(g_szWeaponsDisplayName[nHEGRENADE], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "M249")) 	formatex(g_szWeaponsDisplayName[nM249], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "TMP")) 	formatex(g_szWeaponsDisplayName[nTMP], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "MAC10")) 	formatex(g_szWeaponsDisplayName[nMAC10], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "MP5NAVY")) 	formatex(g_szWeaponsDisplayName[nMP5], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "UMP45")) 	formatex(g_szWeaponsDisplayName[nUMP45], charsmax(g_szWeaponsDisplayName[]), szValue)
		if(equal(szKey, "P90")) 	formatex(g_szWeaponsDisplayName[nP90], charsmax(g_szWeaponsDisplayName[]), szValue)
	}
	fclose(iFile)
	return 1;
}

readPlanePoints()
{
	if (!file_exists(g_szIniFile[2])) 
	{
		log_to_file("pubg_log.log", "[PUBG Mod]: %L", LANG_SERVER, "SERVER_NO_SPAWN_VECTOR_FOUND", g_szIniFile[2])
		return 0;
	}

	new iFile = fopen(g_szIniFile[2], "rt")
	while(!feof(iFile))
	{
		new szBuffer[512]
		fgets(iFile, szBuffer, charsmax(szBuffer))
		trim(szBuffer)
		
		if(!szBuffer[0] || szBuffer[0] == ';' || (szBuffer[0] == '/' && szBuffer[1] == '/'))
			continue

		new szVecs[11][8]
		parse
		(
			szBuffer, 
			szVecs[1], charsmax(szVecs[]),
			szVecs[2], charsmax(szVecs[]),
			szVecs[3], charsmax(szVecs[]),
			szVecs[4], charsmax(szVecs[]),
			szVecs[5], charsmax(szVecs[]),
			szVecs[6], charsmax(szVecs[]),
			szVecs[7], charsmax(szVecs[]),
			szVecs[8], charsmax(szVecs[]),
			szVecs[9], charsmax(szVecs[]),
			szVecs[10], charsmax(szVecs[])
		)
		
		// Origins
		g_fPlaneOrigins[g_iTotalVecs[vOrigin]][0] = str_to_float(szVecs[1])
		g_fPlaneOrigins[g_iTotalVecs[vOrigin]][1] = str_to_float(szVecs[2])
		g_fPlaneOrigins[g_iTotalVecs[vOrigin]][2] = str_to_float(szVecs[3])
		g_iTotalVecs[vOrigin]++
		
		// Angles
		g_fPlaneAngles[g_iTotalVecs[vAngles]][0] = str_to_float(szVecs[4])
		g_fPlaneAngles[g_iTotalVecs[vAngles]][1] = str_to_float(szVecs[5])
		g_fPlaneAngles[g_iTotalVecs[vAngles]][2] = str_to_float(szVecs[6])
		g_iTotalVecs[vAngles]++
			
		// Velocities
		g_fPlaneVelocities[g_iTotalVecs[vVelocities]][0] = str_to_float(szVecs[7])
		g_fPlaneVelocities[g_iTotalVecs[vVelocities]][1] = str_to_float(szVecs[8])
		g_fPlaneVelocities[g_iTotalVecs[vVelocities]][2] = str_to_float(szVecs[9])
		g_iTotalVecs[vVelocities]++

		// Plane Remove time
		g_fPlaneRemoveTime[g_iTotalVecs[vRemoveTime]++] = str_to_float(szVecs[10])

		g_iVecsNum[vecPlane]++

	} 
	
	fclose(iFile)
	return (g_iVecsNum[vecPlane] ? 1 : 0)
}

clientPlaySound(id, szSound[], bool:bIsEffectSound = false)
{
	if(bIsEffectSound)
	{
		emit_sound(id, CHAN_ITEM, szSound, VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
	}
	else client_cmd(id, "%s ^"%s^"", (equal(szSound[strlen(szSound) - 4], ".mp3")) ? "mp3 play" : "spk", szSound)
}

Float:distanceToGround(id) 
{ 
	new Float:fStart[3], Float:fEnd[3]
	entity_get_vector(id, EV_VEC_origin, fStart)
	if(entity_get_int(id, EV_INT_flags) & FL_DUCKING) 
	{ 
		fStart[2] += 18.0
	} 

	fEnd[0] = fStart[0] 
	fEnd[1] = fStart[1]
	fEnd[2] = fStart[2] - 9999.0

	new iPtr = create_tr2(), Float:fFraction
	engfunc(EngFunc_TraceHull, fStart, fEnd, IGNORE_MONSTERS, HULL_HUMAN, id, iPtr)
	get_tr2(iPtr, TR_flFraction, fFraction)
	free_tr2(iPtr)

	return (fFraction * 9999.0)
} 

precacheSound(szSound[])
{
	if(equal(szSound[strlen(szSound) - 4], ".mp3")) 
	{
		if(file_exists(szSound))
		{
			precache_generic(szSound)
			return true
		}
	}
	else 
	{
		precache_sound(szSound)
		return true
	}
	return false
}

precacheModel(szModel[])
{
	if(file_exists(szModel))
	{
		precache_model(szModel)
		return true
	}
	return false
}

airDropMenu(id, bool:bOpenMenu)
{
	g_dUserData[id][bIsAirDropMenuOpen] = bOpenMenu
	bOpenMenu ? showAirDropItems(id) : closeMenu(id)
}

executeKill(iKiller, iVictim)		
{
	if(is_user_connected(iKiller) && is_user_connected(iVictim))
	{
		ExecuteHamB(Ham_Killed, iKiller, iVictim, 1)
	}
}

killEntity(iEnt)			
{
	if(pev_valid(iEnt))
	{
		engfunc(EngFunc_RemoveEntity, iEnt)
	}
}

getPlayersInThePlaneNum()
{
	new iPlayers[MAX_PLAYERS], iNum, iPlayersInPlane
	get_players(iPlayers, iNum, "ach")
	for(new i;i < iNum;i++)
	{
		if(g_dUserData[iPlayers[i]][bIsOnPlane])
		{
			iPlayersInPlane++
		}
	}
	return iPlayersInPlane
}

Float:GetMaxHeightByStartOrigin(Float:fOrigin[3]) 
{ 	
	new pcCurrent
	while(engfunc(EngFunc_PointContents, fOrigin) == CONTENTS_EMPTY)
	{
		fOrigin[2] += 5.0
	}
	
	pcCurrent = engfunc(EngFunc_PointContents, fOrigin)
	return ((pcCurrent == CONTENTS_SKY) || (pcCurrent == CONTENTS_SOLID)) ? fOrigin[2] - 75.0 : fOrigin[2]
} 

getItemNameByID(id, sItemID, szFormat[25])
{
	switch(sItemID)
	{
		case ITEM_KEVLAR_1_ID, ITEM_KEVLAR_2_ID, ITEM_KEVLAR_3_ID:			formatex(szFormat, charsmax(szFormat), "%L lv. %d", id, "SERVER_ITEM_KEVLAR", g_szItens[iItensList:sItemID][iItemInfoLevel])
		case ITEM_HELMET_1_ID, ITEM_HELMET_2_ID, ITEM_HELMET_3_ID:			formatex(szFormat, charsmax(szFormat), "%L lv. %d", id, "SERVER_ITEM_HELMET", g_szItens[iItensList:sItemID][iItemInfoLevel])
		case ITEM_MEDKIT_ID:								formatex(szFormat, charsmax(szFormat), "MedKit")
		case ITEM_BAG_1_ID, ITEM_BAG_2_ID, ITEM_BAG_3_ID:				formatex(szFormat, charsmax(szFormat), "%L lv. %d", id, "SERVER_ITEM_BAG", g_szItens[iItensList:sItemID][iItemInfoLevel])
		case ITEM_IMPULSE_ID:								formatex(szFormat, charsmax(szFormat), "Impulse Grenade")
		case ITEM_SCOPE2X_ID, ITEM_SCOPE4X_ID:						formatex(szFormat, charsmax(szFormat), "%L %s", id, "SERVER_ITEM_SCOPE", (sItemID == ITEM_SCOPE2X_ID) ? "2X" : "4X")
		case ITEM_PENTEALONGADO_1_ID, ITEM_PENTEALONGADO_2_ID, ITEM_PENTEALONGADO_3_ID:	formatex(szFormat, charsmax(szFormat), "%L lv. %d", id, "SERVER_ITEM_ELONGATED_COMB", g_szItens[iItensList:sItemID][iItemInfoLevel])
		case ITEM_GUSPE_ID:								formatex(szFormat, charsmax(szFormat), "Guspe")
	}
}
