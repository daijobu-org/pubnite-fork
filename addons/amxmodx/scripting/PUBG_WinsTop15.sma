#include <amxmodx>
#include <fvault>
#include <pubg_mod>

#define PLUGIN "PUBG Top 15"
#define VERSION "1.0"
#define AUTHOR "guipatinador/EFFx"

#if !defined MAX_PLAYERS
#define MAX_PLAYERS		32
#endif

#define g_VaultWins		"PUBG_Wins"
#define g_VaultNames		"PUBG_names"

enum _:FvaultData 
{
	szSteamID[35],
	szSkillP_Data[128]
}

new g_szAuthID[MAX_PLAYERS + 1][35]
new g_szName[MAX_PLAYERS + 1][32]

public plugin_init() 
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	if(is_plugin_loaded("BattleRoyale Mod") == -1)
	{
		set_fail_state("MOD DISABLED")
	}
	
	register_clcmd("say /pub_top15", "cmdPubTop15")
}

public client_authorized(id)
{
	if(!is_user_bot(id))
	{
		get_user_authid(id, g_szAuthID[id], charsmax(g_szAuthID[]))
		get_user_info(id, "name", g_szName[id], charsmax(g_szName[]))
		fvault_set_data(g_VaultNames, g_szAuthID[id], g_szName[id])
	}
}

public client_disconnect(id)
{
	CheckLevelAndSave(id)
}

public pubg_winner(id, iMateID)
{
	CheckLevelAndSave(id)
	if(iMateID)
	{
		CheckLevelAndSave(iMateID)
	}
}

public CheckLevelAndSave(id)
{
	if(!is_user_bot(id))
	{
		new szFormattedData[128]
		formatex(szFormattedData, charsmax(szFormattedData),"%i", getUserWins(id))
		fvault_set_data(g_VaultWins, g_szAuthID[id], szFormattedData)
	}
}

public cmdPubTop15(id)
{
	new iLen, szMotd[2300]
	iLen = formatex(szMotd, charsmax(szMotd),
	"<body bgcolor=#A4BED6>\
	<table width=100%% cellpadding=2 cellspacing=0 border=0>\
	<tr align=center bgcolor=#52697B>\
	<th width=4%%>#\
	<th width=30%% align=left>Player\
	<th width=8%%>%L", LANG_PLAYER, "SERVER_WINS")
	
	new Array:aKey = ArrayCreate(35)
	new Array:aData = ArrayCreate(128)
	new Array:aAll = ArrayCreate(FvaultData)
	
	fvault_load(g_VaultWins, aKey, aData)
	
	new iArraySize = ArraySize(aKey)
	new iData[FvaultData]
	
	for(new i; i < iArraySize; i++)
	{
		ArrayGetString(aKey, i, iData[szSteamID], charsmax(iData[szSteamID]))
		ArrayGetString(aData, i, iData[szSkillP_Data], charsmax(iData[szSkillP_Data]))
		
		ArrayPushArray(aAll, iData)
	}
	
	ArraySort(aAll, "SortData")
	
	new szPlayerWonRounds[7]
	
	new szName[22]
	new iSize = clamp(iArraySize, 0, 15)
	
	for(new j; j < iSize; j++ )
	{
		ArrayGetArray(aAll, j, iData)
		fvault_get_data(g_VaultNames, iData[szSteamID], szName, charsmax(szName))
		
		replace_all(szName, charsmax(szName), "<", "[")
		replace_all(szName, charsmax(szName), ">", "]")
		
		parse(iData[szSkillP_Data], szPlayerWonRounds, charsmax(szPlayerWonRounds))

		iLen += formatex(szMotd[iLen], charsmax(szMotd) - iLen, "<tr align=center>")
		iLen += formatex(szMotd[iLen], charsmax(szMotd) - iLen, "<td>%i.", j + 1)
		iLen += formatex(szMotd[iLen], charsmax(szMotd) - iLen, "<td align=left>%s", szName)
		iLen += formatex(szMotd[iLen], charsmax(szMotd) - iLen, "<td>%s", szPlayerWonRounds)
	}
	
	iLen += formatex(szMotd[iLen], charsmax(szMotd) - iLen, "</table></body>")
	show_motd(id, szMotd, "PUBG Top Wins")
	
	ArrayDestroy(aKey)
	ArrayDestroy(aData)
	ArrayDestroy(aAll)
}

public SortData(Array:aArray, iItem1, iItem2, iData[], iDataSize)
{
	new Data1[FvaultData]
	new Data2[FvaultData]
	
	ArrayGetArray(aArray, iItem1, Data1)
	ArrayGetArray(aArray, iItem2, Data2)
	
	new szPoints_1[7]
	parse(Data1[szSkillP_Data], szPoints_1, charsmax(szPoints_1))
	
	new szPoints_2[7]
	parse(Data2[szSkillP_Data], szPoints_2, charsmax(szPoints_2))
	
	new iCount1 = str_to_num(szPoints_1)
	new iCount2 = str_to_num(szPoints_2)
	
	return (iCount1 > iCount2) ? -1 : ((iCount1 < iCount2) ? 1 : 0)
}
