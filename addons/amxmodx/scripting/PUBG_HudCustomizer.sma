#include <amxmodx> 
#include <amxmisc> 

#define PLUGIN "HUD Customizer 0.4" 
#define VERSION "0.4" 
#define AUTHOR "Igoreso" 

#define HUD_HIDE_FLASH (1<<1)
#define HUD_HIDE_MONEY (1<<5)

new g_msgHideWeapon

new bool:g_bHideFlash
new bool:g_bHideMoney

new g_cvarHideFlash
new g_cvarHideMoney

public plugin_init() 
{ 
	register_plugin(PLUGIN, VERSION, AUTHOR) 
	
	if(is_plugin_loaded("BattleRoyale Mod") == -1)
	{
		set_fail_state("MOD DISABLED")
	}

	g_msgHideWeapon = get_user_msgid("HideWeapon")
	register_event("ResetHUD", "onResetHUD", "b")
	register_message(g_msgHideWeapon, "msgHideWeapon")
	
	g_cvarHideFlash = register_cvar("pubg_hud_hide_flashlight", "1")
	g_cvarHideMoney = register_cvar("pubg_hud_hide_money", "1")

	HudApplyCVars()
} 

public onResetHUD(id)
{
	HudApplyCVars()
	
	new iHideFlags = GetHudHideFlags()
	if(iHideFlags)
	{
		message_begin(MSG_ONE, g_msgHideWeapon, _, id)
		write_byte(iHideFlags)
		message_end()
	}	
}

public msgHideWeapon()
{
	new iHideFlags = GetHudHideFlags()
	if(iHideFlags)
		set_msg_arg_int(1, ARG_BYTE, get_msg_arg_int(1) | iHideFlags)
}

GetHudHideFlags()
{
	new iFlags

	if( g_bHideFlash )
		iFlags |= HUD_HIDE_FLASH
	if( g_bHideMoney )
		iFlags |= HUD_HIDE_MONEY 

	return iFlags
}

HudApplyCVars()
{
	g_bHideFlash = bool:get_pcvar_num(g_cvarHideFlash)
	g_bHideMoney = bool:get_pcvar_num(g_cvarHideMoney)
}
