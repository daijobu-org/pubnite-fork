#include <amxmodx>
#include <fakemeta>
#include <cstrike>

#define PLUGIN "Roundend Blocker"
#define VERSION "1.4"
#define AUTHOR "Jim"

#define BOT_NUM 2

new g_bot[BOT_NUM]
new g_botnum

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	if(is_plugin_loaded("BattleRoyale Mod") == -1)
	{
		set_fail_state("MOD DISABLED")
	}

	register_logevent("round_start", 2,"1=Round_Start")
}

public round_start() 
{
	if(!g_botnum)
	{
		create_bot(1)
		create_bot(2)
	}

	for(new i = 1;i <= get_maxplayers();i++)
	{
		if(is_user_bot(i))
		{
			hideBot(i)
		}
	}
}

create_bot(iTeam) 
{	
	new botname[32]
	formatex(botname, charsmax(botname), iTeam == 1 ? "PUBG [TR Bot]" : "PUBG [CT Bot]")
	new bot = engfunc(EngFunc_CreateFakeClient, botname)
	if(!bot) 
	{
		return
	}
	new ptr[128]
	engfunc(EngFunc_FreeEntPrivateData, bot)
	dllfunc(DLLFunc_ClientConnect, bot, botname, "127.0.0.1", ptr)
	if(!is_user_connected(bot)) 
	{
		return
	}
	
	g_bot[g_botnum++] = bot
	dllfunc(DLLFunc_ClientPutInServer, bot)
	
	hideBot(bot)
	cs_set_user_team(bot, _:iTeam)
}

hideBot(bot)
{
	set_pev(bot, pev_spawnflags, pev(bot, pev_spawnflags) | FL_FAKECLIENT)
	set_pev(bot, pev_flags, pev(bot, pev_flags) | FL_FAKECLIENT)
	set_pev(bot, pev_model, "")
	set_pev(bot, pev_takedamage, DAMAGE_NO)
	set_pev(bot, pev_solid, SOLID_NOT)
	set_pev(bot, pev_effects, pev(bot, pev_effects) | EF_NODRAW)
	
	engfunc(EngFunc_SetOrigin, bot, Float:{9999.0, 9999.0, 9999.0})
}

public kick_bot() 
{
	if(!g_botnum) 
	{
		return
	}
	server_cmd("kick #%d", get_user_userid(g_bot[--g_botnum]))
}

public plugin_end() 
{
	kick_bot()
}
