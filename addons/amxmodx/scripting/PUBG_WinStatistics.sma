#include <amxmodx>
#include <cstrike>
#include <hamsandwich>
#include <pubg_mod>
#tryinclude <reapi>

#if !defined _reapi_included
const OFFSET_LINUX_WEAPONS =		4
const m_pPlayer = 			41
#endif

new const AllWeapons[][] = 
{
	"weapon_p228", 
	"weapon_elite", 
	"weapon_fiveseven", 
	"weapon_ump45",
	"weapon_galil", 
	"weapon_famas", 
	"weapon_usp", 
	"weapon_glock18", 
	"weapon_awp", 
	"weapon_mp5navy", 
	"weapon_m249",
	"weapon_m3", 
	"weapon_m4a1",
	"weapon_tmp", 
	"weapon_deagle", 
	"weapon_ak47",
	"weapon_mac10",
	"weapon_xm1014",
	"weapon_p90",
	"weapon_scout",
	"weapon_sg550",
	"weapon_sg552",
	"weapon_g3sg1",
	"weapon_aug"
}

enum dUserData
{
	iFrags,
	iHits,
	iShots,
	iHS
}

new g_iUserData[33][dUserData]

public plugin_init() 
{
#if defined _reapi_included
	register_plugin("PUBG Win Statistics", "1.0 (reAPI Fork)", "EFFx");

	if (!is_rehlds() || !is_regamedll())
	{
		set_fail_state("reHLDS and reGameDLL_CS are required, if not using them, please compile without reapi.inc");
	}
#else
	register_plugin("PUBG Win Statistics", "1.0", "EFFx");
#endif

	if (is_plugin_loaded("BattleRoyale Mod") == -1)
	{
		set_fail_state("MOD DISABLED");
	}

	register_dictionary("effxs_battleroyale.txt")
	
	for(new i;i < sizeof AllWeapons;i++)
	{
		RegisterHam(Ham_Weapon_PrimaryAttack, AllWeapons[i], "ham_PrimaryAttack_Pre", 1)
	}

	RegisterHam(Ham_Spawn, "player", "ham_PlayerSpawn", 1)
	RegisterHam(Ham_TakeDamage, "player", "ham_TakeDamage", 1)
	
	register_event("DeathMsg", "event_deathMsg", "a")
}

public client_disconnected(id)
{
	resetData(id)
}

public ham_TakeDamage(iVictim, iInflictor, iAttacker, Float:fDamage, iDamageBits)
{
	if(is_user_alive(iAttacker) && (iAttacker != iVictim))
	{
		g_iUserData[iAttacker][iHits]++
	}
}

public ham_PlayerSpawn(id)
{
	if(is_user_alive(id))
	{
		resetData(id)
	}
}

public event_deathMsg()
{
	new iAttacker = read_data(1)
	if(iAttacker != read_data(2))
	{
		g_iUserData[iAttacker][iFrags]++
		if(read_data(3))
		{
			g_iUserData[iAttacker][iHS]++
		}
	}
}

public ham_PrimaryAttack_Pre(iEnt)
{
	if(cs_get_weapon_ammo(iEnt) >= 1)
	{
#if defined _reapi_included
		g_iUserData[get_member(iEnt, m_pPlayer)][iShots]++;
#else
		g_iUserData[get_pdata_cbase(iEnt, m_pPlayer, OFFSET_LINUX_WEAPONS)][iShots]++;
#endif
	}
}

public pubg_winner(id, iMateID)
{
	showMotd(id, (iMateID && is_user_connected(iMateID)) ? iMateID : 0)
	if (iMateID)
	{
		showMotd(iMateID, id)
	}
}

showMotd(id, iMateID)
{
	new iLen, sBuffer[2300], szWinFormat[80]
	iLen = formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<meta charset=utf-8><body bgcolor=#000000><pre>")
	
	if(iMateID)
	{
		new szMateName[32]
		get_user_name(iMateID, szMateName, charsmax(szMateName))
		
		new szPreposition[5]
		formatex(szPreposition, charsmax(szPreposition), " %L ", id, "SERVER_PREPOSITION")
		formatex(szWinFormat, charsmax(szWinFormat), "<b> %s<font color=red>%s</color><font color=white></b>", szPreposition, szMateName)
	}
	
	new szFrags[15], szHits[15], szShots[15], szHS[15], szAcc[15]
	formatex(szFrags, charsmax(szFrags), "%L", id, "SERVER_FRAGS")
	formatex(szHits, charsmax(szHits), "%L", id, "SERVER_HITS")
	formatex(szShots, charsmax(szShots), "%L", id, "SERVER_SHOTS")
	formatex(szHS, charsmax(szHS), "%L", id, "SERVER_HS")
	formatex(szAcc, charsmax(szAcc), "%L", id, "SERVER_ACCURACY")
	
	new szCongratulations[40]
	formatex(szCongratulations, charsmax(szCongratulations), "%L", id, "SERVER_CONGRATULATIONS")
	
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<center><font size=6><font color=gold><b>%s</font></color>^n<font color=white><font size=4>%L <font color=lime>%d</color></center></color>", szCongratulations, id, "SERVER_MOTD_WIN", szWinFormat[0] ? szWinFormat : "", getUserWins(id))
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<hr noshade>")
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<font color=magenta><font size=4>- %s:</font><font color=white> %d</color>", szFrags, g_iUserData[id][iFrags])
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<font color=magenta><br><font size=4>- %s:</font></color><font color=white> %d</color>", szHits, g_iUserData[id][iHits])
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<font color=magenta><br><font size=4>- %s:</font></color><font color=white> %d</color>", szShots, g_iUserData[id][iShots])
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<font color=magenta><br><font size=4>- %s:</font></color><font color=white> %d</color>", szHS, g_iUserData[id][iHS])
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<font color=magenta><br><font size=4>- %s:</font></color><font color=white> %d%</color>", szAcc, fAccuracy(id))
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<hr noshade>")
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<center><font color=white>Player Unknown's Battleground by</color><font color=gold> EFFx (c)</color></b></center>")
	iLen += formatex(sBuffer[iLen], charsmax(sBuffer) - iLen, "<hr noshade>")
	
	new szMotdTittle[40]
	formatex(szMotdTittle, charsmax(szMotdTittle), "%L", id, "SERVER_MOTD_STATISTICS_TITTLE")
	show_motd(id, sBuffer, szMotdTittle)
} 

fAccuracy(id)
{
	return g_iUserData[id][iShots] ? ((g_iUserData[id][iHits] * g_iUserData[id][iShots]) / 100) : 0
}

resetData(id)
{
	for(new i;i < sizeof g_iUserData[];i++)
	{
		g_iUserData[id][dUserData:i] = 0
	}
}