#if defined _pubg_mod_included
    #endinput
#endif

#define _pubg_mod_included

/**
* Pass the winner(s) ID
* @id			one winner
* @iMateID		his teammate's id
*/
forward pubg_winner(id, iMateID)

/**
* Checks if the user is outside the safe zone
*@id			the id of the user to check if he's in the redzone
*/
native isInRedZone(id)

/*
* gets the redzone compression level
*/
native getRedZoneLevel()

/**
* Checks if the attacker and the victim are teammates
* @iAttacker		the id of the attacker
* @iVictim		the id of the victim
*/
native isTeamMate(iAttacker, iVictim)

/**
* Gets the win ammount of the player
* @id			the id of the player to check
*/
native getUserWins(id)