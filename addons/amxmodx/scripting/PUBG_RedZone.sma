#include <amxmodx>
#include <fakemeta>
#include <engine>
#include <hamsandwich>
#include <pubg_mod>

#define PLUGIN    			"PUBG Red Zone"
#define VERSION     			"1.0"
#define AUTHOR     			"ggv/EFFx"

#define TASK_COMPRESSION 		23102511
#define TASK_FIXDISK 			23201221
#define TASK_STOPCOMPRESSION 		23301341
#define TASK_ZONEFIX 			23541141
#define TASK_TIMER 			23603212
#define TASK_SOUND_COMPRESSION		23702255
#define TASK_DAMAGE 			23802203

#define WEIGH 				50
#define NUMSIDES 			13

new const ZONE_SPRITE[] = 		"sprites/pubg/pubg_redzone.spr"
new const zone_EntName[] = 		"pubg_redzone"

new const g_szGasStopedSound[] = 	"pubg/effects/redzone_stoped.wav" 
new const g_szGasWallkingSound[] = 	"pubg/effects/redzone_walking.wav" 
new const g_szOutSideGas[] =		"pubg/effects/outside_redzone.wav"
new const g_szInsideGas[] =		"pubg/effects/inside_redzone.wav"

new const g_szHitSounds[][] =
{
	"player/bhit_flesh-1.wav",
	"player/bhit_flesh-2.wav",
	"player/bhit_flesh-3.wav",
	"player/bhit_kevlar-1.wav"
}

new g_bWasInside[33]

new Float:fCurRadius // Current radius
new Float:fLastRadius // Previous radius
new Float:fCurZoneOrigins[3] // Current origins of zone's centre
new Float:fLastZoneOrigins[3] // Previous origins of zone's centre
new Float:Delta_X, Float:Delta_Y, Float:Delta_R
new Float:fHitDamage, Float:fWaitingTime, Float:fDiskSeconds, Float:fCompressionTime

new g_mMessageScreenFade

new g_iSprite, Seconds, g_CompressionLevel, g_iSystime
new pcvarZoneSeconds, pcvarZoneAmt, pcvar_updaterate, pCvarWaitTime
new g_ZonePart[NUMSIDES * 2], Float:fDeltaScale, Float:fCurScale

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	if(is_plugin_loaded("BattleRoyale Mod") == -1)
	{
		set_fail_state("MOD DISABLED")
	}

	register_think(zone_EntName, "fw_Zone_Think")
	register_dictionary("effxs_battleroyale.txt")
	
	g_mMessageScreenFade = get_user_msgid("ScreenFade")
	
	pcvarZoneSeconds = register_cvar("pubg_redzone_seconds", "15")
	pcvarZoneAmt = register_cvar("pubg_redzone_renderammount", "300")
	pcvar_updaterate = register_cvar("pubg_redzone_fps", "8")
	pCvarWaitTime = register_cvar("pubg_redzone_wait_time", "50")
	
	register_logevent("event_NewRound", 2, "1&Restart_Round_", "1=Game_Commencing")
	register_event("HLTV", "event_NewRound", "a", "1=0", "2=0")    
}

public plugin_natives()
{
	register_native("isInRedZone", "_isInRedZone")
	register_native("getRedZoneLevel", "_getRedZoneLevel")
}

public _isInRedZone()
{
	return g_CompressionLevel ? isUserInRedZone(get_param(1)) : 0
}

public _getRedZoneLevel()
{
	return bool:g_CompressionLevel
}

public pubg_winner(id1, id2)
{
	zone_Disable(false)
}

public event_NewRound()
{
	new iPlayers[32], playerCount
	get_players(iPlayers, playerCount, "ch")
	if(playerCount >= 1)
	{
		g_iSystime = get_systime()
	
		zone_Disable()
		CompressionTimer()
	}
}

public plugin_precache()
{
	g_iSprite = precache_model("sprites/laserbeam.spr")
	precache_model(ZONE_SPRITE)

	precache_sound(g_szGasWallkingSound)
	precache_sound(g_szGasStopedSound)
	precache_sound(g_szOutSideGas)
	precache_sound(g_szInsideGas)
}

public event_ZoneDamage()
{
	new iPlayers[32], playerCount
	get_players(iPlayers, playerCount, "ach")
	for(new i, id; i < playerCount; i++)
	{
		id = iPlayers[i]
		
		if(isUserInRedZone(id))
		{
			if(pev(id, pev_health) > fHitDamage)
			{
				ScreenFade(id, {255, 0, 0})

				if(!g_bWasInside[id])
				{
					clientPlaySound(id, g_szInsideGas)
					g_bWasInside[id] = true
				}

				set_pev(id, pev_health, pev(id, pev_health) - float(floatround(fHitDamage, floatround_floor)))
				emit_sound(id, 0, g_szHitSounds[random_num(0, sizeof g_szHitSounds - 1)], 1.0, 1.0, 0, 100)
			}
			else
			{
				client_cmd(id, "stopsound")
				user_kill(id, 1)
			}
		}
		else
		{
			if(g_bWasInside[id])
			{
				g_bWasInside[id] = false

				ScreenFade(id, {255, 255, 255})

				client_cmd(id, "stopsound")
				clientPlaySound(id, g_szOutSideGas)
			}
		}
	}
}

public makeZone()
{
	zone_Disable()
	
	fLastRadius = 6000.0
	fHitDamage = 2.0
	fCompressionTime = 60.0
	
	fLastZoneOrigins[0] = 0.0
	fLastZoneOrigins[1] = 0.0
	fLastZoneOrigins[2] = 512.0
	
	fCurRadius = 1700.0
	zone_Spawn(fLastRadius)
	
	new iRandom[4]
	for(new i;i < sizeof iRandom;i++)
	{
		iRandom[i] = random_num(600, 3500)
	}
	
	fCurZoneOrigins[0] = fLastZoneOrigins[0] + float(random_num(-3072 + iRandom[0], 3072 - iRandom[1]))
	fCurZoneOrigins[1] = fLastZoneOrigins[1] + float(random_num(-3072 + iRandom[2], 3072 - iRandom[3]))
	fCurZoneOrigins[2] = 512.0
	
	Delta_X = fCurZoneOrigins[0] - fLastZoneOrigins[0]
	Delta_Y = fCurZoneOrigins[1] - fLastZoneOrigins[1]
	Delta_R = fLastRadius - fCurRadius
	
	fDiskSeconds = (fWaitingTime + fCompressionTime) * 1.01
	
	if(!task_exists(TASK_DAMAGE))
	{
		set_task(1.5, "event_ZoneDamage", TASK_DAMAGE, .flags = "b")
	}
	
	set_task(fWaitingTime, "fn_Compression", TASK_COMPRESSION)
	fix_disk()
}

public zone_Spawn(Float:Radius)
{
	for(new i; i < NUMSIDES * 2; i++)
	{
		g_ZonePart[i] = create_entity("info_target")
		
		if(!pev_valid(g_ZonePart[i]))
			return
		
		new Float:flOrigins[3], Float:flAngles[3]
		flOrigins[0] = Radius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floatcos(360.0 / NUMSIDES * i, degrees)
		flOrigins[1] = Radius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floatsin(360.0 / NUMSIDES * i, degrees)
		flOrigins[2] = 512.0
		
		if(i < NUMSIDES)
			flAngles[1] = 360.0 / NUMSIDES * i
		else
			flAngles[1] = 360.0 / NUMSIDES * i + 180
		
		fCurScale = Radius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floattan(360.0 / (NUMSIDES * 2.0), degrees) / 8
		fDeltaScale = (fCurScale - (fCurRadius * floatcos(360.0 / (NUMSIDES * 2.0), degrees)* floattan(360.0 / (NUMSIDES * 2.0), degrees) / 8)) / fCompressionTime / get_pcvar_num(pcvar_updaterate)
		
		set_pev(g_ZonePart[i], pev_classname, zone_EntName)
		engfunc(EngFunc_SetModel, g_ZonePart[i], ZONE_SPRITE)
		
		set_pev(g_ZonePart[i], pev_scale, fCurScale)
		set_pev(g_ZonePart[i], pev_origin, flOrigins)
		set_pev(g_ZonePart[i], pev_angles, flAngles)
		set_pev(g_ZonePart[i], pev_iuser1, i)
		
		set_pev(g_ZonePart[i], pev_solid, SOLID_NOT)
		set_pev(g_ZonePart[i], pev_movetype, MOVETYPE_NOCLIP)
		set_pev(g_ZonePart[i], pev_renderfx, kRenderFxNone)
		set_pev(g_ZonePart[i], pev_rendermode, kRenderTransAdd)
		set_pev(g_ZonePart[i], pev_renderamt, float(get_pcvar_num(pcvarZoneAmt)))
		
		set_pev(g_ZonePart[i], pev_animtime, get_gametime())
		set_pev(g_ZonePart[i], pev_frame, 0.0)
		set_pev(g_ZonePart[i], pev_framerate, 0.1)
		set_pev(g_ZonePart[i], pev_sequence, 0)
	}
}

public fw_Zone_Think(Ent)
{
	if(!pev_valid(Ent))
		return
	
	new clasadi[15]
	pev(Ent, pev_classname, clasadi, charsmax(clasadi))
	if(!equal(clasadi,zone_EntName))	
	{
		return
	}
	if(pev(Ent, pev_iuser1) == 0)
		fCurScale = fCurScale - fDeltaScale
	
	set_pev(Ent, pev_scale, floatmax(0.0, fCurScale))
	
	if(task_exists(TASK_STOPCOMPRESSION))
		set_pev(Ent, pev_nextthink, get_gametime() + (1.000 / get_pcvar_num(pcvar_updaterate)))
}

public newparams(){
	fCurRadius = fLastRadius * 0.6
	
	fCurZoneOrigins[0] = fLastZoneOrigins[0] + float(random_num(- floatround(fLastRadius) + floatround(fCurRadius), floatround(fLastRadius) - floatround(fCurRadius)))
	fCurZoneOrigins[1] = fLastZoneOrigins[1] + float(random_num(- fn_Get_Y(), fn_Get_Y()))
	fCurZoneOrigins[2] = 0.0
	
	Delta_X = fCurZoneOrigins[0] - fLastZoneOrigins[0]
	Delta_Y = fCurZoneOrigins[1] - fLastZoneOrigins[1]
	Delta_R = fLastRadius - fCurRadius
}

public fn_Compression(){
	static Float:fOldOrigins[3], Float:fNewOrigins[3], Float:fVelocity[3]
	
	set_task(1.0, "fix_zone", TASK_ZONEFIX, _, _, "a", floatround(fCompressionTime) - 1)
	
	set_task(fCompressionTime, "fn_StopCompression", TASK_STOPCOMPRESSION)
	set_task((fCompressionTime - 4.0), "playStopCompressionSound", TASK_SOUND_COMPRESSION)
	
	for(new i; i < NUMSIDES * 2; i++)
	{
		if(pev_valid(g_ZonePart[i]))
		{
			new clasadi[15]
			pev(g_ZonePart[i], pev_classname, clasadi, charsmax(clasadi))
			if(equal(clasadi,zone_EntName))	
			{
				pev(g_ZonePart[i], pev_origin, fOldOrigins)
				
				fNewOrigins[0] = fCurZoneOrigins[0] + (fCurRadius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floatcos(360.0 / NUMSIDES * i, degrees))
				fNewOrigins[1] = fCurZoneOrigins[1] + (fCurRadius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floatsin(360.0 / NUMSIDES * i, degrees))
				
				fVelocity[0] = (fNewOrigins[0] - fOldOrigins[0]) / fCompressionTime
				fVelocity[1] = (fNewOrigins[1] - fOldOrigins[1]) / fCompressionTime
				
				set_pev(g_ZonePart[i], pev_velocity, fVelocity)
				set_pev(g_ZonePart[i], pev_nextthink, get_gametime() + (2.0 / get_pcvar_num(pcvar_updaterate)))
			}	
		}
	}
	return PLUGIN_HANDLED
}

public fn_StopCompression(){

	stopRedZone()

	remove_task(TASK_COMPRESSION)
	fLastZoneOrigins = fCurZoneOrigins
	fLastRadius = fCurRadius
	fCurScale = fLastRadius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floattan(360.0 / (NUMSIDES * 2.0), degrees) / 8
	zone_Create(fLastZoneOrigins, fLastRadius)
	
	g_CompressionLevel += 1
	
	switch(g_CompressionLevel)
	{
		case 1, 2, 3, 4:
		{
			newparams()
			
			fHitDamage *= 1.5
			fWaitingTime *= 0.75
			fCompressionTime *= 0.75
		}
		case 5:
		{
			fCurRadius = 0.0
			
			Delta_X = fCurZoneOrigins[0] - fLastZoneOrigins[0]
			Delta_Y = fCurZoneOrigins[1] - fLastZoneOrigins[1]
			Delta_R = fLastRadius - fCurRadius
			
			fHitDamage = 10.0
			fWaitingTime = 30.0
			fCompressionTime = 10.0
		}
		case 6:
		{
			return
		}
	}
	
	fDeltaScale = (fCurScale - (fCurRadius * floatcos(360.0 / (NUMSIDES * 2.0), degrees)* floattan(360.0 / (NUMSIDES * 2.0), degrees) / 8)) / fCompressionTime / get_pcvar_num(pcvar_updaterate)
	fDiskSeconds = (fWaitingTime + fCompressionTime) * 1.01
	
	fix_disk()
	
	set_task((fWaitingTime + 5.0), "fn_Compression", TASK_COMPRESSION)
	CompressionTimer()
}

public playStopCompressionSound() clientPlaySound(0, g_szGasStopedSound)

public fix_zone()
{
	fLastZoneOrigins[0] += (Delta_X / fCompressionTime)
	fLastZoneOrigins[1] += (Delta_Y / fCompressionTime)
	fLastRadius -= (Delta_R / fCompressionTime)
}

public fix_disk()
{
	disk_Create(fCurZoneOrigins, fCurRadius, floatmin(fDiskSeconds, 10.0))
	fDiskSeconds -= 10.0
	
	if(fDiskSeconds > 0.0)
		set_task(9.95, "fix_disk", TASK_FIXDISK)
}

public zone_Create(Float:Origins[3], Float:radius)
{
	if(!radius)
		return PLUGIN_HANDLED
	
	Origins[2] = 1023.0
	
	for(new i=0; i < NUMSIDES * 2; i++)
	{
		static Float:radialVector[3]
		
		radialVector[0] = Origins[0] + (radius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floatcos(360.0 / NUMSIDES * i, degrees))
		radialVector[1] = Origins[1] + (radius * floatcos(360.0 / (NUMSIDES * 2.0), degrees) * floatsin(360.0 / NUMSIDES * i, degrees))
		radialVector[2] = Origins[2]
		if(pev_valid(g_ZonePart[i]))
		{	 
			new clasadi[15]
			pev(g_ZonePart[i], pev_classname, clasadi, charsmax(clasadi))
			if(equal(clasadi, zone_EntName))	
			{		
				set_pev(g_ZonePart[i], pev_origin, radialVector)
				set_pev(g_ZonePart[i], pev_scale, fCurScale)
			}	
		}
	}
	
	return PLUGIN_HANDLED
}

public disk_Create(Float:Origins[3], Float:radius, Float:time)
{
	if(!radius)
		return
	
	new Float:oldorigin[3]
	
	Origins[2] = 1023.0
	oldorigin = Origins
	
	for(new j; j <= WEIGH; j++)
	{
		static Float:radialVector[3]
		
		radialVector[0] = Origins[0] + (radius * floatcos(360.0 / WEIGH * j, degrees))
		radialVector[1] = Origins[1] + (radius * floatsin(360.0 / WEIGH * j, degrees))
		radialVector[2] = GetMaxHeightWithoutStartOrigin()
		
		if(j != 0)
		{
			draw_line(oldorigin, radialVector, time, {255, 255, 255})
		}
		oldorigin = radialVector
	}
}

public CompressionTimer()
{
	Seconds = floatround((fWaitingTime = fWaitingTime = float(get_pcvar_num(pcvarZoneSeconds))))
	set_task(1.0, "CompressionTimer_Handler", TASK_TIMER, _, _, "b")
}

public CompressionTimer_Handler()
{
	new iDelay = (get_systime() - g_iSystime), iWaitTime = get_pcvar_num(pCvarWaitTime), iSafeZoneTime = (iWaitTime - iDelay)
	if(iDelay < iWaitTime)
	{
		set_hudmessage(255, 255, 255, 0.65, 0.7, 0, 1.0, 1.0)
		show_hudmessage(0, "%L", LANG_PLAYER, "SERVER_SAFEZONE_WILL_APPEAR", iSafeZoneTime)
		return
	}
	else if(iDelay == iWaitTime)
	{
		set_hudmessage(255, 255, 255, -1.0, 0.5, 0, 1.0, 3.5)
		show_hudmessage(0, "%L", LANG_PLAYER, "SERVER_SAFEZONE_SET")
		
		clientPlaySound(0, g_szGasWallkingSound)
		makeZone()
	}
	
	if(iSafeZoneTime >= -4)
		return
	
	switch(Seconds)
	{
		case 0:
		{
			set_hudmessage(255, 130, 20, -1.0, 0.7, 0, 1.0, fCompressionTime)
			show_hudmessage(0, "%L", LANG_PLAYER, "SERVER_REDZONE_GETTING_TINY")
			
			clientPlaySound(0, g_szGasWallkingSound)
			remove_task(TASK_TIMER)
		}
		default:
		{
			set_hudmessage(255, 130, 20, 0.65, 0.7, 0, 6.0, 1.0)
			
			new iMinutes = (Seconds / 60), iSeconds = (Seconds % 60)
			new szMinutes[18], szSeconds[18]
			
			if(iMinutes)
			{
				formatex(szMinutes, charsmax(szMinutes), "%d %L%s", iMinutes, LANG_SERVER, "SERVER_MINUTES", (iMinutes > 1) ? "s" : "")
			}
			if(iSeconds)
			{
				new szPreposition[5]
				formatex(szPreposition, charsmax(szPreposition), " %L ", LANG_SERVER, "SERVER_PREPOSITION")
				formatex(szSeconds, charsmax(szSeconds), "%s%d %L%s", iMinutes ? szPreposition : "", iSeconds, LANG_SERVER, "SERVER_SECONDS", (iSeconds > 1) ? "s" : "")
			}

			show_hudmessage(0, "%L: %d", LANG_PLAYER, "SERVER_REDZONE_WILL_GET_TINY", Seconds)
			--Seconds
		}
	}
}

draw_line(Float:start[3], Float:end[3], Float:time, rgb[3])
{
	engfunc(EngFunc_MessageBegin, MSG_BROADCAST, SVC_TEMPENTITY, start, 0)
	write_byte(TE_BEAMPOINTS)
	engfunc(EngFunc_WriteCoord, start[0])
	engfunc(EngFunc_WriteCoord, start[1])
	engfunc(EngFunc_WriteCoord, start[2])
	engfunc(EngFunc_WriteCoord, end[0])
	engfunc(EngFunc_WriteCoord, end[1])
	engfunc(EngFunc_WriteCoord, end[2])
	write_short(g_iSprite)
	write_byte(2)
	write_byte(9)
	write_byte(max(floatround(time * 10), 1))
	write_byte(50)
	write_byte(0)
	write_byte(rgb[0])
	write_byte(rgb[1])
	write_byte(rgb[2])
	write_byte(150)
	write_byte(50)
	message_end()
}

fn_Get_Y()
{
	return sqroot(floatround(FloatSqr(fLastRadius - fCurRadius) - FloatSqr(fCurZoneOrigins[0] - fLastZoneOrigins[0])))
}

Float:FloatSqr(Float:x)
{
	return x * x
}

isUserInRedZone(id)
{
	new Float:fOrigins[3], Float:fOrigins2[3]
	pev(id, pev_origin, fOrigins)
		
	fOrigins2[0] = fOrigins[0] - fLastZoneOrigins[0]
	fOrigins2[1] = fOrigins[1] - fLastZoneOrigins[1]
		
	return bool:(FloatSqr(fOrigins2[0]) + FloatSqr(fOrigins2[1]) >= FloatSqr(fLastRadius))
}

zone_Disable(bool:bRemove = true)
{
	remove_task(TASK_DAMAGE)
	remove_task(TASK_COMPRESSION)
	remove_task(TASK_FIXDISK)
	remove_task(TASK_STOPCOMPRESSION)
	remove_task(TASK_ZONEFIX)
	remove_task(TASK_TIMER)
	remove_task(TASK_SOUND_COMPRESSION)

	g_CompressionLevel = 0	
	fHitDamage = 0.0

	if(bRemove)
	{
		new ents = -1
		while((ents = find_ent_by_class(ents, zone_EntName)))
		{
			if(pev_valid(ents))
			{
				remove_entity(ents)
			}
		}
	}
	else stopRedZone()
}

stopRedZone()
{
	for(new i; i < NUMSIDES * 2; i++)
	{
		if(pev_valid(g_ZonePart[i]))
		{	
			new clasadi[15]
			pev(g_ZonePart[i], pev_classname, clasadi, charsmax(clasadi))
			if(equal(clasadi,zone_EntName))	
			{		
				set_pev(g_ZonePart[i], pev_velocity, {0.0, 0.0, 0.0})
			}	
		}
	}
}

ScreenFade(id, iRGB[3])
{ 
	message_begin(MSG_ONE, g_mMessageScreenFade, .player = id)
	write_short(4300)
	write_short(1)
	write_short(0)
	write_byte(iRGB[0])
	write_byte(iRGB[1]) 
	write_byte(iRGB[2]) 
	write_byte(110) 
	message_end()
}

Float:GetMaxHeightWithoutStartOrigin() 
{  
	new pcCurrent, Float:fStartPoint[3]
	while((engfunc(EngFunc_PointContents, fStartPoint) == CONTENTS_EMPTY) || (engfunc(EngFunc_PointContents, fStartPoint) == CONTENTS_SOLID)) 
	{ 
		fStartPoint[2] += 5.0 
	} 
	
	pcCurrent = engfunc(EngFunc_PointContents, fStartPoint)
	if(pcCurrent == CONTENTS_SKY) 
	{ 
		return fStartPoint[2] -= 20.0
	} 
	return 0.0 
}  

clientPlaySound(id, const szSound[])
{
	client_cmd(id, "%s ^"%s^"", (equal(szSound[strlen(szSound) - 4], ".mp3")) ? "mp3 play" : "spk", szSound)
}
